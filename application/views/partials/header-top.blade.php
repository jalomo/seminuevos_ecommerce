<div class="header-main">
    <div class="container">
        <div class="row">
            <div class="hidden-xs">
                <div class="logo-main">
                    <a class="bloglogo external" href="<?php echo site_url('inicio'); ?>" rel="nofollow">
                        <img style="height:70px" src="{{ base_url('assets/img/xehosautos.png') }}" class="img-fluid" />
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-5" style="display:none">
                <div class="row">
                    <div class="col-lg-7 hidden-xs hidden-sm">
                        <div class="header-secondary-phone_ float-right">
                            <div class="phone_">
                                <span class="phone-label text-white"> <i class="fa fa-phone-alt fa-2x text-orange"></i> Teléfono:</span>
                                <span class="phone-number heading-font"><a class="text-white" href="tel:33 2805 6406">33 2805 6406</a></span>
                            </div>
                            <div class="phone_">
                                <span class="phone-label text-success"><i class="fab fa-whatsapp  fa-2x text-success"></i> Whatsapp:</span>
                                <span class="phone-number heading-font"><a class="text-white" target="_blank" href="https://api.whatsapp.com/send?phone=+5213328056406">33 2805 6406</a></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-12">
                        <div class="header-main-socs_">
                            <div class="row">
                                <div class="col-lg-3 col-4 mb-2 hidden-lg hidden-md">
                                        <a href="tel:33-3835-2058" target="_blank" class="external" rel="nofollow">
                                            <i class="fa fa-phone-alt fa-2x text-white"></i>
                                        </a>
                                </div>
                                <div class="col-lg-3 col-4 mb-2 hidden-lg hidden-md">
                                    <a href="https://api.whatsapp.com/send?phone=+5213328056406" target="_blank" class="external" rel="nofollow">
                                        <i class="fab fa-whatsapp text-white fa-2x"></i>
                                    </a>
                                </div>
                                <div class="col-lg-3 col-4 mb-2">
                                        <a href="https://www.facebook.com/Xehos-Autolavado-112141223862934" target="_blank" class="external" rel="nofollow">
                                            <i class="fab fa-facebook text-white fa-2x"></i>
                                        </a>
                                </div>
                                <div class="col-lg-3 col-4 mb-2">
                                    <a href="https://twitter.com/xehosautolavado" target="_blank" class="external" rel="nofollow">
                                        <i class="fab fa-twitter text-white fa-2x"></i>
                                    </a>
                                </div>
                                <div class="col-lg-3 col-4">
                                    <a href="https://instagram.com/xehosautolavado" target="_blank" class="external" rel="nofollow">
                                        <i class="fab fa-instagram text-white fa-2x"></i>
                                    </a>
                                </div>
                                <div class="col-lg-3 col-4">
                                    <a href="https://telegram.com/xehosautolavado" target="_blank" class="external" rel="nofollow">
                                        <i class="fab fa-telegram text-white fa-2x"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>