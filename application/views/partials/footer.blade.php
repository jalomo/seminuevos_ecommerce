<footer id="footer">
    <div id="footer-main">
        <div class="footer_widgets_wrapper ">
            <div class="container">
                <div class="widgets cols_4 clearfix">
                    <aside id="socials-2" class="widget widget_socials">
                        <div class="widget-wrapper">
                            <div class="widget-title">
                                <h6>Redes sociales</h6>
                            </div>
                            <div class="socials_widget_wrapper">
                                <ul class="widget_socials list-unstyled clearfix">
                                    <li class="cursor">
                                        <span onclick="goTo(this)" data-url="https://www.facebook.com/Xehos-Autolavado-112141223862934" class="fa-stack">
                                            <i class="fa fa-circle fa-stack-2x text-facebook"></i>
                                            <i class="fa fab fa-facebook fa-stack-1x text-white text-center sizeicon"></i>
                                        </span>
                                    </li>
                                    <li class="cursor">
                                        <span onclick="goTo(this)" data-url="https://www.instagram.com/xehosautolavado/" class="fa-stack">
                                            <i class="fa fa-circle fa-stack-2x text-instagram"></i>
                                            <i class="fa fab fa-instagram fa-stack-1x text-white text-center sizeicon"></i>
                                        </span>
                                    </li>
                                    <li class="cursor">
                                        <span onclick="goTo(this)" data-url="https://twitter.com/xehosautolavado" class="fa-stack">
                                            <i class="fa fa-circle fa-stack-2x text-twitter"></i>
                                            <i class="fa fab fa-twitter fa-stack-1x text-white text-center sizeicon"></i>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                            <div class="sep10"></div>
                            <div class="phone_">
                                <a href="tel:33 2805 6406" class="fa-stack">
                                    <i class="fa fa-circle fa-stack-2x text-orange"></i>
                                    <i class="fa fa-phone-alt fa-stack-1x text-white text-center sizeicon"></i>
                                </a> <span class="text-white ml-2"><b>33 2805 6406</b></span>
                                <div class="sep10"></div>
                                <a href="https://api.whatsapp.com/send?phone=+5213328056406" class="fa-stack">
                                    <i class="fa fa-circle fa-stack-2x text-success"></i>
                                    <i class="fa fab fa-whatsapp fa-stack-1x text-white text-center sizeicon"></i>
                                </a> <span class="text-white ml-2"><b>328056406</b></span>
                            </div>

                        </div>
                    </aside>
                    <aside id="text-3" class="widget widget_text">
                        <div class="widget-wrapper">
                            <div class="widget-title">
                                <h6>HORARIO DE VENTAS <i onclick="$('#horario_ventas').toggle();" class="ml-3 fa fa-plus-circle float-right hidden-lg hidden-sm hidden-md"></i></h6>
                            </div>
                            <div id="horario_ventas" class="textwidget"><span class="date">Lunes - Viernes:</span> 09:00AM - 09:00PM<br />
                                <span class="date">Sabado:</span> 09:00AM - 07:00PM<br />
                                <span class="date">Domingo:</span> 09:00AM - 02:00PM
                            </div>
                        </div>
                    </aside>
                    <aside id="text-4" class="widget widget_text">
                        <div class="widget-wrapper">
                            <div class="widget-title">
                                <h6>HORARIO DE SERVICIOS <i onclick="$('#horario_servicio').toggle();" class="ml-3 fa fa-plus-circle float-right hidden-lg hidden-sm hidden-md"></i></h6>
                            </div>
                            <div id="horario_servicio" class="textwidget"><span class="date">Lunes - Viernes:</span> 09:00AM - 09:00PM<br />
                                <span class="date">Sabado:</span> 09:00AM - 07:00PM<br />
                                <span class="date">Domingo:</span> 09:00AM - 02:00PM
                            </div>
                        </div>
                    </aside>
                    <aside id="text-5" class="widget widget_text">
                        <div class="widget-wrapper">
                            <div class="widget-title">
                                <h6>HORARIO DE PARTES <i onclick="$('#horario_partes').toggle();" class="ml-3 fa fa-plus-circle float-right hidden-lg hidden-sm hidden-md"></i></h6>
                            </div>
                            <div id="horario_partes" class="textwidget"><span class="date">Lunes - Viernes:</span> 09:00AM - 09:00PM<br />
                                <span class="date">Sabado:</span> 09:00AM - 07:00PM<br />
                                <span class="date">Domingo:</span> 09:00AM - 02:00PM
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
    <div id="footer-copyright">
        <div class="container footer-copyright">
            <div class="row">
                <div class="col-12">
                    <div class="clearfix">
                        <div class="copyright-text text-center text-white"> Copyright SOHEX {{ date('Y')}} - Diseñado por SISTEMAS OPERATIVOS HEXADECIMAL. SA DE CV. - Politica de servicio - Aviso de privacidad</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script>
        function goTo(_this) {
            window.open($(_this).data('url'), '_blank');
        }
        $(function() {
            $(document).ready(function() {
                if ($(window).width() < 600)
                    $(".textwidget").hide();
                else
                    $(".textwidget").show();
            });
            var insideIframe = window.top !== window.self;
            if (insideIframe) {
                $('#tecompramosauto').css('display', 'block');
            }
        })
    </script>
</footer>