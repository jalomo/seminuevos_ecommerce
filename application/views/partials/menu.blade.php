<div id="header-nav-holder" style="display:none" class="">
    <div class="header-nav header-nav-default header-nav-fixed">
        <div class="container">
            <div class="header-help-bar-trigger">
                <i class="fa fa-chevron-down"></i>
            </div>
            <div class="header-help-bar">
                <ul>
                    <li class="nav-search">
                        <a href="<?php echo site_url('inicio/login'); ?>"><i class="fa fa-user fa-2x"></i></a>
                    </li>
                </ul>
            </div>
            <div class="main-menu">
                <ul class="header-menu clearfix">
                    <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo site_url('inicio'); ?>">Inicio</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="<?php echo site_url('ventas/autos_nuevos'); ?>" class="external" rel="nofollow">Autos nuevos</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="<?php echo site_url('ventas/autos_seminuevos'); ?>" class="external" rel="nofollow">Autos seminuevos</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="<?php echo site_url('inicio/login'); ?>" class="external" rel="nofollow">Iniciar sesión</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-color:#0e3256 !important;">
            <a class="navbar-brand mr-2 hidden-lg hidden-sm hidden-md" href="#">
            <img style="height:70px" src="{{ base_url('assets/img/xehosautos.png') }}" class="img-fluid" />
            </a>
            <button onclick="$('#navbarText2').toggle()" class="navbar-toggler float-right btn-default" type="button" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon text-white"></span>
            </button>
            <div class="collapse navbar-collapse main-menu" id="navbarText2">
            <div class="hidden-xs mr-5">
                <div class="logo-main">
                    <a class="bloglogo external" href="<?php echo site_url('inicio'); ?>" rel="nofollow">
                        <img style="height:70px" src="{{ base_url('assets/img/xehosautos.png') }}" class="img-fluid" />
                    </a>
                </div>
            </div>
                <ul class="navbar-nav mr-auto header-menu clearfix">
                    <li class="nav-item active menu-item menu-item-type-post_type">
                        <a class="nav-link text-white" href="<?php echo site_url('inicio'); ?>">Inicio <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item menu-item menu-item menu-item-type-post_type text-white">
                        <a href="<?php echo site_url('ventas/autos_nuevos'); ?>" class="nav-link text-white">Autos nuevos</a>
                    </li>
                    <li class="nav-item menu-item menu-item-type-post_type  text-white">
                        <a class="nav-link text-white" href="<?php echo site_url('ventas/autos_seminuevos'); ?>">Autos seminuevos</a>
                    </li>            
                </ul>
                <div class="header-help-bar">
                    <ul>
                        <li class="nav-search">
                            <a href="<?php echo site_url('inicio/login'); ?>"><i class="fa fa-user fa-2x text-white"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>