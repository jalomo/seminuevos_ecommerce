<div id="top-bar">
    <div class="container">
        <div class="clearfix top-bar-wrapper">
            <?php $sucursal = $this->session->userdata('sucursal');
            $agencia = $this->session->userdata('agencia'); ?>
            <div class="pull-right xs-pull-left">
                <ul class="top-bar-info clearfix">
                    <li class="text-white"><i class="fa fa-building"></i> Sucursal : {{ isset($sucursal) ? $sucursal['nombre'] : '' }} </li>
                    <li class="text-white"><i class="fa fa-map-marker"></i> Agencia:  {{ isset($agencia) ? $agencia['nombre'] : '' }} </li>
                    <li class="text-white"><i class="fa fa-user"></i>&nbsp; {{ $this->session->userdata('nombre') ?  $this->session->userdata('nombre')  : $this->session->userdata('adminUsername')   }} </li>

                </ul>
            </div>

        </div>
    </div>
</div>