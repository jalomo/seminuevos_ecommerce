<div id="header-nav-holder" class="">
    <div class="header-nav header-nav-default header-nav-fixed">
        <div class="container">
            <div class="header-help-bar-trigger">
                <i class="fa fa-chevron-down"></i>
            </div>
            <div class="header-help-bar">
                <ul>
                    <li class="nav-search">
                        <a href="{{ site_url('administrador/logout')}}"><i class="fa fa-sign-out-alt"></i></a>
                    </li>
                </ul>
            </div>
            <div class="main-menu">
                <ul class="header-menu clearfix">
                    <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo site_url('administrador/index');?>">Inicio</a></li>
                    <?php if($this->session->userdata('perfil_id') != 2){ ?>

                    <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children"><a href="#">Catálogos</a>
                        <ul class="sub-menu">
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ site_url('administrador/sucursales') }}" class="external" rel="nofollow">Sucursales</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ site_url('administrador/agencias') }}" class="external" rel="nofollow">Agencias</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ site_url('administrador/marcas') }}" class="external" rel="nofollow">Marcas</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ site_url('administrador/tipo_carroceria') }}" class="external" rel="nofollow">Tipo de carroceria</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ site_url('administrador/modelos') }}" class="external" rel="nofollow">Modelos</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ site_url('administrador/tipo_trasmision') }}" class="external" rel="nofollow">Tipo de trasmisión</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ site_url('administrador/tipo_combustible') }}" class="external" rel="nofollow">Tipo de combustible</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ site_url('administrador/tipo_traccion') }}" class="external" rel="nofollow">Tipo de tracción</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ site_url('administrador/colores') }}" class="external" rel="nofollow">Colores</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ site_url('administrador/diseno_interior') }}" class="external" rel="nofollow">Diseño interior</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ site_url('administrador/caracteristicas_adicionales') }}" class="external" rel="nofollow">Características adicionales</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ site_url('administrador/caracteristicas_exteriores') }}" class="external" rel="nofollow">Características exteriores</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ site_url('administrador/caracteristicas_seguridad') }}" class="external" rel="nofollow">Características seguridad</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ site_url('administrador/banners') }}" class="external" rel="nofollow">Banners</a></li>
                        </ul>
                    </li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ site_url('administrador/usuarios')}}" class="external" rel="nofollow">Usuarios</a></li>
                    <?php } ?>

                    <?php if(is_array($this->session->userdata('permisos'))){ ?>
                        <?php foreach ($this->session->userdata('permisos') as $key => $value) { ?>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="<?php echo site_url($value['modulo'].'/'.$value['controlador'].'/'.$value['funcion']); ?>" class="external" rel="nofollow"><?php echo $value['Nombre'] ?></a></li>
                        <?php } ?>
                    <?php } ?>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ site_url('administrador/logout')}}" class="external" rel="nofollow">Salir</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>