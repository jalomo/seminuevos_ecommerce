@section('componentes_css')
    <link rel="stylesheet" href="{{ base_url('assets/components/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ base_url('assets/components/font-awesome/css/all.min.css')}}">
    <link rel="stylesheet" href="{{ base_url('assets/components/toastr/toastr.min.css')}}">
    <link rel="stylesheet" href="{{ base_url('assets/components/select2/dist/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{ base_url('assets/components/sweetalert2/dist/sweetalert2.css')}}">
    <!-- <link rel="stylesheet" href="{{ base_url('assets/components/pace/themes/black/pace-theme-loading-bar.css')}}"> -->
    <link rel="stylesheet" href="{{ base_url('assets/css/custom.css')}}">
@endsection

@section('template_css')
    <link media="all" href="{{ base_url('assets/css/theme.css') }}" rel="stylesheet" />
@endsection

@section('componentes_js')
    <script type="text/javascript" src="{{ base_url('assets/components/jquery/dist/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ base_url('assets/components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ base_url('assets/components/font-awesome/js/all.min.js') }}"></script>
    <script type="text/javascript" src="{{ base_url('assets/components/underscore/underscore-min.js') }}"></script>
    <script type="text/javascript" src="{{ base_url('assets/components/backbone/backbone-min.js') }}"></script>
    <script type="text/javascript" src="{{ base_url('assets/components/backbone.syphon/lib/backbone.syphon.min.js') }}"></script>
    <script type="text/javascript" src="{{ base_url('assets/components/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ base_url('assets/components/sweetalert2/dist/sweetalert2.js') }}"></script>
    <script type="text/javascript" src="{{ base_url('assets/components/select2/dist/js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ base_url('assets/helpers/utils.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ base_url('assets/js/setup.js') }}"></script> --> 
@endsection

@section('template_js')
@endsection

