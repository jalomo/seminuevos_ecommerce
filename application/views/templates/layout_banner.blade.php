<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>eCommerce - Sohex</title>

    @include('templates/assets')
    @yield('componentes_css')
    @yield('template_css')
    @yield('css')

    <script type="text/javascript">
        var PATH = "<?php echo site_url(); ?>";
        var user_id = "<?php echo $this->session->userdata('id'); ?>";
    </script>

</head>

<body class="page-template-default page page-id-986 theme-motors woocommerce-no-js stm_frontend_customizer stm-template-car_dealer stm-user-not-logged-in stm-layout-header-car_dealer wpb-js-composer js-comp-ver-6.4.2 vc_responsive">
    <div id="wrapper">
        @include('partials/header')
        <div id="main">
            <div class="">
                @yield('contenido')
            </div>
        </div>
    </div>
    @include('partials/footer')
    @yield('componentes_js')
    @yield('template_js')
    @yield('modal')
    @yield('scripts')

</body>

</html>