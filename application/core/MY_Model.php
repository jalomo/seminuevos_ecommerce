<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model
{
  
  public $deleted_at = false;

  public $procName;
  public $parametersProcedure = false;
  public $procedureQuery = '';
  
  public function __construct()
  {
    parent::__construct();
  }

  public function getDatetime(){
    $timestamp = DateTime::createFromFormat('0.u00 U', microtime());
    $timestamp->setTimeZone(new DateTimeZone('America/Mexico_City'));
    return $timestamp->format('Y-m-d H:i:s.u');
  }

  public function compile_where($where = false){
    if(is_array($where) && count($where)>0){
      foreach ($where as $key => $value) {
          if(is_array($value) && count($value)>0){
              $this->db->where_in($key,$value);
          }elseif(strlen(trim($value))>0){
              $this->db->where($key,$value);
          } else if (preg_match("/BETWEEN/i", $key)) {
              $this->db->where($key);
          }
      }
    }
    return true;
  }

  public function compile_array($query = false)
  {
    $result = false;
    if($this->deleted_at == true){
      $this->db->where($this->db->get_from().'.deleted_at is NULL', NULL, FALSE);
    }
    try {
      $query = $query == false? $this->db->get() : $query;
      if ($query == false) {
        throw new Exception('Error compile_array');
      }
      $result = ($query->num_rows()>0)? $query->result_array() : false;
      $query->free_result();
    } catch (Exception $e) {
      $result = false;
    }
    return $result;
  }

  public function compile_row($query = false)
  {
    $result = false;
    if($this->deleted_at == true){
      $this->db->where($this->db->get_from().'.deleted_at is NULL', NULL, FALSE);
    }
    try {
      $query = $query == false? $this->db->get() : $query;
      if ($query == false) {
        throw new Exception('Error compile_row');
      }
      $result = ($query->num_rows()>0)? $query->row_array() : false;
      $query->free_result();
    } catch (Exception $e) {
      $result = false;
    }
    return $result;
  }

  public function compile_procedure($name)
  {
    $this->procName = $name;
    return $this->procName;
  }
  public function compile_addParamIn($value){
    $paramters = ($this->parametersProcedure == false)? array() : $this->parametersProcedure;      
    array_push($paramters, array('value'=>$this->db->escape($value),'type'=>'input'));
    $this->parametersProcedure = $paramters;
    return $this->parametersProcedure;
  }

  public function addParamOut($parameter){
    $paramters = ($this->parametersProcedure == false)? array() : $this->parametersProcedure;      
    array_push($paramters, array('value'=>$parameter,'type'=>'output'));
    $this->parametersProcedure = $paramters;
    return $this->parametersProcedure;
  }

  public function compile_call($nameProc = false){
    if($nameProc != false){
      $this->procName = $nameProc;
    }
    
    $parametersCall = '';
    $parametersOut = false;
    if($this->parametersProcedure != false){
      $parametersCall = array();
      foreach ($this->parametersProcedure as $key => $value) {
        array_push($parametersCall,$value['value']);
        if($value['type'] == 'output'){
          if($parametersOut == false){
            $parametersOut = array();
          }
          $parametersOut[] = $value['value'].' as '.trim($value['value'],'@');
        }
      }
      $parametersCall = implode(',',$parametersCall);

    }
    $sql = 'call '.$this->procName.'('.$parametersCall.');';
    $this->procedureQuery = $sql;
    // utils::pre($sql);
    $DB = $this->load->database('default', TRUE);
    $DB->trans_start();
    $query = $DB->query($sql);
    if($parametersOut != false){
      $query = $DB->query('SELECT '.implode(',',$parametersOut).';');
    }
    $DB->trans_complete();
    return $query;
  }
}
