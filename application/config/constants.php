<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

////////////////////////////////////////////////////////////////////////////////////////////////////
//Constantes para encriptados
define('CONST_ENCRYPT',778325.77);
//Correos
define('CONST_CORREO_SALIDA','reservaciones@xehos.com');
define('CONST_PASS_CORREO','bJHXyETnTLNCrZcf1');
define('CONST_AGENCIA','XEHOS AutoLavado');

//Redes sociales 
define("CONST_FACEBOOK",'https://www.facebook.com/Xehos-Autolavado-112141223862934');
define("CONST_INSTAGRAM",'https://www.instagram.com/xehosautolavado/');
define("CONST_TWITTER",'https://twitter.com/xehosautolavado');
define("CONST_YOUTUBE",'https://www.youtube.com/channel/UCqmz6-WM_Ud8luM8ibmHZpA');
define("CONST_WHATSAPP",'https://api.whatsapp.com/send?phone=+523328056406&text=¡Hola!%20Quiero%20solicitar%20un%20servicio');

define("CONST_FACEBOOK_SOHEX",'https://www.facebook.com/SohexDms-104197548089939/');
define("CONST_TWITTER_SOHEX",'https://twitter.com/SohexDms?s=09');
define("CONST_INSTAGRAM_SOHEX",'https://www.instagram.com/sohex_dms/');
define("CONST_LINKEDIN_SOHEX",'https://www.linkedin.com/company/sohexdms');
define("CONST_WHATSAPP_SOHEX",'https://api.whatsapp.com/send?phone=+525535527547&text=¡Hola!%20Quiero%20chatear%20con%20SohexDms');

#CONFIGURACIONES
$settings = include('settings.php');

switch (trim($_SERVER['HTTP_HOST'],'/')) {
    case 'www.xehos.com/ecommerce':
    case 'xehos.com/ecommerce':
        $config = $settings->produccion;
        break;
    default:
        $config = $settings->desarrollo;
        break;
}
// echo '<pre>';print_r($_SERVER);exit();
define('DESARROLLO', $config['desarrollo']);
$server_protocol =  'https';//strpos(strtolower($_SERVER['SERVER_PROTOCOL']), 'https') === false ? 'http' : 'https';
$base_url = $config['host'];
define('BASE_URL', $base_url);

//BASE DE DATOS
define("DB_SERVER",$config['bd_hostname']);
define("DB_USR",$config['bd_user']);
define("DB_PWD", $config['bd_password']);
define("DB_DATABASE", $config['bd_database']);
define("DB_PORT", (array_key_exists('bd_port',$config)? $config['bd_port'] : ' 3306'));

//BASE DE DATOS LOGIN
define("DB_SERVER_LOGIN",$config['bd_hostname_login']);
define("DB_USR_LOGIN",$config['bd_user_login']);
define("DB_PWD_LOGIN", $config['bd_password_login']);
define("DB_DATABASE_LOGIN", $config['bd_database_login']);
define("DB_PORT_LOGIN", (array_key_exists('bd_port_login',$config)? $config['bd_port_login'] : ' 3306'));
