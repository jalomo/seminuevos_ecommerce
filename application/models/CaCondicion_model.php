<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class CaCondicion_model extends MY_Model
{

    public $deleted_at = true;
    public function __construct()
    {
        parent::__construct();
        $this->table = 'ca_condicion';
    }

    public function get($where = false)
    {
        $this->db
            ->select('ca_condicion.*')
            ->from('ca_condicion');
        $this->compile_where($where);
        return $this->compile_row();
    }

    public function getAll($where = false)
    {
        $this->db
            ->select('ca_condicion.*')
            ->from('ca_condicion');
        $this->compile_where($where);
        return $this->compile_array();
    }

    public function insert($dataContent = array())
    {
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));
        return ($this->db->insert('ca_condicion', $dataContent) == true) ? $this->db->insert_id() : false;
    }

    public function update($id, $dataContent = array())
    {
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));

        $this->db->where('id', $id);
        return $this->db->update('ca_condicion', $dataContent);
    }

    function delete($where)
    {
        $this->compile_where($where);
        $this->db->set('deleted_at', date("Y-m-d H:i:s"));
        return $this->db->update('ca_condicion');
    }
}
