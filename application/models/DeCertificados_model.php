<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class DeCertificados_model extends MY_Model
{
    public $query = '';

    public $deleted_at = true;
    public function __construct()
    {
        parent::__construct();
        $this->table = 'de_certificados';
    }

    public function get($where = false)
    {
        $this->db
            ->select('de_certificados.*')
            ->from('de_certificados');
        $this->compile_where($where);
        return $this->compile_row();
    }

    public function getAll($where = false)
    {
        $this->db
            ->select('de_certificados.*')
            ->from('de_certificados');
        $this->compile_where($where);
        return $this->compile_array();
    }

    public function insert($dataContent = array())
    {
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));
        return ($this->db->insert('de_certificados', $dataContent) == true) ? $this->db->insert_id() : false;
    }

    public function update($id, $dataContent = array())
    {
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));

        $this->db->where('id', $id);
        return $this->db->update('de_certificados', $dataContent);
    }

    function delete($where)
    {
        $this->compile_where($where);
        // return $this->db->delete('de_certificados');
        $this->db->set('deleted_at', date("Y-m-d H:i:s"));
        return $this->db->update('de_certificados');
    }
}
