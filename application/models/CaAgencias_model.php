<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class CaAgencias_model extends MY_Model
{
    public $deleted_at = true;

    public function __construct()
    {
        parent::__construct();
        $this->table = 'ca_agencia';
    }

    public function get($where = false)
    {
        $this->db
            ->select('ca_agencia.*')
            ->from('ca_agencia');
        $this->compile_where($where);
        return $this->compile_row();
    }

    public function getAll($where = false)
    {
        $this->db
            ->select('ca_agencia.*, ca_sucursal.nombre as sucursal')
            ->from('ca_agencia')
            ->join('ca_sucursal', 'ca_agencia.sucursal_id = ca_sucursal.id');
        $this->compile_where($where);
        return $this->compile_array();
    }

    public function insert($dataContent = array())
    {
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));
        return ($this->db->insert('ca_agencia', $dataContent) == true) ? $this->db->insert_id() : false;
    }

    public function update($id, $dataContent = array())
    {
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));

        $this->db->where('id', $id);
        return $this->db->update('ca_agencia', $dataContent);
    }

    function delete($where)
    {
        $this->compile_where($where);
        $this->db->set('deleted_at', date("Y-m-d H:i:s"));
        return $this->db->update('ca_agencia');
    }
    
}
