<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class CaBanners_model extends MY_Model
{
    public $deleted_at = true;
    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($where = false)
    {
        $this->db
            ->select('ca_banners.*')
            ->from('ca_banners');
        $this->compile_where($where);
        return $this->compile_array();
    }

    public function insert($dataContent = array())
    {
        $this->db->set('created_at', date("Y-m-d H:i:s"));
        return ($this->db->insert('ca_banners', $dataContent) == true) ? $this->db->insert_id() : false;
    }

    public function delete($where_column, $value)
    {
        $this->db->where($where_column, $value);
        $this->db->set('deleted_at', date("Y-m-d H:i:s"));
        return $this->db->update('ca_banners');
    }
}
