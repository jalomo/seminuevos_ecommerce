<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class DeImagenesCompraAutos_model extends MY_Model
{
    public $deleted_at = true;
    public $query = '';

    public function __construct()
    {
        parent::__construct();
        $this->table = 'de_imagenes_compra_autos';
    }

    public function get($where = false)
    {
        $this->db
            ->select('de_imagenes_compra_autos.*')
            ->from('de_imagenes_compra_autos');
        $this->compile_where($where);
        return $this->compile_row();
    }

    public function getAll($where = false)
    {
        $this->db
            ->select('de_imagenes_compra_autos.*')
            ->from('de_imagenes_compra_autos');
        $this->compile_where($where);
        return $this->compile_array();
    }

    public function insert($dataContent = array())
    {
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));
        return ($this->db->insert('de_imagenes_compra_autos', $dataContent) == true) ? $this->db->insert_id() : false;
    }

    public function update($id, $dataContent = array())
    {
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));

        $this->db->where('id', $id);
        return $this->db->update('de_imagenes_compra_autos', $dataContent);
    }

    function delete($where)
    {
        $this->compile_where($where);
        // return $this->db->delete('de_imagenes_compra_autos');
        $this->db->set('deleted_at', date("Y-m-d H:i:s"));
        return $this->db->update('de_imagenes_compra_autos');
    }
}
