<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class CaAutos_model extends MY_Model
{

    public $deleted_at = true;
    public function __construct()
    {
        parent::__construct();
    }

    public function get($where = false)
    {
        $this->db
            ->select('ca_automovil.*')
            ->from('ca_automovil');
        $this->compile_where($where);
        return $this->compile_row();
    }

    public function getAll($where = false)
    {
        $this->db
            ->select('ca_automovil.*')
            ->from('ca_automovil');
        $this->compile_where($where);
        return $this->compile_array();
    }

    public function getFiltrado($where = false)
    {
        $this->db
            ->select(
                'ca_automovil.*, ca_condicion.nombre as condicion, ca_sucursal.nombre as sucursal, ca_sucursal.coordenada_x, ca_sucursal.coordenada_y, ca_combustible.nombre as combustible,
                 ca_marca.nombre as marca, ca_modelo.nombre as modelo, ca_tipo_carroceria.nombre as tipo_carroceria,
                 ca_tipo_transmision.nombre as tipo_trasmision, ca_combustible.nombre as combustible,
                 ca_tipo_traccion.nombre as tipo_traccion, color_exterior.nombre as color_exterior,color_interior.nombre as color_interior,
                 de_imagenes.file_imagen,ca_agencia.nombre as agencia'
                )
            ->from('ca_automovil')
            ->join('ca_condicion', 'ca_automovil.condicion_id = ca_condicion.id')
            ->join('ca_sucursal', 'ca_automovil.sucursal_id = ca_sucursal.id')
            ->join('ca_agencia', 'ca_automovil.agencia_id = ca_agencia.id' , 'left')
            ->join('ca_marca', 'ca_automovil.marca_id = ca_marca.id')
            ->join('ca_modelo', 'ca_automovil.modelo_id = ca_modelo.id')    
            ->join('ca_tipo_carroceria', 'ca_automovil.tipo_carroceria_id = ca_tipo_carroceria.id')
            ->join('ca_tipo_transmision', 'ca_automovil.tipo_transmision_id = ca_tipo_transmision.id')
            ->join('ca_combustible', 'ca_automovil.tipo_combustible_id = ca_combustible.id')
            ->join('ca_tipo_traccion', 'ca_automovil.tipo_traccion_id = ca_tipo_traccion.id')
            ->join('ca_colores as color_exterior', 'ca_automovil.color_exterior_id = color_exterior.id')
            ->join('ca_colores as color_interior', 'ca_automovil.color_interior_id = color_interior.id')
            ->join('de_imagenes', 'ca_automovil.id = de_imagenes.automovil_id' , 'left');
        $this->compile_where($where);
        $this->db->group_by('ca_automovil.id');
        return $this->compile_array();
    }

    public function insert($dataContent = array())
    {
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));
        return ($this->db->insert('ca_automovil', $dataContent) == true) ? $this->db->insert_id() : false;
    }

    public function update($id, $dataContent = array())
    {
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));

        $this->db->where('id', $id);
        return $this->db->update('ca_automovil', $dataContent);
    }

    function delete($where)
    {
        $this->compile_where($where);
        $this->db->set('deleted_at', date("Y-m-d H:i:s"));
        return $this->db->update('ca_automovil');
    }

}
