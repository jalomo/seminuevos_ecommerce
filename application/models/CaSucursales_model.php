<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class CaSucursales_model extends MY_Model
{


    public $deleted_at = true;
    public function __construct()
    {
        parent::__construct();
        $this->table = 'ca_sucursal';
    }

    public function get($where = false)
    {
        $this->db
            ->select('ca_sucursal.*, ca_marca.nombre as marca')
            ->from('ca_sucursal')
            ->join('ca_marca', 'ca_sucursal.marca_id = ca_marca.id', 'left');
        $this->compile_where($where);
        return $this->compile_row();
    }

    public function getAll($where = false)
    {
        $this->db
            ->select('ca_sucursal.*, ca_marca.nombre as marca')
            ->from('ca_sucursal')
            ->join('ca_marca', 'ca_sucursal.marca_id = ca_marca.id', 'left');
        $this->compile_where($where);
        return $this->compile_array();
    }

    public function insert($dataContent = array())
    {
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));
        return ($this->db->insert('ca_sucursal', $dataContent) == true) ? $this->db->insert_id() : false;
    }

    public function update($id, $dataContent = array())
    {
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));

        $this->db->where('id', $id);
        return $this->db->update('ca_sucursal', $dataContent);
    }

    function delete($where)
    {
        $this->compile_where($where);
        $this->db->set('deleted_at', date("Y-m-d H:i:s"));
        return $this->db->update('ca_sucursal');
    }
}
