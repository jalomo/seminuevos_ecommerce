<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class CaOfertasAuto_model extends MY_Model
{
    public $deleted_at = true;
    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($where = false)
    {
        $this->db->from('ca_ofertas_auto');
        $this->compile_where($where);
        return $this->compile_array();
    }

    public function insert($dataContent = array())
    {
        $this->db->set('created_at', date("Y-m-d H:i:s"));
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));
        return ($this->db->insert('ca_ofertas_auto', $dataContent) == true) ? $this->db->insert_id() : false;
    }

    public function update($id, $dataContent = array())
    {
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));
        $this->db->where('id', $id);
        return $this->db->update('ca_ofertas_auto', $dataContent);
    }
    
}