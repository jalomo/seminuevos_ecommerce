<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class CaPerfiles_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($where = false)
    {
        $this->db->from('ca_perfiles');
        return $this->compile_array();
    }
    
}