<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class CaCompraAuto_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get($where = false)
    {
        $this->db
            ->select('ca_compra_auto.*')
            ->from('ca_compra_auto');
        $this->compile_where($where);
        return $this->compile_row();
    }

    public function getAll($where = false)
    {
        $this->db
            ->select('ca_compra_auto.*')
            ->from('ca_compra_auto');
        $this->compile_where($where);
        return $this->compile_array();
    }

    public function getFiltrado($where = false)
    {
        $this->db
            ->select(
                'ca_compra_auto.*, ca_combustible.nombre as combustible,
                 ca_marca.nombre as marca, ca_modelo.nombre as modelo, ca_tipo_carroceria.nombre as tipo_carroceria,
                 ca_tipo_transmision.nombre as tipo_trasmision, ca_combustible.nombre as combustible,
                 ca_tipo_traccion.nombre as tipo_traccion, color_exterior.nombre as color_exterior,color_interior.nombre as color_interior,
                 de_imagenes_compra_autos.file_imagen'
                )
            ->from('ca_compra_auto')
            ->join('ca_marca', 'ca_compra_auto.marca_id = ca_marca.id')
            ->join('ca_modelo', 'ca_compra_auto.modelo_id = ca_modelo.id')    
            ->join('ca_tipo_carroceria', 'ca_compra_auto.tipo_carroceria_id = ca_tipo_carroceria.id')
            ->join('ca_tipo_transmision', 'ca_compra_auto.tipo_transmision_id = ca_tipo_transmision.id')
            ->join('ca_combustible', 'ca_compra_auto.tipo_combustible_id = ca_combustible.id')
            ->join('ca_tipo_traccion', 'ca_compra_auto.tipo_traccion_id = ca_tipo_traccion.id')
            ->join('ca_colores as color_exterior', 'ca_compra_auto.color_exterior_id = color_exterior.id')
            ->join('ca_colores as color_interior', 'ca_compra_auto.color_interior_id = color_interior.id')
            ->join('de_imagenes_compra_autos', 'ca_compra_auto.id = de_imagenes_compra_autos.compra_auto_id' , 'left');
        $this->compile_where($where);
        $this->db->group_by('ca_compra_auto.id');
        return $this->compile_array();
    }

    public function insert($dataContent = array())
    {
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));
        return ($this->db->insert('ca_compra_auto', $dataContent) == true) ? $this->db->insert_id() : false;
    }

    public function update($id, $dataContent = array())
    {
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));

        $this->db->where('id', $id);
        return $this->db->update('ca_compra_auto', $dataContent);
    }

}
