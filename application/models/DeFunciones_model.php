<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class DeFunciones_model extends MY_Model
{
    public $deleted_at = true;

    public function __construct()
    {
        parent::__construct();
        $this->table = 'de_funciones';
    }

    public function get($where = false)
    {
        $this->db
            ->select('de_funciones.*')
            ->from('de_funciones');
        $this->compile_where($where);
        return $this->compile_row();
    }

    public function getAll($where = false)
    {
        $this->db
            ->select('de_funciones.*')
            ->from('de_funciones');
        $this->compile_where($where);
        return $this->compile_array();
    }

    public function insert($dataContent = array())
    {
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));
        return ($this->db->insert('de_funciones', $dataContent) == true) ? $this->db->insert_id() : false;
    }

    public function update($id, $dataContent = array())
    {
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));

        $this->db->where('id', $id);
        return $this->db->update('de_funciones', $dataContent);
    }

    function delete($where)
    {
        $this->compile_where($where);
        // return $this->db->delete('de_funciones');
        $this->db->set('deleted_at', date("Y-m-d H:i:s"));
        return $this->db->update('de_funciones');
    }

    public function getdetallefunciones($where)
    {
        // dd($where);
        $this->db->select('de_funciones.id, de_funciones.descripcion_funciones')
            ->from('de_funciones');
        $this->compile_where($where);
        $detalle = $this->compile_array();
        
        
        if (!empty($detalle) && count($detalle) > 0) {
            $data = $detalle[0];
            return [
                'funciones' => $data,
                'diseno_interior' => $this->disenointerior(['de_funciones.id' => $data['id']]),
                'caracteristicas_adicionales' => $this->caracteristicaadicional(['de_funciones.id'=> $data['id']]),
                'caracteristicas_exteriores' => $this->caracteristicaexteriores(['de_funciones.id'=> $data['id']]),
                'caracteristica_seguridad' => $this->caracteristicaseguridad(['de_funciones.id'=> $data['id']])
            ];
        }

        return [];
    }

    public function caracteristicaadicional($where)
    {
        $this->db
            ->select('de_funciones.id, ca_caracteristicas_adicionales.nombre as nombre_cadicional,')
            ->from('de_funciones')
            ->join('re_caracteristicas_adicionales_funciones', 'de_funciones.id = re_caracteristicas_adicionales_funciones.funciones_id')
            ->join('ca_caracteristicas_adicionales', 're_caracteristicas_adicionales_funciones.caracteristicas_adicionales_id = ca_caracteristicas_adicionales.id');
        $this->compile_where($where);
        return $this->compile_array();
    }

    public function disenointerior($where)
    {
        $this->db
            ->select('de_funciones.id, ca_diseno_interior.nombre as nombre_diseno_interior')
            ->from('de_funciones')
            ->join('re_diseno_interior_funciones', 'de_funciones.id = re_diseno_interior_funciones.funciones_id')
            ->join('ca_diseno_interior', 're_diseno_interior_funciones.diseno_interior_id = ca_diseno_interior.id');
        $this->compile_where($where);
        return $this->compile_array();
    }

    public function caracteristicaseguridad($where)
    {
        $this->db
            ->select('de_funciones.id, ca_caracteristicas_seguridad.nombre as nombre_caracterisitca_seguridad')
            ->from('de_funciones')
            ->join('re_caracteristicas_seguridad_funciones', 'de_funciones.id = re_caracteristicas_seguridad_funciones.funciones_id')
            ->join('ca_caracteristicas_seguridad', 're_caracteristicas_seguridad_funciones.caracteristicas_seguridad_id = ca_caracteristicas_seguridad.id');
        $this->compile_where($where);
        return $this->compile_array();
    }

    public function caracteristicaexteriores($where)
    {
        $this->db
            ->select('ca_caracteristicas_exteriores.nombre as nombre_disenoexterior')
            ->from('de_funciones')
            ->join('re_caracteristicas_exteriores_funciones', 'de_funciones.id = re_caracteristicas_exteriores_funciones.funciones_id')
            ->join('ca_caracteristicas_exteriores', 're_caracteristicas_exteriores_funciones.caracteristicas_exteriores_id = ca_caracteristicas_exteriores.id');
        $this->compile_where($where);
        return $this->compile_array();
    }
}
