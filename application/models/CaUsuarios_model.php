<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class CaUsuarios_model extends MY_Model
{
    public $table;
    public $deleted_at = true;

    public function __construct()
    {
        parent::__construct();
        $this->table = 'ca_usuarios';
    }

    public function get($where = false)
    {
        $query = $this->db->get($this->table);
        return $this->compile_row($query);
    }

    public function getAll($where = false)
    {
        $this->db->select([
            'ca_usuarios.*',
            'ca_perfiles.id as perfil_id',
            'ca_perfiles.nombre as perfil',
            'ca_agencia.id as agencia_id',
            'ca_agencia.nombre as agencia',
            'ca_sucursal.id as sucursal_id',
            'ca_sucursal.nombre as sucursal',
        ]);
        $this->db->from($this->table);
        $this->db->join('ca_perfiles','ca_usuarios.perfil_id = ca_perfiles.id');
        $this->db->join('ca_agencia','ca_usuarios.agencia_id = ca_agencia.id');
        $this->db->join('ca_sucursal','ca_agencia.sucursal_id = ca_sucursal.id');
        return $this->compile_array();
    }

    public function insert($dataContent = array())
    {
        $this->db->set('fecha_creacion', date("Y-m-d H:i:s"));
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));
        return ($this->db->insert($this->table, $dataContent) == true) ? $this->db->insert_id() : false;
    }

    public function update($dataContent = array(),$id)
    {
        $this->db->where('id',$id);
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));
        return ($this->db->update($this->table, $dataContent) == true) ? true : false;
    }

    function delete($where)
    {
        $this->compile_where($where);
        $this->db->set('deleted_at', date("Y-m-d H:i:s"));
        return $this->db->update($this->table);
    }
    
}