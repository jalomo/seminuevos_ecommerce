<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class CaUsuariosAutolavado_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get($where = false)
    {
        $DB2 = $this->load->database('login', TRUE);
        $DB2->select('usuarios.*')->from('usuarios');
        $DB2->where($where);
        $query = $DB2->get();
        $result = ($query->num_rows()>0)? $query->row_array() : false;
        $query->free_result();
        return $result;
    }

    public function getAll($where = false)
    {
        $DB2
            ->select('usuarios.*')
            ->from('usuarios');
        $this->compile_where($where);
        return $this->compile_array();
    }

}
