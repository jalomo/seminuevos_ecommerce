<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class CaPermisos_model extends MY_Model
{
    public $deleted_at = true;
    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($where = false)
    {
        $this->db->from('ca_permisos');
        return $this->compile_array();
    }

    public function re_agencias($where = false)
    {
        $this->db->from('re_agencia_permiso');
        // $this->db->join('re_agencia_permiso','re_agencia_permiso.id_permiso = ca_permisos.id','right');
        $this->compile_where($where);
        return $this->compile_array();
    }

    public function re_agencias_premiso($where = false)
    {
        $this->db->from('re_agencia_permiso');
        $this->db->join('ca_permisos','re_agencia_permiso.id_permiso = ca_permisos.id','right');
        $this->compile_where($where);
        return $this->compile_array();
    }
    
    public function insert($dataContent = array())
    {
        // $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));
        return ($this->db->insert('re_agencia_permiso', $dataContent) == true) ? $this->db->insert_id() : false;
    }

    function delete($where)
    {
        $this->compile_where($where);
        // return $this->db->delete('ca_tipo_carroceria');
        $this->db->set('deleted_at', date("Y-m-d H:i:s"));
        return $this->db->update('re_agencia_permiso');
    }
}