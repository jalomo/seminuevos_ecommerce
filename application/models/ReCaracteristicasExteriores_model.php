<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class ReCaracteristicasExteriores_model extends MY_Model
{
    public $deleted_at = true;

    public function __construct()
    {
        parent::__construct();
        $this->table = 're_caracteristicas_exteriores_funciones';
    }

    public function get($where = false)
    {
        $this->db
            ->select('re_caracteristicas_exteriores_funciones.*')
            ->from('re_caracteristicas_exteriores_funciones');
        $this->compile_where($where);
        return $this->compile_row();
    }

    public function getAll($where = false)
    {
        $this->db
            ->select('re_caracteristicas_exteriores_funciones.*')
            ->from('re_caracteristicas_exteriores_funciones');
        $this->compile_where($where);
        return $this->compile_array();
    }

    public function insert($dataContent = array())
    {
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));
        return ($this->db->insert('re_caracteristicas_exteriores_funciones', $dataContent) == true) ? $this->db->insert_id() : false;
    }

    public function update($id, $dataContent = array())
    {
        $this->db->set('fecha_actualizacion', date("Y-m-d H:i:s"));

        $this->db->where('id', $id);
        return $this->db->update('re_caracteristicas_exteriores_funciones', $dataContent);
    }

    function delete($where)
    {
        $this->compile_where($where);
        // return $this->db->delete('re_caracteristicas_exteriores_funciones');
        $this->db->set('deleted_at', date("Y-m-d H:i:s"));
        return $this->db->update('re_caracteristicas_exteriores_funciones');
        
    }
}
