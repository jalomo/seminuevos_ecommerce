<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Tipo_traccion extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data = [
      'titulo' => 'Catálogo tipo tracción',
      'model' => 'CaTipoTraccion_model',
      'nombre' => 'tipo tracción',
      'plural' => 'tipo de tracciones'
    ];

    $this->blade->render('administrador/catalogos/index', $data);
  }

}
