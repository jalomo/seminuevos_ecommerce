<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Autos extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function nuevos()
  {
    $this->index(1);
  }

  public function seminuevos()
  {
    $this->index(2);
  }

  public function index($condicion_id = false)
  {
    $condicion_id = ($condicion_id == false) ? $this->input->get('condicion_id') : $condicion_id;
    $subtitle = $condicion_id == 1 ? 'Listado de autos nuevos' : 'Listado de autos seminuevos';
    $data = [
      'titulo' => 'Administrador de autos',
      'subtitle' => $subtitle,
      'mensaje' => 'Lorem Ipsum is simply dummy text of the printing and typesetting ',
      'condicion_id' => $condicion_id
    ];

    $this->blade->render('administrador/autos/index', $data);
  }

  public function agregar()
  {
    $condicion_id = $this->input->get('condicion_id');
    $agencia = $this->session->userdata('agencia');
    $sucursal = $this->session->userdata('sucursal');
    $data = [
      'titulo' => 'Agregar auto' . $condicion_id == 1 ? ' nuevo' : ' seminuevo',
      'condicion_id' => $condicion_id,
      'tipo_user' => $this->session->userdata('session_tipo'),
      'agencia' => $agencia,
      'sucursal' => $sucursal
    ];

    $this->blade->render('administrador/autos/nuevo', $data);
  }
  public function continuar()
  {
    $id = $this->input->get('auto_id');
    $tab = $this->input->get('tab');
    $condicion_id = $this->input->get('condicion_id');
    $agencia = $this->session->userdata('agencia');
    $sucursal = $this->session->userdata('sucursal');

    if (empty($id)) {
      redirect('administrador/autos/index');
    }
    $this->load->model('CaAutos_model');
    $this->load->model('DeInformacionTecnica_model');
    $this->load->model('DeFunciones_model');
    $this->load->model('CaDisenoInterior_model');
    $this->load->model('CaCaracteristicasAdiccionales_model');
    $this->load->model('CaCaracteristicasExteriores_model');
    $this->load->model('CaCaracteristicasSeguridad_model');
    $this->load->model('ReDisenoInteriorFunciones_model');
    $this->load->model('ReCaracteristicasAdiccionales_model');
    $this->load->model('ReCaracteristicasExteriores_model');
    $this->load->model('ReCaracteristicasSeguridad_model');
    $datos = $this->CaAutos_model->get(array('id' => $id));

    $funciones = $this->DeFunciones_model->get(array('automovil_id' => $id));
    if ($funciones) {
      $re_diseno = $this->ReDisenoInteriorFunciones_model->getAll(array('funciones_id' => $funciones['id']));
      $re_adiccional = $this->ReCaracteristicasAdiccionales_model->getAll(array('funciones_id' => $funciones['id']));
      $re_exteriores = $this->ReCaracteristicasExteriores_model->getAll(array('funciones_id' => $funciones['id']));
      $re_seguridad = $this->ReCaracteristicasSeguridad_model->getAll(array('funciones_id' => $funciones['id']));
    } else {
      $re_diseno = [];
      $re_adiccional = [];
      $re_exteriores = [];
      $re_seguridad = [];
    }
    $data = [
      'titulo' => $datos['condicion_id'] == 1 ? 'Auto nuevo' : 'Auto seminuevo',
      'datos' => $datos,
      'tecnica' => $this->DeInformacionTecnica_model->get(array('automovil_id' => $id)),
      'funciones' => $funciones,
      'ca_diseno' => $this->CaDisenoInterior_model->getAll(),
      'check_diseno' => $this->check_array($re_diseno, 'diseno_interior_id'),
      'ca_caracteristicas_adiccionales' => $this->CaCaracteristicasAdiccionales_model->getAll(),
      'check_adiccional' => $this->check_array($re_adiccional, 'caracteristicas_adicionales_id'),
      'ca_caracteristicas_exteriores' => $this->CaCaracteristicasExteriores_model->getAll(),
      'check_exteriores' => $this->check_array($re_exteriores, 'caracteristicas_exteriores_id'),
      'ca_caracteristicas_seguridad' => $this->CaCaracteristicasSeguridad_model->getAll(),
      'check_seguridad' => $this->check_array($re_seguridad, 'caracteristicas_seguridad_id'),
      'tab' => $tab,
      'condicion_id' => $datos['condicion_id'],
      'tipo_user' => $this->session->userdata('session_tipo'),
      'agencia' => $agencia,
      'sucursal' => $sucursal
    ];
    $this->blade->render('administrador/autos/continuar', $data);
  }

  public function getAll()
  {
    $this->load->model('CaAutos_model');
    $data['data'] = $this->CaAutos_model->getFiltrado(array(
      'condicion_id' => $this->input->get('condicion_id'),
      'agencia_id' => $this->session->userdata('agencia_id')

    ));
    return print_r(json_encode($data));
  }

  public function save_caracteristicas()
  {
    $this->load->model('CaAutos_model');
    $this->load->library('form_validation');
    $this->form_validation->set_rules('descripcion', 'Descripción', 'trim|required');
    $this->form_validation->set_rules('sucursal_id', 'Sucursal', 'trim|required');
    $this->form_validation->set_rules('agencia_id', 'Agencia', 'trim|required');
    $this->form_validation->set_rules('condicion_id', 'Condición', 'trim|required');
    $this->form_validation->set_rules('tipo_carroceria_id', 'Tipo carroceria', 'trim|required');
    $this->form_validation->set_rules('marca_id', 'Marca', 'trim|required');
    $this->form_validation->set_rules('modelo_id', 'Modelo', 'trim|required');
    $this->form_validation->set_rules('anio', 'Año', 'trim|required');
    $this->form_validation->set_rules('tipo_transmision_id', 'Tipo transmisión', 'trim|required');
    $this->form_validation->set_rules('kilometraje', 'Kilometraje', 'trim|required');
    $this->form_validation->set_rules('color_exterior_id', 'Color exterior', 'trim|required');
    $this->form_validation->set_rules('color_interior_id', 'Color interior', 'trim|required');
    $this->form_validation->set_rules('tipo_combustible_id', 'Tipo combustible', 'trim|required');
    $this->form_validation->set_rules('tipo_traccion_id', 'Tipo tracción', 'trim|required');
    $this->form_validation->set_rules('precio', 'Precio', 'trim|required');
    $this->form_validation->set_rules('combustible_ciudad', 'Combustible ciudad', 'trim|required');
    $this->form_validation->set_rules('combustible_carretera', 'Combustible carretera', 'trim|required');
    $this->form_validation->set_rules('porcentaje_interes', 'Porcentaje de interes', 'trim|required|numeric');
    $this->form_validation->set_rules('porcentaje_enganche_inicial', 'Porcentaje de enganche inicial', 'trim|required|numeric');

    $automovil_id = $this->input->post('automovil_id');
    if ($this->form_validation->run() == true) {
      $additional_data = array(
        'descripcion' => $this->input->post('descripcion'),
        'sucursal_id' => $this->input->post('sucursal_id'),
        'agencia_id' => $this->input->post('agencia_id'),
        'condicion_id' => $this->input->post('condicion_id'),
        'tipo_carroceria_id' => $this->input->post('tipo_carroceria_id'),
        'marca_id' => $this->input->post('marca_id'),
        'modelo_id' => $this->input->post('modelo_id'),
        'anio' => $this->input->post('anio'),
        'tipo_transmision_id' => $this->input->post('tipo_transmision_id'),
        'kilometraje' => $this->input->post('kilometraje'),
        'color_exterior_id' => $this->input->post('color_exterior_id'),
        'color_interior_id' => $this->input->post('color_interior_id'),
        'tipo_combustible_id' => $this->input->post('tipo_combustible_id'),
        'tipo_traccion_id' => $this->input->post('tipo_traccion_id'),
        'precio' => $this->input->post('precio'),
        'combustible_ciudad' => $this->input->post('combustible_ciudad'),
        'combustible_carretera' => $this->input->post('combustible_carretera'),
        'porcentaje_interes' => $this->input->post('porcentaje_interes'),
        'porcentaje_enganche_inicial' => $this->input->post('porcentaje_enganche_inicial'),
        'link_videos' => $this->input->post('link_videos')
      );
      if ($automovil_id != null && $automovil_id >= 1) {
        $data['automovil_id'] = $automovil_id;
        $data['data'] = $this->CaAutos_model->update($automovil_id, $additional_data);
        $data['mensaje'] = 'Automovil actualizado correctamente';
      } else {
        $data['data'] = $this->CaAutos_model->insert($additional_data);
        $data['automovil_id'] = $data['data'];
        $data['mensaje'] = 'Automovil registrado correctamente';
      }
    } else {
      $data['estatus'] = 'error';
      $data['info'] = $this->form_validation->error_array();
    }

    return print_r(json_encode($data));
  }


  public function save_informacion_tecnica()
  {
    $this->load->model('DeInformacionTecnica_model');
    $this->load->library('form_validation');
    $this->form_validation->set_rules('descripcion_tecnica', 'Descripción técnica', 'trim|required');
    $this->form_validation->set_rules('automovil_id', 'Auto identificador', 'trim|required');
    $this->form_validation->set_rules('numero_cilindros', 'Número de cilindros', 'trim|required');
    $this->form_validation->set_rules('desplazamiento', 'Desplazamiento', 'trim|required');
    $this->form_validation->set_rules('caballos_fuerza', 'Caballos de fuerza', 'trim|required');
    $this->form_validation->set_rules('rpm', '@ rpm', 'trim|required');
    $this->form_validation->set_rules('torque', 'Torque', 'trim|required');
    $this->form_validation->set_rules('indice_compresion', 'Índice de compresión', 'trim|required');
    $this->form_validation->set_rules('velocidad_maxima', 'Velocidad máxima de la pista', 'trim|required');
    $this->form_validation->set_rules('velocidad060', 'Velocidad de 0 - 60 mph', 'trim|required');
    $this->form_validation->set_rules('trasmision', 'Tipo trasmisión  ', 'trim|required');
    $this->form_validation->set_rules('numero_velocidades', 'Número de velocidades  ', 'trim|required');
    $tecnica_id = $this->input->post('tecnica_id');
    if ($this->form_validation->run() == true) {
      $additional_data = array(
        'descripcion_tecnica' => $this->input->post('descripcion_tecnica'),
        'automovil_id' => $this->input->post('automovil_id'),
        'numero_cilindros' => $this->input->post('numero_cilindros'),
        'desplazamiento' => $this->input->post('desplazamiento'),
        'caballos_fuerza' => $this->input->post('caballos_fuerza'),
        'rpm' => $this->input->post('rpm'),
        'torque' => $this->input->post('torque'),
        'indice_compresion' => $this->input->post('indice_compresion'),
        'velocidad_maxima' => $this->input->post('velocidad_maxima'),
        'velocidad060' => $this->input->post('velocidad060'),
        'trasmision' => $this->input->post('trasmision'),
        'numero_velocidades' => $this->input->post('numero_velocidades')
      );
      if ($tecnica_id != null && $tecnica_id >= 1) {
        $data['data'] = $this->DeInformacionTecnica_model->update($tecnica_id, $additional_data);
        $data['mensaje'] = 'Información técnica actualizada correctamente';
      } else {
        $data['data'] = $this->DeInformacionTecnica_model->insert($additional_data);
        $data['mensaje'] = 'Información técnica registrada correctamente';
      }
    } else {
      $data['estatus'] = 'error';
      $data['info'] = $this->form_validation->error_array();
    }
    $data['automovil_id'] = $this->input->post('automovil_id');

    return print_r(json_encode($data));
  }
  public function save_funciones()
  {
    $this->load->model('DeFunciones_model');
    $this->load->model('ReDisenoInteriorFunciones_model');
    $this->load->model('ReCaracteristicasAdiccionales_model');
    $this->load->model('ReCaracteristicasExteriores_model');
    $this->load->model('ReCaracteristicasSeguridad_model');
    $this->load->library('form_validation');
    $this->form_validation->set_rules('descripcion_funciones', 'Descripción funciones', 'trim|required');
    $this->form_validation->set_rules('automovil_id', 'Auto identificador', 'trim|required');
    // $this->form_validation->set_rules('diseno[]', 'Diseño interior', 'trim|required');
    $diseno_post = $this->input->post('diseno');
    $adiccionales_post = $this->input->post('adiccionales');
    $exteriores_post = $this->input->post('exteriores');
    $seguridad_post = $this->input->post('seguridad');
    $funciones_id = $this->input->post('funciones_id');

    if ($this->form_validation->run() == true) {
      $additional_data = array(
        'descripcion_funciones' => $this->input->post('descripcion_funciones'),
        'automovil_id' => $this->input->post('automovil_id'),
      );
      if ($funciones_id != null && $funciones_id >= 1) {
        $data['data'] = $this->DeFunciones_model->update($funciones_id, $additional_data);
        $data['mensaje'] = 'Funciones actualizado correctamente';
      } else {
        $funciones_id = $this->DeFunciones_model->insert($additional_data);
        if ($funciones_id) {
          $data['data'] = $funciones_id;
          $data['mensaje'] = 'Funciones registrado correctamente';
        } else {
          $data['estatus'] = 'error';
          $data['mensaje'] = 'Ocurrio un error al guardar el registro';
        }
      }
      if (is_array($diseno_post) && count($diseno_post) >= 1) {
        $this->ReDisenoInteriorFunciones_model->delete(array('funciones_id' => $funciones_id));
        foreach ($diseno_post as $diseno) {
          $array_diseno = array(
            'funciones_id' => $funciones_id,
            'diseno_interior_id' => $diseno
          );
          $this->ReDisenoInteriorFunciones_model->insert($array_diseno);
        }
      }
      if (is_array($adiccionales_post) && count($adiccionales_post) >= 1) {
        $this->ReCaracteristicasAdiccionales_model->delete(array('funciones_id' => $funciones_id));
        foreach ($adiccionales_post as $addicional) {
          $array_adiccional = array(
            'funciones_id' => $funciones_id,
            'caracteristicas_adicionales_id' => $addicional
          );
          $this->ReCaracteristicasAdiccionales_model->insert($array_adiccional);
        }
      }
      if (is_array($exteriores_post) && count($exteriores_post) >= 1) {
        $this->ReCaracteristicasExteriores_model->delete(array('funciones_id' => $funciones_id));
        foreach ($exteriores_post as $exterior) {
          $array_exteriores = array(
            'funciones_id' => $funciones_id,
            'caracteristicas_exteriores_id' => $exterior
          );
          $this->ReCaracteristicasExteriores_model->insert($array_exteriores);
        }
      }
      if (is_array($seguridad_post) && count($seguridad_post) >= 1) {
        $this->ReCaracteristicasSeguridad_model->delete(array('funciones_id' => $funciones_id));
        foreach ($seguridad_post as $seguridad) {
          $array_seguridad = array(
            'funciones_id' => $funciones_id,
            'caracteristicas_seguridad_id' => $seguridad
          );
          $this->ReCaracteristicasSeguridad_model->insert($array_seguridad);
        }
      }
    } else {
      $data['estatus'] = 'error';
      $data['info'] = $this->form_validation->error_array();
    }
    $data['automovil_id'] = $this->input->post('automovil_id');

    return print_r(json_encode($data));
  }

  public function check_array($array_check, $identificador)
  {
    $check_array = array();
    if (is_array($array_check) && count($array_check) > 0) {
      foreach ($array_check as $row) {
        array_push($check_array, $row[$identificador]);
      }
    }
    return $check_array;
  }

  public function save_imagenes()
  {
    // utils::pre($_POST);

    $this->load->model("DeImagenes_model");
    $this->load->library('form_validation');
    $this->form_validation->set_rules('automovil_id', 'Auto', 'trim|required');
    $this->form_validation->set_rules('name_imagen', 'Subir archivo', 'trim|required');
    if ($this->form_validation->run() == true) {
      $baseFromJavascript = $this->input->post('base64');

      $convert = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $baseFromJavascript));
      $fileName = $this->input->post('name_imagen');
      $fileNameCmps = explode(".", $fileName);
      $fileExtension = strtolower(end($fileNameCmps));
      $newFileName = md5(time() . $fileName). '.' . $fileExtension;

      $uploadFileDir = './imagenes_autos/';
      $dest_path = $uploadFileDir . $newFileName;

      if (file_put_contents($dest_path,$convert)) {
        $additional_data = array(
          'automovil_id' => $this->input->post('automovil_id'),
          'nombre_imagen' => $fileName,
          'file_imagen' => $newFileName
        );
        $data['data'] = $this->DeImagenes_model->insert($additional_data);
        $data['mensaje'] = 'Imagen guardada correctamente';
      } else {
        $message = 'Hubo algún error al mover el archivo al directorio de carga. Asegúrese de que el servidor web pueda escribir en el directorio de carga';

        $data['estatus'] = 'error';
        $data['mensaje'] = $message;
      }

      // Finalmente guarda la imágen en el directorio especificado y con la informacion dada
    } else {
      $data['estatus'] = 'error';
      $data['info'] = $this->form_validation->error_array();
    }
    return print_r(json_encode($data));
  }
  public function save_imagenes_back()
  {

    $this->load->model("DeImagenes_model");
    $this->load->library('form_validation');
    $this->form_validation->set_rules('automovil_id', 'Auto', 'trim|required');
    if ($this->form_validation->run() == true) {

      if (!is_array($_FILES["imagen"]["error"]) && $_FILES["imagen"]["error"] == 0) {
        $fileTmpPath = $_FILES['imagen']['tmp_name'];
        $fileName = $_FILES['imagen']['name'];
        $fileSize = $_FILES['imagen']['size'];
        $fileNameCmps = explode(".", $fileName);
        $fileExtension = strtolower(end($fileNameCmps));

        $fileinfo = @getimagesize($_FILES["imagen"]["tmp_name"]);
        $width = $fileinfo[0];
        $height = $fileinfo[1];

        $newFileName = md5(time() . $fileName) . '.' . $fileExtension;

        $allowedfileExtensions = array('jpg', 'gif', 'png');
        if ($width < "1024" || $height < "768") {
          $message = 'Error la resolución tiene que ser mayor o igual a 1024 por 768 pixeles';
          $data['estatus'] = 'error';
          $data['mensaje'] = $message;
        } else if (in_array($fileExtension, $allowedfileExtensions)) {
          $uploadFileDir = './imagenes_autos/';
          $dest_path = $uploadFileDir . $newFileName;

          if (move_uploaded_file($fileTmpPath, $dest_path)) {
            $additional_data = array(
              'automovil_id' => $this->input->post('automovil_id'),
              'nombre_imagen' => $fileName,
              'file_imagen' => $newFileName
            );
            $data['data'] = $this->DeImagenes_model->insert($additional_data);
            $data['mensaje'] = 'Imagen guardada correctamente';
          } else {
            $message = 'Hubo algún error al mover el archivo al directorio de carga. Asegúrese de que el servidor web pueda escribir en el directorio de carga';
            $data['estatus'] = 'error';
            $data['mensaje'] = $message;
          }
        } else {
          $data['estatus'] = 'error';
          $data['mensaje'] = 'Subida fallida. Extensiones permitidas : ' . implode(',', $allowedfileExtensions);
        }
      } else {
        $message = 'Falta adjuntar el archivo';
        $data['estatus'] = 'error';
        $data['mensaje'] = $message;
      }
    } else {
      $data['estatus'] = 'error';
      $data['info'] = $this->form_validation->error_array();
    }
    return print_r(json_encode($data));
  }

  public function getImagenesByAuto()
  {
    $automovil_id = $this->input->get('auto_id');
    $this->load->model('DeImagenes_model');
    $data['data'] = $this->DeImagenes_model->getAll(array('automovil_id' => $automovil_id));
    return print_r(json_encode($data));
  }

  public function deleteFile()
  {
    $file_id = $this->input->get('file_id');
    $this->load->model('DeImagenes_model');
    $eliminado = $this->DeImagenes_model->delete(array('id' => $file_id));
    if ($eliminado) {
      $data['data'] = $eliminado;
      $data['mensaje'] = "Registro eliminado correctamente";
    } else {
      $data['estatus'] = 'error';
      $data['mensaje'] = 'Ocurrio un error al eliminar el archivo';
    }

    return print_r(json_encode($data));
  }
}
