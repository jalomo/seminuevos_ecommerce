<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Administrador extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index() {
      $data = [
        'titulo' => 'Administración del sistema',
        'mensaje' => 'Lorem Ipsum is simply dummy text of the printing and typesetting '
      ];

      $this->blade->render('administrador/administracion/index', $data);
    }

    public function logout(){
      $this->session->set_userdata('logged_in',false);
      $this->session->sess_destroy();
      redirect('inicio/login', 'refresh'); 
    }

  }
