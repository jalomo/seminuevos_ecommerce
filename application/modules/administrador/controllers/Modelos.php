<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Modelos extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data = [
      'titulo' => 'Catálogo de modelos',
      'model' => 'CaModelos_model',
      'nombre' => 'modelo',
      'plural' => 'modelos'
    ];

    $this->blade->render('administrador/catalogos/index', $data);
  }

}
