<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Autos_ofertados extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index() {
      $data = [
        'titulo' => 'Autos ofertados',
      ];

      $this->blade->render('administrador/autos_ofertados/index', $data);
    }

    public function getAll()
    {
      $this->load->model('CaCompraAuto_model');
      $this->db->where('ca_compra_auto.estatus_id >','0');
      $data['data'] = $this->CaCompraAuto_model->getFiltrado();
      return print_r(json_encode($data));
    }

    

    public function detalle($id = '') {

      $id = $this->input->get('compra_auto_id');    
      
      $this->load->model(['CaCompraAuto_model','DeImagenesCompraAutos_model']);

      $detalle = $this->CaCompraAuto_model->getFiltrado(array('ca_compra_auto.id' => $id));
      $imagenes = $this->DeImagenesCompraAutos_model->getAll(array('compra_auto_id' => $id));  

      $this->load->model('CaUsuariosAutolavado_model');
      $propietario = $this->CaUsuariosAutolavado_model->get(['id'=>$detalle[0]['usuario_id'] ]);

      $this->load->model('CaOfertasAuto_model');
      $ofertas = $this->CaOfertasAuto_model->getAll(array(
        'automovil_id' => $id,
        'agencia_id' => $this->session->userdata('agencia_id')
      ));

      $data = [
        'id' => $id,
        'detalle'=> !empty($detalle) ? $detalle[0] : [],

        'ofertas'=> $ofertas,
        'agencia_id' => $this->session->userdata('agencia_id'),
        'usuario_id' => $this->session->userdata('id'),
        'propietario' => $propietario,
        'titulo' => 'Detalle vehículo',
        'banners' => $imagenes,
        'mensaje' => 'Detalle del vehículo tal con tales caracterisitcas'
      ];

      $this->blade->render('administrador/autos_ofertados/detalle/index', $data);

    }

    public function guardar_ofertas(){

      $this->load->library('form_validation');
      $this->form_validation->set_rules('monto_oferta', 'Monto a ofertar (compra)', 'trim|callback_montos_check',
        array('montos_check' => 'Debe de ingresar por lo menos un monto mayor a 0')
      );
      $this->form_validation->set_rules('monto_compra_nuevo', 'Monto a ofertar (comprando auto nuevo)', 'trim');
      $this->form_validation->set_rules('monto_compra_seminuevo', 'Monto a ofertar (comprando auto seminuevo)', 'trim');
      $this->form_validation->set_rules('comentario', 'Comentario', 'trim');

      if ($this->form_validation->run($this) == true) {
        $additional_data = array(
          'agencia_id' => $this->input->post('agencia_id'),
          'automovil_id' => $this->input->post('auto_id'),
          'usuario_id' => $this->input->post('usuario_id'),
          'monto_ofertado' => $this->input->post('monto_oferta'),
          'monto_compra_nuevo' => $this->input->post('monto_compra_nuevo'),
          'monto_compra_seminuevo' => $this->input->post('monto_compra_seminuevo'),
          'comentarios' => $this->input->post('comentario'),
          'estatus_id' => 1,
        );
        $this->load->model('CaOfertasAuto_model');
        $data['data'] = $this->CaOfertasAuto_model->insert($additional_data);

        $auto_id = $this->input->post('auto_id');
        $this->load->model('CaCompraAuto_model');
        $auto = $this->CaCompraAuto_model->getFiltrado(array('ca_compra_auto.id' => $auto_id));
        $auto = (is_array($auto) && array_key_exists(0,$auto))? current($auto) : $auto;
        
        $this->load->model('CaAgencias_model');
        $agencia = $this->CaAgencias_model->get(array(
          'id' => $this->input->post('agencia_id')
        ));
        
        $this->load->helper('string');
        $mensaje = $this->eliminar_tildes('Su vehículo '.$auto['marca'].' '.$auto['modelo'].' ha recibido una oferta de '.$agencia['nombre'].'. <a href='."'https://xehos.com/xehos_web/xehos/tecompramostuauto'".'>Ver oferta</a>');

        $this->load->model('CaUsuariosAutolavado_model');
        $propietario = $this->CaUsuariosAutolavado_model->get(['id'=>$auto['usuario_id'] ]);
        
        $ch = curl_init();
        $fields = array(
          'celular' => $propietario['telefono'],
          'mensaje' => $mensaje
        );

        $postvars = '';
        foreach($fields as $key=>$value) {
          $postvars .= $key . "=" . $value . "&";
        }

        $url = "https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion";
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch,CURLOPT_POST, 1);                //0 for a get request
        curl_setopt($ch,CURLOPT_POSTFIELDS,$postvars);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
        curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        $response = curl_exec($ch);

        if($errno = curl_errno($ch)) {
          $error_message = curl_strerror($errno);
          echo "cURL error ({$errno}):\n {$error_message}";
        }
        curl_close ($ch);

        $data['mensaje'] = 'Registro agregado correctamente';
      } else {
        $data['status'] = 'error';
        $data['message'] = $this->form_validation->error_array();
      }

      return print_r(json_encode($data));


    }

    public function montos_check($str)
    {
      if ( (int)$this->input->post('monto_oferta') == 0  && (int)$this->input->post('monto_compra_nuevo') == 0 && (int)$this->input->post('monto_compra_seminuevo') == 0 )
      {
        // $this->form_validation->set_message('callback_montos_check', 'The {field} field can not be the word "test"');
        return FALSE;
      }
      else
      {
        return TRUE;
      }
    }

    public function get_ofertas(){
      $id = $this->input->get('id');

      $this->load->model('CaCompraAuto_model');
      $detalle = $this->CaCompraAuto_model->get(array(
        'id' => $id,
      ));
      

      $this->load->model('CaOfertasAuto_model');
      if($detalle['estatus_id'] == 1){
        $ofertas = $this->CaOfertasAuto_model->getAll(array(
          'automovil_id' => $id,
        ));
      }else{
        $ofertas = $this->CaOfertasAuto_model->getAll(array(
          'automovil_id' => $id,
          'estatus_id' => 2
        ));
      }
      

      $this->load->model('CaAgencias_model');
      $this->load->model('CaSucursales_model');
      if(is_array($ofertas)){
        foreach ($ofertas as $key => $value) {
          $agencia = $this->CaAgencias_model->get(array(
            'id' => $value['agencia_id']
          ));
          $ofertas[$key]['agencia'] = $agencia;

          $sucursal = $this->CaSucursales_model->get(array(
            'ca_sucursal.id' => $agencia['sucursal_id']
          ));
          $ofertas[$key]['sucursal'] = $sucursal;
        }
      }
      
      $data = [
        'id' => $id,
        'ofertas'=> $ofertas,
        'detalle' => $detalle
      ];

      $html['html'] = base64_encode($this->blade->render('administrador/autos_ofertados/detalle/get_ofertas', $data,true));
      return print_r(json_encode($html));
    }

  private function eliminar_tildes($cadena)
	{

		$cadena = ($cadena);

		$cadena = str_replace(
			array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
			array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
			$cadena
		);

		$cadena = str_replace(
			array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
			array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
			$cadena
		);

		$cadena = str_replace(
			array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
			array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
			$cadena
		);

		$cadena = str_replace(
			array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
			array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
			$cadena
		);

		$cadena = str_replace(
			array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
			array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
			$cadena
		);

		$cadena = str_replace(
			array('ñ', 'Ñ', 'ç', 'Ç'),
			array('n', 'N', 'c', 'C'),
			$cadena
		);

		return $cadena;
	}

    public function aceptar_rechazar(){
      $id = $this->input->post('id');
      $auto = $this->input->post('auto');
      $additional_data = array(
        'estatus_id' => $this->input->post('estatus')
      );

      if($this->input->post('estatus') == 2){
        $this->load->model('CaCompraAuto_model');
        $this->CaCompraAuto_model->update($auto,array(
          'estatus_id' => 2
        ));
      }

      $this->load->model('CaOfertasAuto_model');
      $data['data'] = $this->CaOfertasAuto_model->update($id,$additional_data);
      return print_r(json_encode($data));
    }

  }
