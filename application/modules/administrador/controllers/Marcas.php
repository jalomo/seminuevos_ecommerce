<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Marcas extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data = [
      'titulo' => 'Catálogo de marcas',
      'model' => 'CaMarcas_model',
      'nombre' => 'marca',
      'plural' => 'marcas'
    ];

    $this->blade->render('administrador/catalogos/index', $data);
  }

}
