<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Tipo_carroceria extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data = [
      'titulo' => 'Catálogo tipo carroceria',
      'model' => 'CaTipoCarroceria_model',
      'nombre' => 'tipo carroceria',
      'plural' => 'tipos carrocerias'
    ];

    $this->blade->render('administrador/catalogos/index', $data);
  }

}
