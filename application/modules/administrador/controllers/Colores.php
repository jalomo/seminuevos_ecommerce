<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Colores extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data = [
      'titulo' => 'Catálogo de colores',
      'model' => 'CaColores_model',
      'nombre' => 'color',
      'plural' => 'colores'
    ];

    $this->blade->render('administrador/catalogos/index', $data);
  }

}
