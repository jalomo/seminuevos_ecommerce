<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Agencias extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data = [
      'titulo' => 'Catálogo de agencias',
      'nombre' => 'agencia',
      'plural' => 'agencias'
    ];

    $this->blade->render('administrador/catalogos/agencias/listado', $data);
  }

  public function agregar()
  {

    $this->load->model('CaPermisos_model');
    $pefiles = $this->CaPermisos_model->getAll();

    $data = [
      'titulo' => 'Agregar agencia',
      'nombre' => 'agencia',
      'pefiles' => $pefiles,
    ];

    $this->blade->render('administrador/catalogos/agencias/vform', $data);
  }

  public function editar()
  {
    $this->load->model('CaAgencias_model');

    if (!$this->input->get('agencia_id')) {
      redirect('administrador/agencias/index');
    }

    $this->load->model('CaPermisos_model');
    $permisos_selec = $this->CaPermisos_model->getAll();
    if(is_array($permisos_selec)){
      foreach ($permisos_selec as $key => $value) {

        $permiso = $this->CaPermisos_model->re_agencias(array(
          're_agencia_permiso.id_agencia' => $this->input->get('agencia_id'),
          're_agencia_permiso.id_permiso' => $value['id']
        ));
        if(is_array($permiso) && count($permiso)>0){
          $permisos_selec[$key]['select'] = true;
        }else{
          $permisos_selec[$key]['select'] = false;
        }
        
      }
    }

    

    $data = [
      'titulo' => 'Editar agencia',
      'nombre' => 'agencia',
      'pefiles' => $permisos_selec,
      'datos' => $this->CaAgencias_model->get(array('id' => $this->input->get('agencia_id')))
    ];

    $this->blade->render('administrador/catalogos/agencias/vform', $data);
  }

  public function certificados()
  {
    $this->load->model('CaAgencias_model');

    if (!$this->input->get('agencia_id')) {
      redirect('administrador/agencias/index');
    }
    $data = [
      'titulo' => 'Certificados',
      'agencia_id' => $this->input->get('agencia_id')
    ];

    $this->blade->render('administrador/catalogos/agencias/certificados', $data);
  }

  public function getAll()
  {
    $this->load->model('CaAgencias_model');
    $data['data'] = $this->CaAgencias_model->getAll();
    return print_r(json_encode($data));
  }

  public function saveform()
  {
    $this->load->model('CaAgencias_model');
    $this->load->library('form_validation');
    $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
    // $this->form_validation->set_rules('permisos[]', 'Permisos', 'trim|required');
    $this->form_validation->set_rules('sucursal_id', 'Sucursal', 'trim|required');
    $this->form_validation->set_rules('coordenada_x', 'Coordenadax', 'trim|required');
    $this->form_validation->set_rules('coordenada_y', 'Coordenaday', 'trim|required');
    $catalogo_id = $this->input->post('catalogo_id');
    if ($this->form_validation->run() == true) {
      $additional_data = array(
        'nombre' => $this->input->post('nombre'),
        'sucursal_id' => $this->input->post('sucursal_id'),
        'coordenada_x' => $this->input->post('coordenada_x'),
        'coordenada_y' => $this->input->post('coordenada_y'),
      );
      if ($catalogo_id != null && $catalogo_id >= 1) {
        $data['data'] = $this->CaAgencias_model->update($catalogo_id, $additional_data);
        $data['mensaje'] = 'Agencia actualizada correctamente';

        $this->load->model('CaPermisos_model');
        $this->CaPermisos_model->delete(array(
          'id_agencia' => $catalogo_id
        ));

        $permisos = $this->input->post('permisos');
        if(is_array($permisos)){
          foreach ($permisos as $key => $value) {
            $this->CaPermisos_model->insert(array(
              'id_agencia' => $catalogo_id,
              'id_permiso' => $value
            ));
          }
        }
      } else {
        $data['data'] = $this->CaAgencias_model->insert($additional_data);

        $permisos = $this->input->post('permisos');
        if(is_array($permisos)){
          $this->load->model('CaPermisos_model');
          foreach ($permisos as $key => $value) {
            $this->CaPermisos_model->insert(array(
              'id_agencia' => $data['data'],
              'id_permiso' => $value
            ));
          }
        }


        $data['mensaje'] = 'Agencia agregada correctamente';
      }
    } else {
      $data['estatus'] = 'error';
      $data['info'] = $this->form_validation->error_array();
    }

    return print_r(json_encode($data));
  }

  public function delete()
  {
    $model = $this->input->post('model');
    $this->load->model('CaAgencias_model');
    $this->load->library('form_validation');
    $this->form_validation->set_rules('id', 'id', 'trim|required');

    if ($this->form_validation->run() == true) {
      $additional_data = array(
        'id' => $this->input->post('id'),
      );
      $data['data'] = $this->CaAgencias_model->delete($additional_data);
      $data['mensaje'] = 'Registro nuevo registrado correctamente';
    } else {
      $data['estatus'] = 'error';
      $data['mensaje'] = 'Ocurrio un error al eliminar el registro';
    }

    return print_r(json_encode($data));
  }

  public function getCertificadosByAgencia()
  {
    $agencia_id = $this->input->get('agencia_id');
    $this->load->model('DeCertificados_model');
    $data['data'] = $this->DeCertificados_model->getAll(array('agencia_id' => $agencia_id));
    return print_r(json_encode($data));
  }

  public function save_certificados()
  {
    $this->load->model("DeCertificados_model");
    $this->load->library('form_validation');
    $this->form_validation->set_rules('agencia_id', 'Agencia', 'trim|required');
    if ($this->form_validation->run() == true) {

      if (!is_array($_FILES["imagen"]["error"]) && $_FILES["imagen"]["error"] == 0) {
        $fileTmpPath = $_FILES['imagen']['tmp_name'];
        $fileName = $_FILES['imagen']['name'];
        $fileSize = $_FILES['imagen']['size'];
        $fileNameCmps = explode(".", $fileName);
        $fileExtension = strtolower(end($fileNameCmps));

        $newFileName = md5(time() . $fileName) . '.' . $fileExtension;

        $allowedfileExtensions = array('jpg', 'gif', 'png');
        if (in_array($fileExtension, $allowedfileExtensions)) {
          $uploadFileDir = './certificados_agencia/';
          $dest_path = $uploadFileDir . $newFileName;

          if (move_uploaded_file($fileTmpPath, $dest_path)) {
            $additional_data = array(
              'agencia_id' => $this->input->post('agencia_id'),
              'nombre_imagen' => $fileName,
              'file_imagen' => $newFileName
            );
            $data['data'] = $this->DeCertificados_model->insert($additional_data);
            $data['mensaje'] = 'Certificado guardado correctamente';
          } else {
            $message = 'Hubo algún error al mover el archivo al directorio de carga. Asegúrese de que el servidor web pueda escribir en el directorio de carga';
            $data['estatus'] = 'error';
            $data['mensaje'] = $message;
          }
        } else {
          $data['estatus'] = 'error';
          $data['mensaje'] = 'Subida fallida. Extensiones permitidas : ' . implode(',', $allowedfileExtensions);
        }
      } else {
        $message = 'Falta adjuntar el archivo';
        $data['estatus'] = 'error';
        $data['mensaje'] = $message;
      }
    } else {
      $data['estatus'] = 'error';
      $data['info'] = $this->form_validation->error_array();
    }
    return print_r(json_encode($data));
  }
  public function deleteFile()
  {
    $file_id = $this->input->get('file_id');
    $this->load->model('DeCertificados_model');
    $eliminado = $this->DeCertificados_model->delete(array('id' => $file_id));
    if ($eliminado) {
      $data['data'] = $eliminado;
      $data['mensaje'] = "Registro eliminado correctamente";
    } else {
      $data['estatus'] = 'error';
      $data['mensaje'] = 'Ocurrio un error al eliminar el archivo';
    }

    return print_r(json_encode($data));
  }
 

}
