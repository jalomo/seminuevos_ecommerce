<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Caracteristicas_exteriores extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data = [
      'titulo' => 'Catálogo caracteristicas exteriores',
      'model' => 'CaCaracteristicasExteriores_model',
      'nombre' => 'caracteristica exterior',
      'plural' => 'caracteristicas exteriores'
    ];

    $this->blade->render('administrador/catalogos/index', $data);
  }

}
