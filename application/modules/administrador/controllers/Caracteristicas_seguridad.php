<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Caracteristicas_seguridad extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data = [
      'titulo' => 'Catálogo caracteristicas seguridad',
      'model' => 'CaCaracteristicasSeguridad_model',
      'nombre' => 'característica seguridad',
      'plural' => 'características de seguridad'
    ];

    $this->blade->render('administrador/catalogos/index', $data);
  }

}
