<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Banners extends MX_Controller
{

    public function __construct()
    {
        $this->load->model('CaBanners_model');
        parent::__construct();
    }

    public function index()
    {
        $banners = $this->CaBanners_model->getAll();
        $data = [
            'banners' => $banners,
            'titulo' => 'Administración del banners',
            'mensaje' => 'Lorem Ipsum is simply dummy text of the printing and typesetting '
        ];

        $this->blade->render('administrador/administracion/banners/list_banners', $data);
    }

    public function ajax_deletebanner()
    {
        if ($this->input->post('id') !== '') {
            $id = $this->input->post('id');
            $deleted = $this->CaBanners_model->delete('id', $id);
            if (!empty($deleted) && $deleted != 0) {
                echo json_encode(['status' => 1, 'msg' => 'Borrado']);
                die();
			} else {
                echo json_encode(['status' => 0, 'msg' => '']);
                die();
			}
        }

        echo json_encode(['status' => 0, 'msg' => '']);
    }

    public function uploadbanner($id = '')
    {

        $this->load->helper(['form']);
        $config['upload_path']          = './assets/banners/';
        $config['allowed_types']        = 'gif|jpg|png';
        // $config['max_size']             = 100;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);
        $this->form_validation->set_rules('bannerfile', 'banner', 'required');
        $this->form_validation->set_message('required', 'el campo {field} es requerido.');
        $data = [
            'titulo' => 'Administración del banners',
            'mensaje' => 'Lorem Ipsum is simply dummy text of the printing and typesetting '
        ];

        if (!$this->upload->do_upload('bannerfile')) {
            $this->blade->render('administrador/administracion/banners/uploadfile', $data);
        } else {
            $data = array('upload_data' => $this->upload->data());
            $nombre = $this->upload->data('file_name');
            $file_path = '/assets/banners/';
            $file_ext = $this->upload->data('file_ext');

            $uploaded = $this->CaBanners_model->insert([
                'nombre_archivo' => $nombre,
                'ruta' => $file_path,
                'file_ext' => $file_ext
            ]);

            if ($uploaded) {
                redirect('/administrador/banners');
            }
        }
    }
}
