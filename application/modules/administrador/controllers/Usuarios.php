<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {

    // $this->load->model('CaUsuarios_model');

    // $result = $this->CaUsuarios_model->get();
    // utils::pre($result);

    $this->load->model('CaSucursales_model');
    $sucursales = $this->CaSucursales_model->getAll();
    
    $this->load->model('CaPerfiles_model');
    $perfiles = $this->CaPerfiles_model->getAll();

    $this->load->model('CaAgencias_model');
    $agencias = $this->CaAgencias_model->getAll();

    $data = [
      'titulo' => 'Catálogo de usuarios',
      'model' => 'CaUsuarios_model',
      'nombre' => 'usuario',
      'plural' => 'usuarios',

      'sucursales' => $sucursales,
      'perfiles' => $perfiles,
      'agencias' => $agencias,

    ];

    $this->blade->render('/usuarios/index', $data);
  }

  public function nuevo(){

    $this->load->model('CaSucursales_model');
    $sucursales = $this->CaSucursales_model->getAll();

    $data = array(
        'sucursales' => $sucursales
    );
    $this->blade->render('/usuarios/nuevo',$data);

  }

  public function getAll(){
    $this->load->model('CaUsuarios_model');
    $response['data'] = $this->CaUsuarios_model->getAll();
    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
      ->_display();
    exit;

  }

  public function nuevo_guardar(){
      
    $response = array();


    $this->load->library('form_validation');
    $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required|max_length[250]');
    $this->form_validation->set_rules('usuario', 'Usuario', 'trim|required|min_length[5]|max_length[250]|is_unique[ca_usuarios.usuario]',array(
      'is_unique'     => 'Este usuario ya se encuentra registrado.'
    ));
    $this->form_validation->set_rules('correo_electronico', 'Correo electrónico', 'trim|required|max_length[250]|valid_email');
    $this->form_validation->set_rules('telefono', 'Teléfono', 'trim|required|numeric|exact_length[10]|max_length[250]',
      array('validar_telefono' => 'El número de Teléfono no es válido')
    );
    $this->form_validation->set_rules('contrasena', 'Contraseña', 'trim|required|min_length[8]|max_length[250]');
    $this->form_validation->set_rules('repetir_contrasena', 'Repetir contraseña', 'trim|required|min_length[8]|max_length[250]|matches[contrasena]');    
    $this->form_validation->set_rules('sucursal_id', 'Sucursal', 'trim|required|max_length[250]');
    $this->form_validation->set_rules('agencia_id', 'Agencia', 'trim|required|max_length[250]');
    $this->form_validation->set_rules('perfil_id', 'Perfil', 'trim|required|max_length[250]');


    if ($this->form_validation->run($this) == FALSE)
    {
      $response['status'] = 'error';
      $response['message'] = $this->form_validation->error_array();
    }
    else
    {
      $this->load->model('CaUsuarios_model');
      $id = $this->CaUsuarios_model->insert(array(
        'nombre' => $this->input->post('nombre'),
        'usuario' => $this->input->post('usuario'),
        'correo_electronico' => $this->input->post('correo_electronico'),
        'telefono' => $this->input->post('telefono'),
        'contrasena' => md5($this->input->post('contrasena')),
        'agencia_id' => $this->input->post('agencia_id'),
        'perfil_id' => $this->input->post('perfil_id')
      ));
      $response['status'] = 'ok';
      $response['data'] = $id;
    }

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
      ->_display();
    exit;

  }

  public function username_check($str)
  {
    $this->load->model('CaUsuarios_model');
    $this->db->where('id <>',$this->input->post('catalogo_id'));
    $this->db->where('usuario',$str);
    $list = $this->CaUsuarios_model->get();
    if ($list != false) {
            $this->form_validation->set_message('username_check', 'Este usuario ya se encuentra registrado.');
            return FALSE;
    } else {
            return TRUE;
    }
  }

  public function editar_guardar(){
      
    $response = array();


    $this->load->library('form_validation');
    $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required|max_length[250]');
    $this->form_validation->set_rules('usuario', 'Usuario', 'trim|required|min_length[5]|max_length[250]|callback_username_check');
    $this->form_validation->set_rules('correo_electronico', 'Correo electrónico', 'trim|required|max_length[250]|valid_email');
    $this->form_validation->set_rules('telefono', 'Teléfono', 'trim|required|numeric|exact_length[10]|max_length[250]',
      array('validar_telefono' => 'El número de Teléfono no es válido')
    );
    $this->form_validation->set_rules('sucursal_id', 'Sucursal', 'trim|required|max_length[250]');
    $this->form_validation->set_rules('agencia_id', 'Agencia', 'trim|required|max_length[250]');
    $this->form_validation->set_rules('perfil_id', 'Perfil', 'trim|required|max_length[250]');


    if ($this->form_validation->run($this) == FALSE)
    {
      $response['status'] = 'error';
      $response['message'] = $this->form_validation->error_array();
    }
    else
    {
      $this->load->model('CaUsuarios_model');
      $id_reg = $this->input->post('catalogo_id');
      $id = $this->CaUsuarios_model->update(array(
        'nombre' => $this->input->post('nombre'),
        'usuario' => $this->input->post('usuario'),
        'correo_electronico' => $this->input->post('correo_electronico'),
        'telefono' => $this->input->post('telefono'),
        'agencia_id' => $this->input->post('agencia_id'),
        'perfil_id' => $this->input->post('perfil_id')
      ),$id_reg);
      $response['status'] = 'ok';
      $response['data'] = $id;
    }

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
      ->_display();
    exit;

  }

  public function delete()
  {
    $model = $this->input->post('model');
    $this->load->model('CaUsuarios_model');
    $this->load->library('form_validation');
    $this->form_validation->set_rules('id', 'id', 'trim|required');

    if ($this->form_validation->run() == true) {
      $additional_data = array(
        'id' => $this->input->post('id'),
      );
      $data['data'] = $this->CaUsuarios_model->delete($additional_data);
      $data['mensaje'] = 'Registro eliminado registrado correctamente';
    } else {
      $data['estatus'] = 'error';
      $data['mensaje'] = 'Ocurrio un error al eliminar el registro';
    }

    return print_r(json_encode($data));
  }

}
