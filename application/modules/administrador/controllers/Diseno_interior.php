<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Diseno_interior extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data = [
      'titulo' => 'Catálogo de diseño interior',
      'model' => 'CaDisenoInterior_model',
      'nombre' => 'diseño interior',
      'plural' => 'diseños interiores'
    ];

    $this->blade->render('administrador/catalogos/index', $data);
  }

}
