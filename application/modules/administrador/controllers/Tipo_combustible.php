<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Tipo_combustible extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data = [
      'titulo' => 'Catálogo tipo combustible',
      'model' => 'CaTipoCombustible_model',
      'nombre' => 'tipo combustible',
      'plural' => 'tipo de combustibles'
    ];

    $this->blade->render('administrador/catalogos/index', $data);
  }

}
