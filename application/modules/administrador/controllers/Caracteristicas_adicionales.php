<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Caracteristicas_adicionales extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data = [
      'titulo' => 'Catálogo caracteristicas adicionales',
      'model' => 'CaCaracteristicasAdiccionales_model',
      'nombre' => 'caracteristica adicional',
      'plural' => 'caracteristicas adicionales'
    ];

    $this->blade->render('administrador/catalogos/index', $data);
  }

}
