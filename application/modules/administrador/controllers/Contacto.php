<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contacto extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {

    $data = [
      'titulo' => 'Citas',
    ];

    $this->blade->render('/contacto/index', $data);
  }

  public function getAll(){
    $this->load->model('CaContactos_model');
    $contacto = $this->CaContactos_model->getAll();

    $response = array(
        'data' => $contacto
    );
    
    $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
        ->_display();
    exit;
  }

  public function get(){
    
    $id = $this->input->post('id');

    $this->load->model('CaContactos_model');
    $contacto = $this->CaContactos_model->get(array(
        'id' => $id
    ));

    $automovil = array();
    if($contacto != false){
        $this->load->model('CaAutos_model');
        $automovil = $this->CaAutos_model->getFiltrado(array(
            'ca_automovil.id' => $contacto['automovil_id']
        ));
    }

    $data = array(
        'contacto' => $contacto,
        'auto' => current($automovil)
    );
    // utils::pre($data);
    
    $html = base64_encode(utf8_decode($this->blade->render('/contacto/detalle', $data)));

    $response = array(
        'contenido' => $html
    );
    
    $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
        ->_display();
    exit;
  }

}
