<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Catalogos extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getAll()
  {
    $model = $this->input->get('model');
    $this->load->model($model);
    $data['data'] = $this->$model->getAll();
    return print_r(json_encode($data));
  }

  public function getAgenciaBySucursal()
  {
    $this->load->model('CaAgencias_model');
    $sucursal_id = $this->input->get('sucursal_id');
    $data['data'] = $this->CaAgencias_model->getAll(array('sucursal_id' => $sucursal_id));
    return print_r(json_encode($data));
  }

  public function store()
  {
    $model = $this->input->post('model');
    $this->load->model($model);
    $this->load->library('form_validation');
    $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');

    if ($this->form_validation->run() == true) {
      $additional_data = array(
        'nombre' => $this->input->post('nombre'),
      );
      $data['data'] = $this->$model->insert($additional_data);
      $data['mensaje'] = 'Registro agregado correctamente';
    } else {
      $data['estatus'] = 'error';
      $data['info'] = $this->form_validation->error_array();
    }

    return print_r(json_encode($data));
  }

  public function update()
  {
    $model = $this->input->post('model');
    $this->load->model($model);
    $this->load->library('form_validation');
    $this->form_validation->set_rules('catalogo_id', 'Id', 'trim|required');
    $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');

    $id = $this->input->post('catalogo_id');
    if ($this->form_validation->run() == true) {
        $additional_data = array(
          'nombre' => $this->input->post('nombre'),
        );
        $data['data'] = $this->$model->update($id, $additional_data);
        $data['mensaje'] = 'Registro actualizado registrado correctamente';
    } else {
        $data['estatus'] = 'error';
        $data['info'] = $this->form_validation->error_array();
    }

    return print_r(json_encode($data));
  }

  public function delete()
  {
    $model = $this->input->post('model');
    $this->load->model($model);
    $this->load->library('form_validation');
    $this->form_validation->set_rules('id', 'id', 'trim|required');

    if ($this->form_validation->run() == true) {
      $additional_data = array(
        'id' => $this->input->post('id'),
      );
      $data['data'] = $this->$model->delete($additional_data);
      $data['mensaje'] = 'Registro nuevo registrado correctamente';
    } else {
      $data['estatus'] = 'error';
      $data['mensaje'] = 'Ocurrio un error al eliminar el registro';
    }

    return print_r(json_encode($data));
  }
}
