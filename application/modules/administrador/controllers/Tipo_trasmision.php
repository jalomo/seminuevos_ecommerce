<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Tipo_trasmision extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data = [
      'titulo' => 'Catálogo tipo trasmisión',
      'model' => 'CaTipoTrasmision_model',
      'nombre' => 'tipo trasmisión',
      'plural' => 'tipo de trasmisiones'
    ];

    $this->blade->render('administrador/catalogos/index', $data);
  }

}
