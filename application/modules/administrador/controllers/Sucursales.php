<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Sucursales extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data = [
      'titulo' => 'Catálogo de sucursales',
      'model' => 'CaSucursales_model',
      'nombre' => 'sucursal',
      'plural' => 'sucursales'
    ];

    $this->blade->render('administrador/catalogos/sucursales', $data);
  }

  public function getAll()
  {
    $this->load->model('CaSucursales_model');
    $data['data'] = $this->CaSucursales_model->getAll();
    return print_r(json_encode($data));
  }

  public function store()
  {
    $this->load->model('CaSucursales_model');
    $this->load->library('form_validation');
    $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
    $this->form_validation->set_rules('marca', 'Marca', 'trim|required');
    $this->form_validation->set_rules('coordenada_x', 'Coordenadax', 'trim|required');
    $this->form_validation->set_rules('coordenada_y', 'Coordenaday', 'trim|required');

    if ($this->form_validation->run() == true) {
      $additional_data = array(
        'nombre' => $this->input->post('nombre'),
        'marca_id' => $this->input->post('marca'),
        'coordenada_x' => $this->input->post('coordenada_x'),
        'coordenada_y' => $this->input->post('coordenada_y'),
      );
      $data['data'] = $this->CaSucursales_model->insert($additional_data);
      $data['mensaje'] = 'Registro agregado correctamente';
    } else {
      $data['estatus'] = 'error';
      $data['info'] = $this->form_validation->error_array();
    }

    return print_r(json_encode($data));
  }

  public function update()
  {
    $model = $this->input->post('model');
    $this->load->model('CaSucursales_model');
    $this->load->library('form_validation');
    $this->form_validation->set_rules('catalogo_id', 'Id', 'trim|required');
    $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
    $this->form_validation->set_rules('marca', 'Marca', 'trim|required');
    $this->form_validation->set_rules('coordenada_x', 'Coordenadax', 'trim|required');
    $this->form_validation->set_rules('coordenada_y', 'Coordenaday', 'trim|required');

    $id = $this->input->post('catalogo_id');
    if ($this->form_validation->run() == true) {
      $additional_data = array(
        'nombre' => $this->input->post('nombre'),
        'marca_id' => $this->input->post('marca'),
        'coordenada_x' => $this->input->post('coordenada_x'),
        'coordenada_y' => $this->input->post('coordenada_y'),
      );
      $data['data'] = $this->CaSucursales_model->update($id, $additional_data);
      $data['mensaje'] = 'Registro actualizado registrado correctamente';
    } else {
      $data['estatus'] = 'error';
      $data['info'] = $this->form_validation->error_array();
    }

    return print_r(json_encode($data));
  }

  public function delete()
  {
    $model = $this->input->post('model');
    $this->load->model('CaSucursales_model');
    $this->load->library('form_validation');
    $this->form_validation->set_rules('id', 'id', 'trim|required');

    if ($this->form_validation->run() == true) {
      $additional_data = array(
        'id' => $this->input->post('id'),
      );
      $data['data'] = $this->CaSucursales_model->delete($additional_data);
      $data['mensaje'] = 'Registro nuevo registrado correctamente';
    } else {
      $data['estatus'] = 'error';
      $data['mensaje'] = 'Ocurrio un error al eliminar el registro';
    }

    return print_r(json_encode($data));
  }
}
