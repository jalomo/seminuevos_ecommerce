@layout('templates/layout_admin')
@section('css')
<link rel="stylesheet" href="{{ base_url('assets/components/DataTables/media/css/jquery.dataTables.min.css')}}">
@endsection
@section('contenido')
<div class="header">
    <h2>Listado de {{ $plural }}</h2>
</div>
<div class="body">
    <div class="float-right mb-3">
        <button catalogo_id="0" type="button" class="btn btn-primary" onclick="app.openModal()"> <i class="fa fa-plus"></i>&nbsp;Agregar</a>
    </div>
    <table id="table-catalogos" class="table table-bordered table-striped" style="width:100%">
    </table>
</div>
@endsection

<div class="modal fade" id="modal-catalogo" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true" >
    <div class="modal-dialog modal-dialog-centered  modal-xl" role="document" style="margin-top:200px">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal">Agregar {{ $plural }}</h5>
            </div>
            <div class="modal-body">
                <form name="form_content">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label>*Nombre</label>
                                <input type="text" name="nombre" class="form-control" />
                                <small id="msg_nombre" class="form-text text-danger"></small>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>*Usuario</label>
                                <input type="text" name="usuario" class="form-control" />
                                <small id="msg_usuario" class="form-text text-danger"></small>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>*Correo electrónico</label>
                                <input type="text" name="correo_electronico" class="form-control" />
                                <small id="msg_correo_electronico" class="form-text text-danger"></small>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>*Teléfono</label>
                                <input type="text" name="telefono" class="form-control" />
                                <small id="msg_telefono" class="form-text text-danger"></small>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="div_contrasena">

                        <div class="col-6">
                            <div class="form-group">
                                <label>*Contraseña</label>
                                <input type="password" name="contrasena" class="form-control" />
                                <small id="msg_contrasena" class="form-text text-danger"></small>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>*Repetir contraseña</label>
                                <input type="password" name="repetir_contrasena" class="form-control" />
                                <small id="msg_repetir_contrasena" class="form-text text-danger"></small>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>*Sucursal</label>
                                <select onchange="app.getAgencias()" name="sucursal_id" class="form-control">
                                    @foreach ($data["sucursales"] as $sucursal)
                                        <option value="{{ $sucursal['id'] }}">{{ $sucursal['nombre'] }}</option>
                                    @endforeach
                                </select>
                                <small id="msg_sucursal_id" class="form-text text-danger"></small>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>*Agencia</label>
                                <select name="agencia_id" class="form-control">
                                    @foreach ($data["agencias"] as $agencia)
                                        <option value="{{ $agencia['id'] }}">{{ $agencia['nombre'] }}</option>
                                    @endforeach
                                </select>
                                <small id="msg_agencia_id" class="form-text text-danger"></small>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>*Perfil</label>
                                <select name="perfil_id" class="form-control">
                                    @foreach ($data["perfiles"] as $perfil)
                                        <option value="{{ $perfil['id'] }}">{{ $perfil['nombre'] }}</option>
                                    @endforeach
                                </select>
                                <small id="msg_perfil_id" class="form-text text-danger"></small>
                            </div>
                        </div>

                        <input type="hidden" name="catalogo_id" class="form-control" />

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button id="btn-modal-agregar" type="button" class="btn btn-primary"><i class="fas fa-save"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>

@section('scripts')
<script type="text/javascript" src="{{ base_url('assets/components/DataTables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ base_url('assets/scripts/administrador/usuarios/index.js') }}"></script>
<link href="{{ base_url('assets/components/pace/themes/red/pace-theme-minimal.css') }}" rel="stylesheet" />
<script src="{{ base_url('assets/components/pace/pace.min.js') }}"></script>
@endsection