@layout('templates/layout_admin')
@section('contenido')
<div class="header">
    <h2> {{ $titulo }}</h2>
</div>
<div class="body">
    <div class="card">
        <div class="card-body">
            <form name="formAgencias" method="POST" enctype="multipart/form-data">
                <div class="col">
                    <div class="form-group">
                        <label>*Sucursal</label>
                        <select name="sucursal_id" class="form-control" data="{{ isset($datos['sucursal_id']) ? $datos['sucursal_id'] : '' }}"></select>
                        <small id="msg_sucursal_id" class="form-text text-danger"></small>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label>* Agencia</label>
                        <input type="text" name="nombre" class="form-control" value="{{ isset($datos['nombre']) ? $datos['nombre'] : '' }}" />
                        <small id="msg_nombre" class="form-text text-danger"></small>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label>* Permisos para administrar</label>
                        <?php 
                        foreach ($data['pefiles'] as $key => $value) {
                            ?><div class="form-check">
                                <input class="form-check-input" type="checkbox" <?php echo (array_key_exists('select',$value)? ($value['select']? 'checked' : '' ) : '' ) ?> name="permisos[]" value="<?php echo $value['id'] ?>">
                                <label class="form-check-label" for="gridCheck1">
                                <?php echo $value['Nombre'] ?>
                                </label>
                            </div><?php
                        }
                        ?>
                        <small id="msg_permisos[]" class="form-text text-danger"></small>
                    </div>
                </div>
                <div class="col">
                    <div id="map" style="height:400px; "></div>
                </div>
                <input type="hidden" name="catalogo_id" value="{{ isset($datos['id']) ? $datos['id'] : '' }}" class="form-control" />
                <input type="hidden" name="coordenada_x" id="coordenada_x" class="form-control" value="{{ isset($datos['coordenada_x']) ? $datos['coordenada_x'] : '' }}" />
                <input type="hidden" name="coordenada_y" id="coordenada_y" class="form-control" value="{{ isset($datos['coordenada_y']) ? $datos['coordenada_y'] : '' }}" />
                <div class="float-right mt-4">
                    <a href="{{ site_url('administrador/agencias/index') }}" class="btn btn-dark"> <i class="fa fa-arrow-left"></i> Regresar</a>
                    @if (isset($datos['id']))
                    <button id="btn-editar" type="button" class="btn btn-primary"><i class="fas fa-save"></i> Editar</button>
                    @else
                    <button id="btn-guardar" type="button" class="btn btn-primary"><i class="fas fa-save"></i> Guardar</button>
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>


</div>
@endsection


@section('scripts')
<script type="text/javascript">
    let nombre = "<?php echo isset($nombre) ? $nombre : ''; ?>";
</script>
<script type="text/javascript" src="{{ base_url('assets/scripts/administrador/catalogos/agencias/vform.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_gbAjK7KGkfidjIi0iLFqD39VWbhtXcs&callback=initMap&libraries=&v=weekly" defer></script>
@endsection