@layout('templates/layout_admin')
@section('css')
<link rel="stylesheet" href="{{ base_url('assets/components/DataTables/media/css/jquery.dataTables.min.css')}}">
@endsection
@section('contenido')
<div class="header">
    <h2> {{ $titulo }}</h2>
</div>
<div class="body">
    <div class="card">
        <div class="card-body">
            <div class="row mb-4">
                <div class="col-md-6">
                    <form id="formCertificados" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>*Subir archivo</label>
                            <div class="custom-file">
                                <input type="file" id="imagen" accept="image/jpg" name="imagen" class="custom-file-input">
                                <label class="custom-file-label"></label>
                            </div>
                            <button type="submit" id="subirFile" class="btn btn-primary mt-4 float-right"><i class="fa fa-save"></i>&nbsp;Guardar</button>
                        </div>
                        <input type="hidden" name="agencia_id" value="{{ isset($agencia_id) ? $agencia_id : '' }}" class="form-control" />
                    </form>
                </div>
                <div class="col-md-6">
                    <table id="table-certificados" class="table table-bordered table-striped" style="width:100%">
                    </table>
                </div>
                <div class=" col float-right mt-4">
                    <a href="{{ site_url('administrador/agencias/index') }}" class="btn btn-dark"> <i class="fa fa-arrow-left"></i> Regresar</a>
                </div>
            </div>
        </div>
    </div>
</div>


</div>
@endsection


@section('scripts')
<script type="text/javascript" src="{{ base_url('assets/components/DataTables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ base_url('assets/scripts/administrador/catalogos/agencias/certificados.js') }}"></script>
<script type="text/javascript">
    $('.custom-file-input').on('change', function() {
        var fileName = document.getElementById("imagen").files[0].name;
        $('.custom-file-label').addClass("selected").html(fileName);
    })
    $("form#formCertificados").on("submit", function(e) {
        e.preventDefault();
        var formData = new FormData(document.getElementById("formCertificados"));
        $.ajax({
            url: PATH + '/administrador/agencias/save_certificados',
            type: "post",
            dataType: "json",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(result, status, xhr) {
                $('.custom-file-label').removeClass("selected").html('');
                if (result.estatus == 'error') {
                    if (result.mensaje) {
                        message.error(result.mensaje, true);
                    } else {
                        errors.mostrar(result.info);
                    }
                } else {
                    if (result.data) {
                        $("form#formCertificados")[0].reset();
                        message.success(result.mensaje, function() {
                            app.getBusquedaCertificados();
                        });
                    }
                }
            }
        })
    });
</script>
@endsection