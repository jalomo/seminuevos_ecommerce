@layout('templates/layout_admin')
@section('css')
<link rel="stylesheet" href="{{ base_url('assets/components/DataTables/media/css/jquery.dataTables.min.css')}}">
@endsection
@section('contenido')
<div class="header">
    <h2>Listado {{ $plural }}</h2>
</div>
<div class="body">
    <div class="float-right mb-3">
        <a href="{{ site_url('administrador/agencias/agregar') }}" class="btn btn-primary"> <i class="fa fa-plus"></i>&nbsp;Agregar</a>
    </div>
    <table id="table-catalogos" class="table table-bordered table-striped" style="width:100%">
    </table>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    let nombre = "<?php echo isset($nombre) ? $nombre : ''; ?>";
</script>
<script type="text/javascript" src="{{ base_url('assets/components/DataTables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ base_url('assets/scripts/administrador/catalogos/agencias/listado.js') }}"></script>

@endsection