@layout('templates/layout_admin')
@section('css')
<link rel="stylesheet" href="{{ base_url('assets/components/DataTables/media/css/jquery.dataTables.min.css')}}">
@endsection
@section('contenido')
<div class="header">
    <h2>Listado {{ $plural }}</h2>
</div>
<div class="body">
    <div class="float-right mb-3">
        <button type="button" class="btn btn-primary" onclick="app.openModal()"> <i class="fa fa-plus"></i>&nbsp;Agregar</a>
    </div>
    <table id="table-catalogos" class="table table-bordered table-striped" style="width:100%">
    </table>
</div>
@endsection

<div class="modal" id="modal-catalogo">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal">Agregar {{ $plural }}</h5>
            </div>
            <div class="modal-body">
                <form name="formCatalogo">
                    <div class="col">
                        <div class="form-group">
                            <label>*Nombre</label>
                            <input type="text" name="nombre" class="form-control" />
                            <small id="msg_nombre" class="form-text text-danger"></small>
                        </div>
                    </div>
                    <input type="hidden" name="catalogo_id" class="form-control" />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button id="btn-modal-agregar" type="button" class="btn btn-primary"><i class="fas fa-save"></i> Guardar</button>
                <button id="btn-modal-editar" type="button" class="btn btn-primary"><i class="fas fa-save"></i> Editar</button>
            </div>
        </div>
    </div>
</div>

@section('scripts')
<script type="text/javascript">
    let modelo = "<?php echo isset($model) ? $model : ''; ?>";
    let nombre = "<?php echo isset($nombre) ? $nombre : ''; ?>";
</script>
<script type="text/javascript" src="{{ base_url('assets/components/DataTables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ base_url('assets/scripts/administrador/catalogos/index.js') }}"></script>
@endsection