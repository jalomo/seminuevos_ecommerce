@layout('templates/layout_admin')
@section('contenido')
<div class="row mt-4">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Banners activos</h5>
                <a href="{{ site_url('administrador/banners/uploadbanner') }}" class="btn btn-primary"> + </a>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">url</th>
                            <th scope="col">nombre</th>
                            <th scope="col">#</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (is_array($data['banners']))
                        @foreach ($data['banners'] as $key => $item)
                            <tr>    
                                <td>{{ $key +1 }}</td>
                                <td>{{ $item['ruta']}}</td>
                                <td>{{ $item['nombre_archivo']}}</td>
                                <td> 
                                    <a href="{{ site_url('administrador/banners/uploadbanner/'.$item['id']) }}" class="btn btn-primary">
                                        Editar
                                    </a>
                                    <button type="button" onclick="handleBorrar({{ $item['id'] }});" class="btn btn-danger">
                                        borrar
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>

        function handleBorrar(id) {
            const formData = new FormData();
            formData.append("id",id);
            $.ajax({
                url: PATH + '/administrador/banners/ajax_deletebanner',
                type: "post",
                dataType: "json",
                data:formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(result, status, xhr) {
                    if(result.status == 1){
                        window.location.reload();
                    }else{
                        message.error("Ha ocurrido un error borrando el archivo", true);
                    }
                }
            })
        }
    </script>
@endsection