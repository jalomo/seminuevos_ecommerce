@layout('templates/layout_admin')
@section('contenido')
<div class="header">
    <h2>Repositorio banners</h2>
    <div class="alert alert-warning" role="alert">
        <p>Se sugiere una resolución de <b>1280 por 500 pixeles</b> para que el cliente tenga una mejor visualización en los banners</p>
    </div>
</div>
<div class="clearfix mt-4"></div>
<div class="body">
    <div class="col">
        <?php echo form_open_multipart('administrador/banners/uploadbanner'); ?>
        <div class="form-group">
            <label>*Selecciona un banner</label>
            <div class="custom-file">
                <input type="file" id="bannerfile"  accept=".jpg, .png" name="bannerfile" class="custom-file-input">
                <label class="custom-file-label"></label>
            </div>
            <button type="submit" id="subirFile" class="btn btn-primary mt-4 float-right"><i class="fa fa-save"></i>&nbsp;Guardar</button>
        </div>
        <!-- <div class="form-group">
            <label for="banner">Selecciona un banner</label>
            <input class="form-control-file" type="file" name="bannerfile" size="20" />
            <button type="submit" class="btn btn-primary">Subir archivo</button>
        </div> -->
        </form>
    </div>
    <div class="clearfix"></div>

</div>
@endsection

@section('scripts')
<script>
$('.custom-file-input').on('change', function() {
        var fileName = document.getElementById("bannerfile").files[0].name;
        $('.custom-file-label').addClass("selected").html(fileName);
    })
</script>
@endsection