@layout('templates/layout_admin')
@section('contenido')
<div class="row mt-4">
    <div class="col">
       @include('administracion/banners/partial_banner')
    </div>
</div>
@endsection

@section('scripts')
    <script>
      $("#carouselbanners > .carousel-inner > .carousel-item").first().addClass('active')
    </script>
@endsection