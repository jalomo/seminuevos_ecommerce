@layout('templates/layout_admin')
@section('contenido')
<div class="row mt-4">
    <div class="col-12 col-centered" style="min-height:350px">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Try Other</h5>
                <h6 class="card-subtitle mb-2 text-muted">Bootstrap 4.0.0 Snippet by pradeep330</h6>
                <p class="card-text">You can also try different version of Bootstrap V4 side menu. Click below link to view all Bootstrap Menu versions.</p>
                <a href="https://bootsnipp.com/pradeep330" class="card-link">link</a>
                <a href="http://websitedesigntamilnadu.com" class="card-link">Another link</a>
            </div>
        </div>
    </div>
</div>

@endsection