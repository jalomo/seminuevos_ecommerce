<div class="row mb-2 mt-2">
    <form name="formAuto">
        <div class="col-md-8 mt-2">
            <div class="form-group">
                <label>*Descripción</label>
                <textarea name="descripcion" class="form-control" style="min-height:200px" maxlength="1000" rows="6">{{ isset($datos['descripcion']) ? $datos['descripcion'] : '' }}</textarea>
                <small id="msg_descripcion" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Sucursal</label>
                @if($tipo_user == 2) 
                <select name="sucursal_id" onchange="app.getAgencias()" class="form-control" data="{{ isset($datos['sucursal_id']) ? $datos['sucursal_id'] : '' }}">
                    <option value=""></option>
                </select>
                @else 
                <input type="hidden" name="sucursal_id" value="{{ $sucursal['id'] }}" />
                <input type="text" readonly="readonly" class="form-control" value="{{ $sucursal['nombre'] }}" />
                @endif
                <small id="msg_sucursal_id" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Agencia</label>
                @if($tipo_user == 2) 
                <select name="agencia_id" class="form-control" data="{{ isset($datos['agencia_id']) ? $datos['agencia_id'] : '' }}">
                    <option value=""></option>
                </select>
                @else 
                <input type="hidden" name="agencia_id" value="{{ $agencia['id'] }}" />
                <input type="text" readonly="readonly" class="form-control" value="{{ $agencia['nombre'] }}" />
                @endif
                <small id="msg_agencia_id" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Precio</label>
                <input type="text" name="precio" class="form-control" value="{{ isset($datos['precio']) ? $datos['precio'] : '' }}" />
                <small id="msg_precio" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Tipo carroceria</label>
                <select name="tipo_carroceria_id" class="form-control" data="{{ isset($datos['tipo_carroceria_id']) ? $datos['tipo_carroceria_id'] : '' }}">
                    <option value=""></option>
                </select>
                <small id="msg_tipo_carroceria_id" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Marca</label>
                <select name="marca_id" class="form-control" data="{{ isset($datos['marca_id']) ? $datos['marca_id'] : '' }}">
                    <option value=""></option>
                </select>
                <small id="msg_marca_id" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Modelo</label>
                <select name="modelo_id" class="form-control" data="{{ isset($datos['modelo_id']) ? $datos['modelo_id'] : '' }}">
                    <option value=""></option>
                </select>
                <small id="msg_modelo_id" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Año</label>
                <select name="anio" class="form-control select2">
                    @for($year = date('Y'); $year >= 1970; $year--)
                    <option {{ isset($datos['anio']) && $year == $datos['anio']  ? 'selected="selected"'  : '' }} value="{{ $year }}">{{ $year }}</option>
                    @endfor
                </select>
                <small id="msg_anio" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Tipo transmisión</label>
                <select name="tipo_transmision_id" class="form-control" data="{{ isset($datos['tipo_transmision_id']) ? $datos['tipo_transmision_id'] : '' }}">
                    <option value=""></option>
                </select>
                <small id="msg_tipo_transmision_id" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*kilometraje</label>
                <input type="number" name="kilometraje" class="form-control" value="{{ isset($datos['kilometraje']) ? $datos['kilometraje'] : '' }}" />
                <small id="msg_kilometraje" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Color exterior</label>
                <select name="color_exterior_id" class="form-control" data="{{ isset($datos['color_exterior_id']) ? $datos['color_exterior_id'] : '' }}">
                    <option value=""></option>
                </select>
                <small id="msg_color_exterior_id" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Color interior</label>
                <select name="color_interior_id" class="form-control" data="{{ isset($datos['color_interior_id']) ? $datos['color_interior_id'] : '' }}">
                    <option value=""></option>
                </select>
                <small id="msg_color_interior_id" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Tipo combustible</label>
                <select name="tipo_combustible_id" class="form-control" data="{{ isset($datos['tipo_combustible_id']) ? $datos['tipo_combustible_id'] : '' }}">
                    <option value=""></option>
                </select>
                <small id="msg_tipo_combustible_id" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Tipo de tracción </label>
                <select name="tipo_traccion_id" class="form-control" data="{{ isset($datos['tipo_traccion_id']) ? $datos['tipo_traccion_id'] : '' }}">
                    <option value=""></option>
                </select>
                <small id="msg_tipo_traccion_id" class="form-text text-danger"></small>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Combustible en ciudad</label>
                <input type="number" name="combustible_ciudad" class="form-control" value="{{ isset($datos['combustible_ciudad']) ? $datos['combustible_ciudad'] : '' }}" />
                <small id="msg_combustible_ciudad" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Combustible en carretera</label>
                <input type="number" name="combustible_carretera" class="form-control" value="{{ isset($datos['combustible_carretera']) ? $datos['combustible_carretera'] : '' }}" />
                <small id="msg_combustible_carretera" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Porcentaje de interes %</label>
                <input type="number" name="porcentaje_interes" class="form-control" value="{{ isset($datos['porcentaje_interes']) ? $datos['porcentaje_interes'] : '' }}" />
                <small id="msg_porcentaje_interes" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Porcentaje de enganche inicial %</label>
                <input type="number" name="porcentaje_enganche_inicial" class="form-control" value="{{ isset($datos['porcentaje_enganche_inicial']) ? $datos['porcentaje_enganche_inicial'] : '' }}" />
                <small id="msg_porcentaje_enganche_inicial" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-8 mt-2">
            <div class="form-group">
                <label>Link video youtube</label>
                <input type="text" name="link_videos" class="form-control" value="{{ isset($datos['link_videos']) ? $datos['link_videos'] : '' }}" />
                <small id="msg_link_videos" class="form-text text-danger"></small>
            </div>
        </div>
        <input type="hidden" class="form-control" name="condicion_id" value="{{ isset($condicion_id) ? $condicion_id : '' }}" />
        <input type="hidden" class="form-control" name="automovil_id" value="{{ isset($datos['id']) ? $datos['id'] : '' }}" />
    </form>
</div>