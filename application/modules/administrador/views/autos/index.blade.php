@layout('templates/layout_admin')
@section('css')
<link rel="stylesheet" href="{{ base_url('assets/components/DataTables/media/css/jquery.dataTables.min.css')}}">
@endsection
@section('contenido')

<div class="header">
    <h2>{{ $subtitle }}</h2>
</div>
<div class="body">
    <div class="float-right mb-3">
        <a href="{{ site_url('administrador/autos/agregar?condicion_id=' . $condicion_id) }}"  class="btn btn-primary"> <i class="fa fa-car"></i>&nbsp;Agregar</a>
    </div>
    <input type="hidden" name="condicion_id" id="condicion_id" value="{{ $condicion_id }}" />
    <table id="table-autos" class="table table-bordered table-striped" style="width:100%">       
    </table>
</div>

@endsection

@section('scripts')

<script type="text/javascript" src="{{ base_url('assets/components/DataTables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ base_url('assets/scripts/administrador/autos/index.js') }}"></script>
@endsection