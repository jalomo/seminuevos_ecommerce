<form name="formTecnica">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>*Descripción técnica</label>
                <textarea type="text" class="form-control" maxlength="1000" style="min-height:300px" name="descripcion_tecnica">{{ isset($tecnica['descripcion_tecnica']) ? $tecnica['descripcion_tecnica'] : '' }} </textarea>
                <small id="msg_descripcion_tecnica" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-6">
            <div class="stm-tech-infos mt-4">
                <div class="">
                    <i class="fa fa-car text-orange" style="font-size:27px;"></i>
                    <span class="title h5">Motor</span>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Número de cilindros</label>
                            <select name="numero_cilindros" class="form-control">
                                <option value=""></option>
                                @for($cilindros= 2; $cilindros <= 8; $cilindros ++)
                                 <option {{ isset($tecnica['numero_cilindros']) && $cilindros == $tecnica['numero_cilindros'] ? 'selected="selected"' : ''}} value="{{ $cilindros }}">{{ $cilindros }}</option>
                                @endfor
                            </select>
                            <small id="msg_numero_cilindros" class="form-text text-danger"></small>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Desplazamiento</label>
                            <input type="text" class="form-control" name="desplazamiento" value="{{ isset($tecnica['desplazamiento']) ? $tecnica['desplazamiento'] : '' }}" />
                            <small id="msg_desplazamiento" class="form-text text-danger"></small>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Caballos de fuerza</label>
                            <input type="text" class="form-control" name="caballos_fuerza" value="{{ isset($tecnica['caballos_fuerza']) ? $tecnica['caballos_fuerza'] : '' }}" />
                            <small id="msg_caballos_fuerza" class="form-text text-danger"></small>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>@ rpm</label>
                            <input type="text" class="form-control" name="rpm" value="{{ isset($tecnica['rpm']) ? $tecnica['rpm'] : '' }}" />
                            <small id="msg_rpm" class="form-text text-danger"></small>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Torque</label>
                            <input type="text" class="form-control" name="torque" value="{{ isset($tecnica['torque']) ? $tecnica['torque'] : '' }}" />
                            <small id="msg_torque" class="form-text text-danger"></small>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Índice de compresión</label>
                            <input type="text" class="form-control" name="indice_compresion" value="{{ isset($tecnica['indice_compresion']) ? $tecnica['indice_compresion'] : '' }}" />
                            <small id="msg_indice_compresion" class="form-text text-danger"></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="stm-tech-infos">
        <div class="">
            <i class="fa fa-road text-orange" style="font-size:28px;"></i>
            <span class="title h5">Velocidad</span>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Velocidad máxima de la pista</label>
                    <input type="text" class="form-control" name="velocidad_maxima" value="{{ isset($tecnica['velocidad_maxima']) ? $tecnica['velocidad_maxima'] : '' }}" />
                    <small id="msg_velocidad_maxima" class="form-text text-danger"></small>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>0 - 60 mph</label>
                    <input type="text" class="form-control" name="velocidad060" value="{{ isset($tecnica['velocidad060']) ? $tecnica['velocidad060'] : '' }}" />
                    <small id="msg_velocidad060" class="form-text text-danger"></small>
                </div>
            </div>
        </div>
    </div>
    <div class="stm-tech-infos">
        <div class="">
            <i class="fa fa-cogs text-orange" style="font-size:35px;"></i>
            <span class="title h5">Transmisión</span>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tipo trasmisión</label>
                    <input type="text" class="form-control" name="trasmision" value="{{ isset($tecnica['trasmision']) ? $tecnica['trasmision'] : '' }}" />
                    <small id="msg_trasmision" class="form-text text-danger"></small>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Número velocidades</label>
                    <select name="numero_velocidades" class="form-control">
                        <option value=""></option>
                        @for($velocidades= 1; $velocidades <= 12; $velocidades ++) 
                        <option {{ isset($tecnica['numero_velocidades']) && $velocidades == $tecnica['numero_velocidades'] ? 'selected="selected"' : ''}} value="{{ $velocidades }}">{{ $velocidades }}</option>
                        @endfor
                    </select>
                    <small id="msg_numero_velocidades" class="form-text text-danger"></small>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="automovil_id" value="{{ isset($datos['id']) ? $datos['id'] : '' }}" />
    <input type="hidden" name="tecnica_id" value="{{ isset($tecnica['id']) ? $tecnica['id'] : '' }}" />
</form>
<div class="float-right mt-3">
    @if (isset($tecnica['id']))
    <button type="button" id="guardarInformacionTecnica" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;Editar</button>
    @else
    <button type="button" id="guardarInformacionTecnica" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;Guardar</button>
    @endif
</div>