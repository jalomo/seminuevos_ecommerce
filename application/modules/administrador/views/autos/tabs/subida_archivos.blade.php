<div class="header">
    <h2>Repositorio archivos</h2>
    <div class="alert alert-warning" role="alert">
        <p>Se sugiere una resolución minima a <b>1024 por 768 pixeles</b> para que el cliente tenga una mejor visualización</p>
    </div>
</div>
<div class="body">
    <div class="row mb-4">
        <div class="col-md-6">
            <form name="formImagenes" id="formImagenes" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label>*Subir archivo</label>
                    <div class="custom-file">
                        <input type="file" id="imagen" accept=".jpg, .png" name="imagen" class="custom-file-input">
                        <label class="custom-file-label"></label>
                    </div>
                    <button type="submit" id="subirFile" class="btn btn-primary mt-4 float-right col-12"><i class="fa fa-save"></i>&nbsp;Guardar</button>
                </div>
                <input type="hidden" name="automovil_id" value="{{ isset($datos['id']) ? $datos['id'] : '' }}" />
            </form>
        </div>
        <div class="col-md-6">
            <h5>Listado de imagenes</h5>
            <table id="table-imagenes" class="table table-bordered table-striped" style="width:100%">
            </table>
        </div>
    </div>
    <div id="preview_image" style="display:none">
        <!-- <div class="row">
            <div class="col-10">
                <input type="range" id="size" value="1023" min="128" max="1024">
            </div>
            <div class="col-2">
                <button class="btn btn-primary btn-lg" type="button" id="resize">Aplicar</button>
            </div>
        </div> -->
        <div class="row">
            <!-- <div class="col-12">
                <img style="display:none" id="inputImage" crossorigin="anonymous" src="" width="1024" height="768" />
            </div> -->
            <div class="col-12">
                <h2>Imagen Previa</h2>
                <img id="outputImage" />
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>