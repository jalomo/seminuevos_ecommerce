<div class="col-md-12">
    <form name="formFunciones">
        <div class="form-group">
            <label>*Descripción funciones</label>
            <textarea type="text" class="form-control" maxlength="1000" style="min-height:300px" name="descripcion_funciones">{{ isset($funciones['descripcion_funciones']) ? $funciones['descripcion_funciones'] : '' }} </textarea>
            <small id="msg_descripcion_funciones" class="form-text text-danger"></small>
        </div>
        <div class="wpb_column vc_column_container vc_col-sm-3">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <h5 style="text-align: left" class="vc_custom_heading">Diseño interior</h5>
                    <?php foreach ($ca_diseno as $key => $diseno) { ?>
                        <div class="form-check mb-1">
                            <input class="form-check-input" type="checkbox" {{ isset($check_diseno) && in_array($diseno['id'], $check_diseno) ? 'checked="active"' : '' }} name="diseno[]" id="diseno{{ $diseno['id'] }}" value="{{ $diseno['id'] }}">
                            <label class="form-check-label">
                                {{ $diseno['nombre'] }}
                            </label>
                        </div>
                    <?php } ?>
                    <small id="msg_diseno[]" class="form-text text-danger"></small>
                </div>
            </div>
        </div>
        <div class="wpb_column vc_column_container vc_col-sm-3">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <h5 style="text-align: left" class="vc_custom_heading">Caracteristicas adicionales</h5>
                    <?php foreach ($ca_caracteristicas_adiccionales as $key => $adiccionales) { ?>
                        <div class="form-check mb-1">
                            <input class="form-check-input" type="checkbox" {{ isset($check_adiccional) && in_array($adiccionales['id'], $check_adiccional) ? 'checked="active"' : '' }} name="adiccionales[]" id="adiccionales{{ $adiccionales['id'] }}" value="{{ $adiccionales['id'] }}">
                            <label class="form-check-label">
                                {{ $adiccionales['nombre'] }}
                            </label>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="wpb_column vc_column_container vc_col-sm-3">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <h5 style="text-align: left" class="vc_custom_heading">Características de exteriores</h5>
                    <?php foreach ($ca_caracteristicas_exteriores as $key => $exteriores) { ?>
                        <div class="form-check mb-1">
                            <input class="form-check-input" type="checkbox" {{ isset($check_exteriores) && in_array($exteriores['id'], $check_exteriores) ? 'checked="active"' : '' }} name="exteriores[]" id="exteriores{{ $exteriores['id'] }}" value="{{ $exteriores['id'] }}">
                            <label class="form-check-label">
                                {{ $exteriores['nombre'] }}
                            </label>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="wpb_column vc_column_container vc_col-sm-3">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <h5 style="text-align: left" class="vc_custom_heading">Características de seguridad</h5>
                    <?php foreach ($ca_caracteristicas_seguridad as $key => $seguridad) { ?>
                        <div class="form-check mb-1">
                            <input class="form-check-input" type="checkbox" {{ isset($check_seguridad) && in_array($seguridad['id'], $check_seguridad) ? 'checked="active"' : '' }} name="seguridad[]" id="seguridad{{ $seguridad['id'] }}" value="{{ $seguridad['id'] }}">
                            <label class="form-check-label">
                                {{ $seguridad['nombre'] }}
                            </label>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <input type="hidden" name="automovil_id" value="{{ isset($datos['id']) ? $datos['id'] : '' }}" />
        <input type="hidden" name="funciones_id" value="{{ isset($funciones['id']) ? $funciones['id'] : '' }}" />
    </form>
    <div class="float-right mt-3">
        @if (isset($funciones['id']))
        <button type="button" id="guardarFunciones" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;Editar</button>
        @else
        <button type="button" id="guardarFunciones" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;Guardar</button>
        @endif
    </div>
</div>