@layout('templates/layout_admin')
@section('css')
<link rel="stylesheet" href="{{ base_url('assets/components/DataTables/media/css/jquery.dataTables.min.css')}}">
<style>
    .car-listing-tabs-unit .car-listing-top-part:before {
        background-color: #0e3256;
        margin-top: -25px;
    }

    .car-listing-tabs-unit img {
        width: auto !important;
    }
</style>
@endsection

@section('contenido')

<div class="car-listing-tabs-unit listing-cars-id-30554">
    <div class="car-listing-top-part">
        <div class="title">
            <h1 style="font-size: 36px;"><span style="color: #ffffff;">Información </span> <span class="stm-base-color">automovil</span></h1>
        </div>
        <div class="stm-listing-tabs">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link {{ isset($tab) && $tab == 1 ? 'active' : '' }}" data-toggle="tab" href="#principal" role="tab" aria-controls="principal" aria-selected="false">Caracteristicas principales</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ isset($tab) && $tab == 2 ? 'active' : '' }}" data-toggle="tab" href="#tecnica" role="tab" aria-controls="tecnica" aria-selected="false">Información técnica</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ isset($tab) && $tab == 3 ? 'active' : '' }}" data-toggle="tab" href="#funciones" role="tab" aria-controls="funciones" aria-selected="false">Funciones extras</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ isset($tab) && $tab == 4 ? 'active' : '' }}" data-toggle="tab" href="#imagenes" role="tab" aria-controls="imagenes" aria-selected="false">Imagenes</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="tab-content panel-theme" id="myTabContent">
        <div class="tab-pane {{ isset($tab) && $tab == 1 ? 'in active' : '' }}" id="principal">
            @include('autos/form')
            <div class="float-right mt-3">
                <a href="{{ site_url('administrador/autos/index') }}" class="btn btn-dark"><i class="fa fa-arrow-left"></i>&nbsp;Regresar</a>
                <button type="button" id="guardar" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;Editar</button>
            </div>
        </div>
        <div class="tab-pane {{ isset($tab) && $tab == 2 ? 'in active' : '' }}" id="tecnica">
            @include('autos/tabs/informacion_tecnica_vehiculo')
        </div>
        <div class="tab-pane {{ isset($tab) && $tab == 3 ? 'active' : '' }}" id="funciones" role="tabpanel">
            @include('autos/tabs/funciones_vehiculo')
        </div>
        <div class="tab-pane {{ isset($tab) && $tab == 4 ? 'active' : '' }}" id="imagenes" role="tabpanel">
            @include('autos/tabs/subida_archivos')
        </div>
        <div class="clearfix"></div>
    </div>
    <!--cont-->
</div>
@endsection

@section('scripts')

<script type="text/javascript" src="{{ base_url('assets/components/DataTables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ base_url('assets/components/resize/resize.js') }}"></script>
<script type="text/javascript" src="{{ base_url('assets/scripts/administrador/autos/agregar.js') }}"></script>

<script type="text/javascript">
    $('.custom-file-input').on('change', function() {
        filePreview(this);
        var fileName = document.getElementById("imagen").files[0].name;
        $('.custom-file-label').addClass("selected").html(fileName);
    })

    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(input.files[0]);
            reader.onload = function(e) {
                $('#outputImage + img').remove();
                $('#outputImage').attr("src", e.target.result);
                $("#preview_image").show();
            }
        }

    }
    $("form#formImagenes").on("submit", function(e) {
        e.preventDefault();
        let name_image = utils.isDefined(document.getElementById("imagen").files[0]) ? document.getElementById("imagen").files[0].name : '';
        var canvas = $('#outputImage').attr("src");
        var sendData = $('form[name="formImagenes"]').serializeArray();
        sendData.push({
            name: 'base64',
            value: canvas,
        });
        sendData.push({
            name: 'name_imagen',
            value: name_image,
        });
        $.ajax({
            url: PATH + '/administrador/autos/save_imagenes',
            type: "post",
            dataType: "json",
            data: sendData,
            success: function(result, status, xhr) {
                $('.custom-file-label').removeClass("selected").html('');
                if (result.estatus == 'error') {
                    if (result.mensaje) {
                        message.error(result.mensaje, true);
                    } else {
                        errors.mostrar(result.info);
                    }
                } else {
                    if (result.data) {
                        $("#preview_image").hide();
                        $("form#formImagenes")[0].reset();
                        message.success(result.mensaje, function() {
                            app.getBusquedaImagenes();
                        });
                    }
                }
            }
        })
    });
</script>
@endsection