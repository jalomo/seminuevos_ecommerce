@layout('templates/layout_admin')
@section('css')
<link rel="stylesheet" href="{{ base_url('assets/components/DataTables/media/css/jquery.dataTables.min.css')}}">
@endsection
@section('contenido')

<div class="header">
    <h2>Ofertados realizadas</h2>
</div>
<div class="row mt-5 mb-3">
    <div class="col-sm-12">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop" >Agregar Oferta</button>
    </div>
</div>
<div class="body">
    <table id="table-autos-ofertados" class="table table-bordered table-striped" style="width:100%">       
    </table>
</div>


@endsection

<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog" style="margin-top: 200px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Agregar Oferta</h5>
      </div>
      <div class="modal-body">
        
        <form id="form_agregar_oferta">
            <div class="form-group">
                <label for="exampleFormControlInput1">Monto a ofertar:</label>
                <input type="number" step="0.01" class="form-control" name="monto_oferta" >

                <input type="hidden" class="form-control" name="auto_id" value="{{ $data['id']}}" >
                <input type="hidden" class="form-control" name="agencia_id" value="{{ $data['agencia_id']}}" >
                <input type="hidden" class="form-control" name="usuario_id" value="{{ $data['usuario_id']}}" >
                

            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Comentario:</label>
                <textarea class="form-control" name="comentario" rows="3"></textarea>
            </div>
        </form>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary">Ofertar</button>
      </div>
    </div>
  </div>
</div>

@section('scripts')

<script type="text/javascript" src="{{ base_url('assets/components/DataTables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ base_url('assets/scripts/administrador/autos_ofertados/ofertas.js') }}"></script>

<script>

</script>

@endsection