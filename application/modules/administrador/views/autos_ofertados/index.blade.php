@layout('templates/layout_admin')
@section('css')
<link rel="stylesheet" href="{{ base_url('assets/components/DataTables/media/css/jquery.dataTables.min.css')}}">
@endsection
@section('contenido')

<div class="header">
    <h2>Listado autos ofertados</h2>
</div>
<div class="body">
    <table id="table-autos-ofertados" class="table table-bordered table-striped" style="width:100%">       
    </table>
</div>

@endsection

@section('scripts')

<script type="text/javascript" src="{{ base_url('assets/components/DataTables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ base_url('assets/scripts/administrador/autos_ofertados/index.js') }}"></script>
@endsection