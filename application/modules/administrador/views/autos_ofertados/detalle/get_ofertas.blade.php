<div>
    <div class=" mt-4">
        <div class="">
            <div class="">
                <center><h4>Ofertas obtenidas</h4></center>
                <?php 
                if(is_array($data['ofertas'])){
                    foreach ($data['ofertas'] as $key => $value) {
                        ?>

                <div class="row mb-0 mt-0">

                    <div class="col-sm-12">
                        <div class="row p-3 mb-2">
                            <div class="col-sm-12">
                                <div class="col-sm-3"><b for="" class="">Sucursal:</b></div>
                                <div class="col-sm-9"><?php echo ($value['sucursal']['nombre']) ?></div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-3"><b for="" class="">Agencia:</b></div>
                                <div class="col-sm-9"><?php echo ($value['agencia']['nombre']) ?></div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-3"><b for="" class="">Monto ofertado:</b></div>
                                <div class="col-sm-9">
                                <table class="table table-sm  table-striped " style="width:100%">
                                    <tr>
                                    <?php if(strlen($value['monto_ofertado'])>0 && $value['monto_ofertado'] != 0){ ?>
                                        <td class="align-bottom">Compra</td>
                                    <?php } ?>
                                    <?php if(strlen($value['monto_compra_nuevo'])>0 && $value['monto_compra_nuevo'] != 0){ ?>
                                        <td class="align-bottom">Comprando un auto nuevo</td>
                                    <?php } ?>
                                    <?php if(strlen($value['monto_compra_seminuevo'])>0 && $value['monto_compra_seminuevo'] != 0){ ?>
                                        <td class="align-bottom">Comprando un auto seminuevo</td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                    <?php if(strlen($value['monto_ofertado'])>0 && $value['monto_ofertado'] != 0){ ?>
                                        <td>$ <?php echo utils::formatMoney($value['monto_ofertado']) ?></td>
                                    <?php } ?>
                                    <?php if(strlen($value['monto_compra_nuevo'])>0 && $value['monto_compra_nuevo'] != 0){ ?>
                                        <td>$ <?php echo utils::formatMoney($value['monto_compra_nuevo']) ?></td>
                                    <?php } ?>
                                    <?php if(strlen($value['monto_compra_seminuevo'])>0 && $value['monto_compra_seminuevo'] != 0){ ?>
                                        <td>$ <?php echo utils::formatMoney($value['monto_compra_seminuevo']) ?></td>   
                                    <?php } ?>
                                    </tr>
                                </table>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-3"><b for="" class="">Fecha:</b></div>
                                <div class="col-sm-9"><?php echo utils::aFecha($value['created_at'],false); ?></div>
                            </div>

                            <div class="col-sm-12">
                                <div class="col-sm-3"><b class="">Estatus:</b></div>
                                <div class="col-sm-9"><?php 
                                switch ($value['estatus_id']) {
                                    case 1:
                                    case '1':
                                        echo 'Nueva';
                                        break;
                                    case '2':
                                        echo 'Aceptada';
                                        break;
                                    case 3:
                                        echo 'Rechazada';
                                        break;
                                } ?></div>
                            </div>

                            <div class="col-sm-12">
                                <div class="col-sm-3"><b for="" class="">Comentario:</b></div>
                                <div class="col-sm-9">{{ $value['comentarios']}}</div>
                            </div>
                            <div class="col-sm-12">
                            <?php if($value['estatus_id'] == 1){ ?>
                                <div class="float-right" role="group" aria-label="First group">
                                    <button type="button" onclick="app.aceptar({{$value['id']}});" class="btn btn-success btn-sm">Aceptar</button>
                                    <button type="button" onclick="app.rechazar({{$value['id']}});" class="btn btn-danger btn-sm">Rechazar</button>
                                </div>
                            <?php } ?>
                            </div>
                            <hr />
                        </div>
                    </div>
                </div>

                <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>