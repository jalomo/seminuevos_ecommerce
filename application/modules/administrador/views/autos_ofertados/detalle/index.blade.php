@layout('templates/layout_admin')
<!-- <style>
    .carousel-item,
    .carousel-inner,
    .carousel-inner img {
        height: 100%;
        width: 100%;
    }

    .carousel-item {
        text-align: center;
    }

    .carousel {
        height: 500px;
    }
</style> -->

@section('contenido')
<script>
    var automovil_id = "{{ $id }}";
</script>
<div class="stm-single-car-page" style="background-position: 0px -136px;">
    <div class="container_">
    <div class="mt-4 mb-4">
            <div class="stm-vc-single-car-content-left wpb_column vc_column_container vc_col-sm-12 vc_col-lg-9">
                <div class="vc_column-inner_">
                    <div class="wpb_wrapper_">
                        <h1 class="title h2">{{ $data['detalle']['marca'].' '.$data['detalle']['modelo'].', '.$data['detalle']['tipo_trasmision'].' '.$data['detalle']['tipo_traccion']  }}</h1>
                        <div class="row">
                            <div class="image">
                                <div id="carouselInterval_4" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselInterval_4" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselInterval_4" data-slide-to="1" class=""></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        @if (isset($data['banners']))
                                        @foreach ($data['banners'] as $key => $item)
                                        <div class="carousel-item {{ $key == 0 ? 'active' : ''}}">
                                            <img class="img-fluid d-block" src="{{ base_url('compra_autos/' . $item['file_imagen']) }}" alt="Third slide">
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselInterval_4" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselInterval_4" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="stm-vc-single-car-sidebar-right wpb_column vc_column_container vc_col-sm-12 vc_col-lg-3">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="single-car-prices">
                            <div class="single-regular-price text-center">
                                <span class="h3">$ {{ utils::formatMoney($detalle['precio'])}}</span>
                            </div>
                        </div>
                        <div class="single-car-data">
                            <table>
                                <tbody>
                                    <tr>
                                        <td class="t-label">Marca</td>
                                        <td class="t-value h6">{{ $detalle['marca'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="t-label">Modelo</td>
                                        <td class="t-value h6">{{ $detalle['modelo'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="t-label">Tipo trasmisión</td>
                                        <td class="t-value h6">{{ $detalle['tipo_trasmision'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="t-label">Tipo carroceria</td>
                                        <td class="t-value h6">{{ $detalle['tipo_carroceria'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="t-label">Kilometraje</td>
                                        <td class="t-value h6">{{ $detalle['kilometraje']}}</td>
                                    </tr>
                                    <tr>
                                        <td class="t-label">Tipo de combustible</td>
                                        <td class="t-value h6">{{ $detalle['combustible']}}</td>
                                    <tr>
                                        <td class="t-label">Año</td>
                                        <td class="t-value h6">{{ $detalle['anio']}}</td>
                                    </tr>
                                    <tr>
                                        <td class="t-label">Transmición</td>
                                        <td class="t-value h6">{{ $detalle['tipo_trasmision']}}</td>
                                    </tr>
                                    <tr>
                                        <td class="t-label">Conducir</td>
                                        <td class="t-value h6">{{ $detalle['tipo_traccion'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="t-label">Color exterior</td>
                                        <td class="t-value h6">{{ $detalle['color_exterior']}}</td>
                                    </tr>
                                    <tr>
                                        <td class="t-label">Color interior</td>
                                        <td class="t-value h6">{{ $detalle['color_interior']}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="single-car-mpg heading-font">
                            <div class="row">
                                <div class="col-2">
                                    <i class="fa fa-user fa-2x text-center text-orange"></i>
                                </div>
                                <div class="col-10">
                                    <span"><b><?php echo $propietario['nombre'] . ' ' . $propietario['apellido_paterno'] . ' ' . $propietario['apellido_materno']; ?></b></span><br />
                                        <span><b>Teléfono :</b> <?php echo $propietario['telefono']; ?> </span><br />
                                        <span><b>Correo electrónico :</b> <?php echo $propietario['email']; ?> </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="sep10 clearfix"></div>
        <?php if ($detalle['estatus_id'] == 1) { ?>
            <div class="clearfix mt-2">
                <a type="button" onclick="app.abrir_modal();" class="btn btn-primary btn-large"><i class="fa fa-share"></i> Ofertar por el auto</a>
            </div>
        <?php } else {  ?>
            <div class="clearfix"></div>
            <div class="alert alert-info mt-5" role="alert">
                <p><i class="fa fa-info-circle fa-2x"></i> El propietario <b><?php echo $propietario['nombre'] . ' ' . $propietario['apellido_paterno'] . ' ' . $propietario['apellido_materno']; ?></b> ha vendido su automovil</p>
            </div> 
        <?php } ?>
        <div class="row">
            <div class="col-sm-<?php echo ($data['ofertas'] == false) ? 12 : 6 ?>">
                <div class="single-car-mpg heading-font mt-4">
                    <div class="">
                        <div class="row">
                            <div class="col-sm-12">
                                <h5>Descripción</h5>
                                {{ $detalle['descripcion']}}
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div style="display:<?php echo ($data['ofertas'] != false) ? 'block' : 'none' ?>" class="col-sm-6">
                <div class="single-car-mpg heading-font mt-4">
                    <div class="">
                        <div class="">
                            <h5>Ofertas realizadas</h5>
                            <?php
                            if (is_array($data['ofertas'])) {
                                foreach ($data['ofertas'] as $key => $value) {
                            ?>

                                    <?php
                                    $color = '';
                                    switch ($value['estatus_id']) {
                                        case 1:
                                        case '1':
                                            break;
                                        case '2':
                                            $color = 'background-color:bdecb6;';
                                            break;
                                        case 3:
                                            break;
                                    } ?>
                                    <div class="row mb-1 mt-1 ml-1 mr-1" style="{{$color}}">
                                        <?php if(strlen($value['monto_ofertado'])>0 && $value['monto_ofertado'] != 0){ ?>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b for="" class="small">Monto a ofertar (compra):</b></div>
                                            <div class="col-sm-7">$ <?php echo utils::formatMoney($value['monto_ofertado']) ?></div>
                                        </div>
                                        <?php } ?>

                                        <?php if(strlen($value['monto_compra_nuevo'])>0 && $value['monto_compra_nuevo'] != 0){ ?>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b for="" class="small">Monto a ofertar (comprando auto nuevo):</b></div>
                                            <div class="col-sm-7">$ <?php echo utils::formatMoney($value['monto_compra_nuevo']) ?></div>
                                        </div>
                                        <?php } ?>

                                        <?php if(strlen($value['monto_compra_seminuevo'])>0 && $value['monto_compra_seminuevo'] != 0){ ?>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b for="" class="small">Monto a ofertar (comprando auto seminuevo):</b></div>
                                            <div class="col-sm-7">$ <?php echo utils::formatMoney($value['monto_compra_seminuevo']) ?></div>
                                        </div>
                                        <?php } ?>

                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b for="" class="small">Fecha:</b></div>
                                            <div class="col-sm-7"><?php echo utils::aFecha($value['created_at'], false); ?></div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b for="" class="small">Estatus:</b></div>
                                            <div class="col-sm-7"><?php
                                                                    switch ($value['estatus_id']) {
                                                                        case 1:
                                                                        case '1':
                                                                            echo 'Enviada';
                                                                            break;
                                                                        case '2':
                                                                            echo 'Aceptada';
                                                                            break;
                                                                        case 3:
                                                                            echo 'Rechazada';
                                                                            break;
                                                                    } ?></div>
                                        </div>

                                        <div class="col-sm-12 mb-3">
                                            <div class="col-sm-5"><b for="" class="small">Comentario:</b></div>
                                            <div class="col-sm-7" class="small">{{ $value['comentarios']}}</div>
                                        </div>
                                        <hr />
                                    </div>

                            <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @endsection

    <!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Agregar Oferta</h5>
                </div>
                <div class="modal-body">

                    <form id="form_agregar_oferta">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Monto a ofertar (compra):</label>
                            <input type="number" step="0.01" value="0" class="form-control" name="monto_oferta">
                            <small id="msg_monto_oferta" class="form-text text-danger"></small>

                            <input type="hidden" class="form-control" name="auto_id" value="{{ $data['id']}}">
                            <input type="hidden" class="form-control" name="agencia_id" value="{{ $data['agencia_id']}}">
                            <input type="hidden" class="form-control" name="usuario_id" value="{{ $data['usuario_id']}}">


                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Monto a ofertar (comprando auto nuevo):</label>
                            <input type="number" step="0.01" value="0" class="form-control" name="monto_compra_nuevo">
                            <small id="msg_monto_compra_nuevo" class="form-text text-danger"></small>
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlInput1">Monto a ofertar (comprando auto seminuevo):</label>
                            <input type="number" step="0.01" value="0" class="form-control" name="monto_compra_seminuevo">
                            <small id="msg_monto_compra_seminuevo" class="form-text text-danger"></small>
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Comentario:</label>
                            <textarea class="form-control" name="comentario" rows="3"></textarea>
                            <small id="msg_comentario" class="form-text text-danger"></small>
                        </div>
                    </form>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" onclick="app.guardar_modal();" class="btn btn-primary">Ofertar</button>
                </div>
            </div>
        </div>
    </div>


    @section('scripts')
    <script type="text/javascript" src="{{ base_url('assets/scripts/administrador/autos_ofertados/detalle.js') }}"></script>
    @endsection