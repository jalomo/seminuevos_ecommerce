@layout('templates/layout_admin')
@section('css')
<link rel="stylesheet" href="{{ base_url('assets/components/DataTables/media/css/jquery.dataTables.min.css')}}">
@endsection
@section('contenido')

<div class="header">
    <h2>Citas solicitadas</h2>
</div>
<div class="body">
    <!-- <div class="float-right mb-3">
        <a href="{{ site_url('administrador/contacto/ver') }}"  class="btn btn-primary"> <i class="fa fa-car"></i>&nbsp;Agregar</a>
    </div> -->
        <table id="table-contactos" class="table table-bordered table-striped" style="width:100%"></table>
</div>

@endsection

<div class="modal" id="modal_contacto" tabindex="-1">
</div>


@section('scripts')
<script type="text/javascript" src="{{ base_url('assets/components/DataTables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ base_url('assets/components/momentjs/moment-with-locales.min.js') }}"></script>
<link href="{{ base_url('assets/components/pace/themes/red/pace-theme-minimal.css') }}" rel="stylesheet" />
<script src="{{ base_url('assets/components/pace/pace.min.js') }}"></script>
<script type="text/javascript" src="{{ base_url('assets/scripts/administrador/contacto/index.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_gbAjK7KGkfidjIi0iLFqD39VWbhtXcs&callback=initMap&libraries=&v=weekly" defer></script>
@endsection