  <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title">Contacto</h5>
              <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> -->
          </div>
          <div class="modal-body">

              <form>
                  <div class="row">
                      <div class="col-sm-6">
                          <div class="form-group ">
                              <label for="nombre" class="col-form-label">Donde desea tomar su prueba de manejo:</label>
                              <input type="text" readonly class="form-control-plaintext"
                                  value="{{ $contacto['tipo_ubicacion'] }}">
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="form-group ">
                              <label for="nombre" class="col-form-label">Sucursal / Agencia:</label>
                              <input type="text" readonly class="form-control-plaintext"
                                  value="{{ $auto['sucursal'] }}/{{ $auto['agencia'] }}">
                          </div>
                      </div>
                      <div class="col-sm-12">
                          <div class="form-group ">
                              <label for="" class="col-form-label">Automóvil:</label>

                              <input type="text" readonly class="form-control-plaintext"
                                  value="{{ $auto['marca'] }} {{ $auto['modelo'] }} {{ $auto['tipo_carroceria'] }} {{ $auto['anio'] }}">

                          </div>
                      </div>
                      <div class="col-sm-4">
                          <div class="form-group ">
                              <label for="nombre" class="col-form-label">Nombre:</label>
                              <input type="text" readonly class="form-control-plaintext" id="nombre"
                                  value="{{ $contacto['nombre'] }}">
                          </div>
                      </div>
                      <div class="col-sm-4">
                          <div class="form-group ">
                              <label for="nombre" class="col-form-label">Primer apellido:</label>
                              <input type="text" readonly class="form-control-plaintext" id="nombre"
                                  value="{{ $contacto['apellido1'] }}">
                          </div>
                      </div>
                      <div class="col-sm-4">
                          <div class="form-group ">
                              <label for="nombre" class="col-form-label">Segundo apellido:</label>
                              <input type="text" readonly class="form-control-plaintext" id="nombre"
                                  value="{{ $contacto['apellido2'] }}">
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="form-group ">
                              <label for="" class="col-form-label">Teléfono:</label>
                              <input type="text" readonly class="form-control-plaintext"
                                  value="{{ $contacto['telefono'] }}">
                          </div>
                      </div>
                      <div class="col-sm-6">

                          <div class="form-group ">
                              <label for="" class="col-form-label">Correo electrónico:</label>
                              <input type="text" readonly class="form-control-plaintext"
                                  value="{{ $contacto['correo_electronico'] }}">
                          </div>
                      </div>
                      <div class="col-sm-12">
                          <div class="form-group ">
                              <label for="" class=" col-form-label">Domicilio:</label>
                              <textarea readonly rows="" cols="2"
                                  class="form-control">{{ $contacto['domicilio'] }}</textarea>

                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="form-group ">
                              <label for="" class=" col-form-label">Licencia para conducir (Delantera):</label>
                              <a class="btn btn-primary btn-sm btn-block" onclick="app.abrir_imagen(this);"
                                  imagen="{{ base64_encode(base_url('documentos_prueba_manejo/'.$contacto['imagen_licencia'] ))}}"
                                  target="_blank">Abrir</a>


                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="form-group ">
                              <label for="" class=" col-form-label">Identificación oficial (Delantera):</label>
                              <a class="btn btn-primary btn-sm btn-block" onclick="app.abrir_imagen(this);"
                                  imagen="{{ base64_encode(base_url('documentos_prueba_manejo/'.$contacto['imagen_identificacion'] ))}}"
                                  target="_blank">Abrir</a>


                          </div>
                      </div>
                      

                      <div class="col-sm-6">
                          <div class="form-group ">
                              <label for="" class=" col-form-label">Licencia para conducir (Trasera):</label>
                              <a class="btn btn-primary btn-sm btn-block" onclick="app.abrir_imagen(this);"
                                  imagen="{{ base64_encode(base_url('documentos_prueba_manejo/'.$contacto['imagen_licencia2'] ))}}"
                                  target="_blank">Abrir</a>


                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="form-group ">
                              <label for="" class=" col-form-label">Identificación oficial  (Trasera):</label>
                              <a class="btn btn-primary btn-sm btn-block" onclick="app.abrir_imagen(this);"
                                  imagen="{{ base64_encode(base_url('documentos_prueba_manejo/'.$contacto['imagen_identificacion2'] ))}}"
                                  target="_blank">Abrir</a>


                          </div>
                      </div>

                      <div class="col-sm-12">
                          <div class="form-group ">
                              <label for="" class=" col-form-label">Ubicación:</label>
                              <input type="hidden" readonly class="form-control-plaintext" id="coordenada_x"
                                  value="{{ $contacto['latitud'] }}">
                              <input type="hidden" readonly class="form-control-plaintext" id="coordenada_y"
                                  value="{{ $contacto['longitud'] }}">
                              <div id="map_prueba_manejo" style="height:300px; ">Cargando mapa...</div>


                          </div>

                      </div>

                  </div>




              </form>


          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          </div>
      </div>
  </div>