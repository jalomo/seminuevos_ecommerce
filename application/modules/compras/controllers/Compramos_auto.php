<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Compramos_auto extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $user_id = $this->input->get('user_id');
    $this->load->model('CaCompraAuto_model');
    $this->load->model('CaOfertasAuto_model');

    $autos = array();
    if ($user_id) {
      $autos = $this->CaCompraAuto_model->getFiltrado(array(
        'usuario_id' => $user_id
      ));

      if (is_array($autos)) {
        foreach ($autos as $key => $value) {
          $ofertas = $this->CaOfertasAuto_model->getAll(array(
            'automovil_id' => $value['id'],
          ));
          $autos[$key]['num_ofertas'] = (is_array($ofertas) > 0) ? count($ofertas) : 0;
        }
      }
    }
    $layout = $this->input->get('webapp') ? 'templates/layoutframe' : 'templates/layout';
    $data = array(
      'layout' => $layout,
      'titulo' => 'Te compramos tu auto',
      'subtitle' => 'Mis autos ofertados',
      'usuario_id' => $user_id,
      'autos' => $autos
    );
    $this->blade->render('/autos/index', $data);
  }

  public function agregar()
  {
    $layout = $this->input->get('webapp') ? 'templates/layoutframe' : 'templates/layout';
    $data = array(
      'layout' => $layout,
      'titulo' => 'Registrar auto',
      'usuario_id' => $this->input->get('user_id') ? $this->input->get('user_id') : 1
    );
    $this->blade->render('/autos/nuevo', $data);
  }

  public function getAll()
  {
    $this->load->model('CaCompraAuto_model');
    $data['data'] = $this->CaCompraAuto_model->getFiltrado(array(
      'usuario_id' => 1
    ));
    return print_r(json_encode($data));
  }

  public function save()
  {
    $this->load->model('CaCompraAuto_model');
    $this->load->library('form_validation');
    $this->form_validation->set_rules('descripcion', 'Descripción', 'trim|required');
    $this->form_validation->set_rules('tipo_carroceria_id', 'Tipo carroceria', 'trim|required');
    $this->form_validation->set_rules('marca_id', 'Marca', 'trim|required');
    $this->form_validation->set_rules('modelo_id', 'Modelo', 'trim|required');
    $this->form_validation->set_rules('anio', 'Año', 'trim|required');
    $this->form_validation->set_rules('tipo_transmision_id', 'Tipo transmisión', 'trim|required');
    $this->form_validation->set_rules('kilometraje', 'Kilometraje', 'trim|required');
    $this->form_validation->set_rules('color_exterior_id', 'Color exterior', 'trim|required');
    $this->form_validation->set_rules('color_interior_id', 'Color interior', 'trim|required');
    $this->form_validation->set_rules('tipo_combustible_id', 'Tipo combustible', 'trim|required');
    $this->form_validation->set_rules('tipo_traccion_id', 'Tipo tracción', 'trim|required');
    $this->form_validation->set_rules('precio', 'Precio', 'trim|required');
    $this->form_validation->set_rules('usuario_id', 'Usuario', 'trim|required');

    $compra_auto_id = $this->input->post('compra_auto_id');
    if ($this->form_validation->run() == true) {
      $additional_data = array(
        'descripcion' => $this->input->post('descripcion'),
        'tipo_carroceria_id' => $this->input->post('tipo_carroceria_id'),
        'marca_id' => $this->input->post('marca_id'),
        'modelo_id' => $this->input->post('modelo_id'),
        'anio' => $this->input->post('anio'),
        'tipo_transmision_id' => $this->input->post('tipo_transmision_id'),
        'kilometraje' => $this->input->post('kilometraje'),
        'color_exterior_id' => $this->input->post('color_exterior_id'),
        'color_interior_id' => $this->input->post('color_interior_id'),
        'tipo_combustible_id' => $this->input->post('tipo_combustible_id'),
        'tipo_traccion_id' => $this->input->post('tipo_traccion_id'),
        'precio' => $this->input->post('precio'),
        'usuario_id' => $this->input->post('usuario_id')
      );
      if ($compra_auto_id != null && $compra_auto_id >= 1) {
        $data['compra_auto_id'] = $compra_auto_id;
        $data['data'] = $this->CaCompraAuto_model->update($compra_auto_id, $additional_data);
        $data['mensaje'] = 'Automovil actualizado correctamente';
      } else {
        $additional_data['estatus_id'] = 0;
        $data['data'] = $this->CaCompraAuto_model->insert($additional_data);
        $data['compra_auto_id'] = $data['data'];
        $data['mensaje'] = 'Su automovil ha sido registrado correctamente';
      }
    } else {
      $data['estatus'] = 'error';
      $data['info'] = $this->form_validation->error_array();
    }

    return print_r(json_encode($data));
  }

  public function continuar()
  {
    $id = ($this->input->get('compra_auto_id'));
    $tab = $this->input->get('tab');

    if (empty($id)) {
      redirect('compras/compramos_autos/index');
    }

    $this->load->model('CaCompraAuto_model');

    $datos = $this->CaCompraAuto_model->get(array('id' => $id));
    $data = [
      'titulo' => 'Editar Datos Automovil',
      'datos' => $datos,
      'tab' => $tab,
    ];
    $this->blade->render('compras/autos/continuar', $data);
  }

  public function save_imagenes()
  {
    $this->load->model("DeImagenesCompraAutos_model");
    $this->load->library('form_validation');
    $this->form_validation->set_rules('compra_auto_id', 'Auto', 'trim|required');
    $this->form_validation->set_rules('tipo_imagen', 'Tipo imagen', 'trim|required');
    if ($this->form_validation->run() == true) {

      if (!is_array($_FILES["imagen"]["error"]) && $_FILES["imagen"]["error"] == 0) {
        $fileTmpPath = $_FILES['imagen']['tmp_name'];
        $fileName = $_FILES['imagen']['name'];
        $fileNameCmps = explode(".", $fileName);
        $fileExtension = strtolower(end($fileNameCmps));

        $newFileName = md5(time() . $fileName) . '.' . $fileExtension;

        $allowedfileExtensions = array('jpg', 'gif', 'png');
        if (in_array($fileExtension, $allowedfileExtensions)) {
          $uploadFileDir = './compra_autos/';
          $dest_path = $uploadFileDir . $newFileName;

          if (move_uploaded_file($fileTmpPath, $dest_path)) {
            $additional_data = array(
              'compra_auto_id' => $this->input->post('compra_auto_id'),
              'tipo_imagen' => $this->input->post('tipo_imagen'),
              'nombre_imagen' => $fileName,
              'file_imagen' => $newFileName
            );
            $data['data'] = $this->DeImagenesCompraAutos_model->insert($additional_data);
            $data['mensaje'] = 'Imagen guardada correctamente';
          } else {
            $message = 'Hubo algún error al mover el archivo al directorio de carga. Asegúrese de que el servidor web pueda escribir en el directorio de carga';
            $data['estatus'] = 'error';
            $data['mensaje'] = $message;
          }
        } else {
          $data['estatus'] = 'error';
          $data['mensaje'] = 'Subida fallida. Extensiones permitidas : ' . implode(',', $allowedfileExtensions);
        }
      } else {
        $message = 'Falta adjuntar el archivo';
        $data['estatus'] = 'error';
        $data['mensaje'] = $message;
      }
    } else {
      $data['estatus'] = 'error';
      $data['info'] = $this->form_validation->error_array();
    }
    return print_r(json_encode($data));
  }

  public function publicar()
  {
    $compra_auto_id = $this->input->get('compra_auto_id');
    $additional_data = array(
      'estatus_id' => 1,
      'fecha_actualizacion' => date("Y-m-d H:i:s")
    );
    $data['compra_auto_id'] = $compra_auto_id;
    $this->load->model('CaCompraAuto_model');
    $data['data'] = $this->CaCompraAuto_model->update($compra_auto_id, $additional_data);
    $data['mensaje'] = 'Su Automovíl ha sido publicado';
   
    return print_r(json_encode($data));
  }

  public function getImagenesByAuto()
  {
    $compra_auto_id = $this->input->get('compra_auto_id');
    $this->load->model('DeImagenesCompraAutos_model');
    $data['data'] = $this->DeImagenesCompraAutos_model->getAll(array('compra_auto_id' => $compra_auto_id));
    $count_int = 0;
    $count_ext = 0;
    if(is_array($data['data']) && count($data['data'])>0){
      foreach ($data['data'] as $key => $value) {
        if($value['tipo_imagen'] == 1){
          $count_ext ++;
        }else{
          $count_int ++;
        }
      }
    }
    $data['count_ext'] = $count_ext;
    $data['count_int'] = $count_int;
    return print_r(json_encode($data));
  }

  public function deleteFile()
  {
    $file_id = $this->input->get('file_id');
    $this->load->model('DeImagenesCompraAutos_model');
    $eliminado = $this->DeImagenesCompraAutos_model->delete(array('id' => $file_id));
    if ($eliminado) {
      $data['data'] = $eliminado;
      $data['mensaje'] = "Registro eliminado correctamente";
    } else {
      $data['estatus'] = 'error';
      $data['mensaje'] = 'Ocurrio un error al eliminar el archivo';
    }

    return print_r(json_encode($data));
  }
}
