<div class="header">
    <h2>Imagenes automovil</h2>
</div>
<div class="alert alert-warning" role="alert">
    <p>Favor de adjuntar como mínimo <b>4 imagenes externas y 4 imagenes internas</b> de su vehículo, se sugiere una resolución minima a 1024 por 768 pixeles </p>
</div>
<div class="body">
    <div class="row mb-4">
        <div class="col-md-6">
            <form id="formImagenes" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label>*Subir archivo</label>
                    <div class="custom-file">
                        <input type="file" id="imagen" accept="image/jpg" name="imagen" class="custom-file-input">
                        <label class="custom-file-label"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label>*Tipo imagen</label>
                    <select name="tipo_imagen" id="tipo_imagen" class="form-control">
                        <option value=""></option>
                        <option value="1">Exterior</option>
                        <option value="2">Interior</option>
                    </select>
                </div>
                <input type="hidden" name="compra_auto_id" value="{{ isset($datos['id']) ? $datos['id'] : '' }}" />
                <button type="submit" id="subirFile" class="btn btn-primary mt-4 float-right col-12"><i class="fa fa-save"></i>&nbsp;Guardar imagen</button>

            </form>
        </div>
        <div class="col-md-6">
            <h5>Imagenes automovil</h5>
            <table id="table-imagenes" class="table table-bordered table-striped" style="width:100%">
            </table>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-sm-4"> 
        &nbsp;
        </div>
        <div class="col-sm-4"> 
            <button type="button" id="publicar_anuncio" onclick="app.publicar();" style="display:none;" class="btn btn-success mt-4 float-right col-12"><i class="fa fa-save"></i>&nbsp;Publicar</button>
        </div>
    </div>
    <div class="clearfix"></div>
</div>