@layout('templates/layout')
@section('css')
<link rel="stylesheet" href="{{ base_url('assets/components/DataTables/media/css/jquery.dataTables.min.css')}}">
<style>
    .car-listing-tabs-unit .car-listing-top-part:before {
        background-color: #0e3256;
        margin-top: -25px;
    }
</style>
@endsection

@section('contenido')

<div class="car-listing-tabs-unit listing-cars-id-30554">
    <div class="car-listing-top-part">
        <div class="title">
            <h1 style="font-size: 36px;"><span style="color: #ffffff;">Información </span> <span class="stm-base-color">automovil</span></h1>
        </div>
        <div class="stm-listing-tabs">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#principal" role="tab" aria-controls="principal" aria-selected="false">Caracteristicas principales</a>
                </li>
                <li class="nav-item disabled">
                    <a class="nav-link">Imagenes</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="tab-content panel-theme" id="myTabContent">
        <div class="tab-pane in active" id="principal">
            @include('autos/form')
            <div class="float-right mt-3">
                <a href="{{ site_url('compras/compramos_auto/index') }}" class="btn btn-dark"><i class="fa fa-arrow-left"></i>&nbsp;Regresar</a>
                <button type="button" id="guardar" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;Guardar</button>
            </div>
        </div>
        <div class="tab-pane disabled" id="imagenes" role="tabpanel">
        </div>
        <div class="clearfix"></div>
    </div>
    <!--cont-->
</div>
@endsection

@section('scripts')

<script type="text/javascript" src="{{ base_url('assets/components/DataTables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ base_url('assets/scripts/compras/compramos_auto/agregar.js') }}"></script>
@endsection