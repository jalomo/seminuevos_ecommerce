@layout('templates/layout')
@section('css')
<link rel="stylesheet" href="{{ base_url('assets/components/DataTables/media/css/jquery.dataTables.min.css')}}">
<style>
    .car-listing-tabs-unit .car-listing-top-part:before {
        background-color: #0e3256;
        margin-top: -25px;
    }
</style>
@endsection

@section('contenido')

<div class="car-listing-tabs-unit listing-cars-id-30554">
    <div class="car-listing-top-part">
        <div class="title">
            <h1 style="font-size: 36px;"><span style="color: #ffffff;">Información </span> <span class="stm-base-color">automovil</span></h1>
        </div>
        <div class="stm-listing-tabs">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link {{ isset($tab) && $tab == 1 ? 'active' : '' }}" data-toggle="tab" href="#principal" role="tab" aria-controls="principal" aria-selected="false">Caracteristicas principales</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ isset($tab) && $tab == 2 ? 'active' : '' }}" data-toggle="tab" href="#imagenes_exteriores" role="tab" aria-controls="imagenes_exteriores" aria-selected="false">Imagenes automovil</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="tab-content panel-theme" id="myTabContent">
        <div class="tab-pane {{ isset($tab) && $tab == 1 ? 'in active' : '' }}" id="principal">
            @include('autos/form')

            <div class="col-12 text-right mt-3 mb-3">
                <a href="{{ site_url('compras/compramos_auto/index') }}" class="btn btn-dark"><i class="fa fa-arrow-left"></i>&nbsp;Regresar</a>
                <button type="button" id="guardar" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;Editar</button>
            </div>
        </div>
        <div class="tab-pane {{ isset($tab) && $tab == 2 ? 'active' : '' }}" id="imagenes_exteriores" role="tabpanel">
            @include('autos/tabs/imagenes_automovil')
            <div class="clearfix"></div>

        </div>
    </div>
    <!--cont-->
</div>
@endsection

@section('scripts')

<script type="text/javascript" src="{{ base_url('assets/components/DataTables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ base_url('assets/scripts/compras/compramos_auto/agregar.js') }}"></script>

<script type="text/javascript">
    $('.custom-file-input').on('change', function() {
        var fileName = document.getElementById("imagen").files[0].name;
        $('.custom-file-label').addClass("selected").html(fileName);
    })

    $("form#formImagenes").on("submit", function(e) {
        e.preventDefault();
        var formData = new FormData(document.getElementById("formImagenes"));
        $.ajax({
            url: PATH + '/compras/compramos_auto/save_imagenes',
            type: "post",
            dataType: "json",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(result, status, xhr) {
                $('.custom-file-label').removeClass("selected").html('');
                if (result.estatus == 'error') {
                    if (result.mensaje) {
                        message.error(result.mensaje, true);
                    } else {
                        errors.mostrar(result.info);
                    }
                } else {
                    if (result.data) {
                        $("form#formImagenes")[0].reset();
                        message.success(result.mensaje, function() {
                            app.getImagenesByAuto();
                        });
                    }
                }
            }
        })
    });
</script>
@endsection