@layout($layout)
@section('css')
<link rel="stylesheet" href="{{ base_url('assets/components/DataTables/media/css/jquery.dataTables.min.css')}}">
@endsection
@section('contenido')
@if($usuario_id)
<div class="header mt-4">
    <h1 class="title h2">Listado de tus automoviles ofertados</h1>
    <p>Agrega tu(s) vehículos que desees ofertar a las agencias</p>
    <div class="float-right mb-3">
        <a href="{{ site_url('compras/compramos_auto/agregar?user_id='.$usuario_id) }}" class="btn btn-primary btn-large"> <i class="fa fa-car"></i>&nbsp;Agregar nuevo vehículo</a>
    </div>
    <div class="clearfix"></div>
</div>
<div class="body mt-4 p-3" style="background-color:#fff">
    <div class="row row-4 car-listing-row">
        <?php 
        if (is_array($autos)) {
        foreach ($autos as $auto) { ?>
            <div class="col-md-4 col-sm-6 col-xs-12 col-xxs-12 stm-template-front-loop mb-3 mt-3">
                @if( $auto['num_ofertas'] == 0)
                <a href="<?php echo site_url('compras/compramos_auto/continuar/?compra_auto_id=' . ($auto['id']) . '&tab=1'); ?>" class="rmv_txt_drctn xx external" rel="nofollow">
                @endif

                
                    <div class="image">
                        <img style="height:250px; width:100%" src="{{ base_url('compra_autos/'.$auto['file_imagen']) }}" onerror="this.src='<?php echo base_url('assets/img/no-imagen.jpg'); ?>'" class="attachment-stm-img-255-135 size-stm-img-255-135" alt="" loading="lazy" />
                    </div>
                    <div class="listing-car-item-meta">
                        <div class="car-meta-top heading-font clearfix ">
                            <div class="price">
                                <div class="normal-price">$ {{ utils::formatMoney($auto['precio']) }}</div>
                            </div>
                            <div class="car-title">{{ $auto['marca'] }} {{ $auto['modelo'] }} {{ $auto['tipo_carroceria'] }} {{ $auto['anio'] }} 
                                @if($auto['estatus_id'] == 0)
                                <span class="badge badge-pill badge-warning">Sin publicar</span>
                                @endif
                            </div>

                        </div>
                        <div class="car-meta-bottom">
                            <ul>
                                <li><i class="fa fa-wrench"></i><span>{{ $auto['tipo_traccion'] }}</span></li>
                                <li><i class="fa fa-road"></i><span>{{ $auto['combustible'] }}</span></li>
                                <li><i class="fa fa-car"></i><span>{{ $auto['tipo_trasmision'] }}</span></li>
                                @if( $auto['num_ofertas'] == 0)
                                <li><a class="ml-4 bold text-orange" href="<?php echo site_url('compras/compramos_auto/continuar/?compra_auto_id=' . ($auto['id']) . '&tab=1'); ?>"><i class="fa fa-edit text-orange"></i><span>Editar</span></a></li>
                                @endif
                                @if( $auto['num_ofertas'] > 0)
                                &nbsp;
                                    @if($auto['estatus_id'] > 1)
                                        <li><button  onclick="app.abrir_modal({{$auto['id']}});"  type="button" class="btn btn-success">
                                        Vendido
                                        </a></li>
                                    @else
                                        <li><button type="button" onclick="app.abrir_modal({{$auto['id']}});" class="btn btn-warning">
                                        Ofertas <span class="badge badge-success">{{ $auto['num_ofertas'] }}</span>
                                        </button></li>
                                    @endif
                                @endif
                            </ul>
                        </div>
                    </div>
                @if( $auto['num_ofertas'] == 0)
                </a>
                @endif
            </div>
        <?php } 
        }?>
    </div>
</div>
@else
<div class="header mt-4">
    <h2>¡Te compramos tu auto!</h2>
</div>
<div class="alert alert-warning" role="alert">
    <p><b>No se eligio un usuario desde la app de autolavados.</b></p>
</div>
@endif

@endsection

<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog  modal-xl" style="margin-top: 200px;">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Ofertas</h5>
       
      </div> -->
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

@section('scripts')

<script type="text/javascript" src="{{ base_url('assets/components/DataTables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ base_url('assets/scripts/compras/compramos_auto/index.js') }}"></script>
@endsection