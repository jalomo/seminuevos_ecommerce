<div class="row mb-2 mt-2">
    <form name="formAuto">
        <div class="col-md-8 mt-2">
            <div class="form-group">
                <label>*Descripción</label>
                <textarea name="descripcion" class="form-control" style="min-height:200px" maxlength="1000" rows="6">{{ isset($datos['descripcion']) ? $datos['descripcion'] : '' }}</textarea>
                <small id="msg_descripcion" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Precio</label>
                <input type="text" name="precio" class="form-control" value="{{ isset($datos['precio']) ? $datos['precio'] : '' }}" />
                <small id="msg_precio" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Tipo carroceria</label>
                <select name="tipo_carroceria_id" class="form-control" data="{{ isset($datos['tipo_carroceria_id']) ? $datos['tipo_carroceria_id'] : '' }}">
                    <option value=""></option>
                </select>
                <small id="msg_tipo_carroceria_id" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Marca</label>
                <select name="marca_id" class="form-control" data="{{ isset($datos['marca_id']) ? $datos['marca_id'] : '' }}">
                    <option value=""></option>
                </select>
                <small id="msg_marca_id" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Modelo</label>
                <select name="modelo_id" class="form-control" data="{{ isset($datos['modelo_id']) ? $datos['modelo_id'] : '' }}">
                    <option value=""></option>
                </select>
                <small id="msg_modelo_id" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Año</label>
                <select name="anio" class="form-control select2">
                    @for($year = date('Y'); $year >= 1970; $year--)
                    <option {{ isset($datos['anio']) && $year == $datos['anio']  ? 'selected="selected"'  : '' }} value="{{ $year }}">{{ $year }}</option>
                    @endfor
                </select>
                <small id="msg_anio" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Tipo transmisión</label>
                <select name="tipo_transmision_id" class="form-control" data="{{ isset($datos['tipo_transmision_id']) ? $datos['tipo_transmision_id'] : '' }}">
                    <option value=""></option>
                </select>
                <small id="msg_tipo_transmision_id" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*kilometraje</label>
                <input type="number" name="kilometraje" class="form-control" value="{{ isset($datos['kilometraje']) ? $datos['kilometraje'] : '' }}" />
                <small id="msg_kilometraje" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Color exterior</label>
                <select name="color_exterior_id" class="form-control" data="{{ isset($datos['color_exterior_id']) ? $datos['color_exterior_id'] : '' }}">
                    <option value=""></option>
                </select>
                <small id="msg_color_exterior_id" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Color interior</label>
                <select name="color_interior_id" class="form-control" data="{{ isset($datos['color_interior_id']) ? $datos['color_interior_id'] : '' }}">
                    <option value=""></option>
                </select>
                <small id="msg_color_interior_id" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Tipo combustible</label>
                <select name="tipo_combustible_id" class="form-control" data="{{ isset($datos['tipo_combustible_id']) ? $datos['tipo_combustible_id'] : '' }}">
                    <option value=""></option>
                </select>
                <small id="msg_tipo_combustible_id" class="form-text text-danger"></small>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="form-group">
                <label>*Tipo de tracción </label>
                <select name="tipo_traccion_id" class="form-control" data="{{ isset($datos['tipo_traccion_id']) ? $datos['tipo_traccion_id'] : '' }}">
                    <option value=""></option>
                </select>
                <small id="msg_tipo_traccion_id" class="form-text text-danger"></small>
            </div>
            <div class="clearfix"></div>
        </div>
        <input type="hidden" class="form-control" name="compra_auto_id" value="{{ isset($datos['id']) ? $datos['id'] : '' }}" />
        <input type="hidden" class="form-control" name="usuario_id" value="{{ isset($datos['usuario_id']) ? $datos['usuario_id'] : $usuario_id }}" />
    </form>
</div>