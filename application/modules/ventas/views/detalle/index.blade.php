@layout($layout)
<!-- <style>
    .carousel-item,
    .carousel-inner,
    .carousel-inner img {
        height: 100%;
        width: 100%;
    }

    .carousel-item {
        text-align: center;
    }

    .carousel {
        height: 650px;
    }

    @media (max-width: 575.98px) {
        .carousel {
            height: 400px;
        }
    } -->
</style>

@section('contenido')
<script>
    var automovil_id = "{{ $id }}";
</script>
<div class="stm-single-car-page" style="background-position: 0px -136px;">
    <div class="container_">
        <div class="mt-4 mb-4">
            <div class="stm-vc-single-car-content-left wpb_column vc_column_container vc_col-sm-12 vc_col-lg-9">
                <div class="vc_column-inner_">
                    <div class="wpb_wrapper_">
                        <h1 class="title h2">{{ $data['detalle']['marca'].' '.$data['detalle']['modelo'].', '.$data['detalle']['tipo_trasmision'].' '.$data['detalle']['tipo_traccion']  }}</h1>
                        <div class="row">
                            <div class="image">
                                <div id="carouselInterval_4" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselInterval_4" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselInterval_4" data-slide-to="1" class=""></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        @if (isset($data['banners']))
                                        @foreach ($data['banners'] as $key => $item)
                                        <div class="carousel-item {{ $key == 0 ? 'active' : ''}}">
                                            <img class="img-fluid" src="{{ base_url('imagenes_autos/' . $item['file_imagen']) }}" alt="Third slide">
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselInterval_4" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselInterval_4" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="stm-vc-single-car-sidebar-right wpb_column vc_column_container vc_col-sm-12 vc_col-lg-3">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="single-car-prices">
                            <div class="single-regular-price text-center">
                                <span class="h3">$ {{ utils::formatMoney($detalle['precio'])}}</span>
                            </div>
                        </div>
                        <div class="single-car-data">
                            <table>
                                <tbody>
                                    @if($frame_url != false)
                                    <tr>
                                        <td class="t-label">Video</td>
                                        <td class="t-value h6"><i class="fa fab fa-youtube text-danger"></i><a data-frame="{{ $frame_url }}" onclick="app.openModal(this)">Ver video</a></td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td class="t-label">Tipo carroceria</td>
                                        <td class="t-value h6">{{ $detalle['tipo_carroceria'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="t-label">Condición</td>
                                        <td class="t-value h6">{{ $detalle['condicion']}}</td>
                                    </tr>
                                    <tr>
                                        <td class="t-label">Sucursal</td>
                                        <td class="t-value h6">{{ $detalle['sucursal']}}</td>
                                    </tr>
                                    <tr>
                                        <td class="t-label">Kilometraje</td>
                                        <td class="t-value h6">{{ $detalle['kilometraje']}}</td>
                                    </tr>
                                    <tr>
                                        <td class="t-label">Tipo de combustible</td>
                                        <td class="t-value h6">{{ $detalle['combustible']}}</td>
                                    <tr>
                                        <td class="t-label">Año</td>
                                        <td class="t-value h6">{{ $detalle['anio']}}</td>
                                    </tr>
                                    <tr>
                                        <td class="t-label">Transmición</td>
                                        <td class="t-value h6">{{ $detalle['tipo_trasmision']}}</td>
                                    </tr>
                                    <tr>
                                        <td class="t-label">Conducir</td>
                                        <td class="t-value h6">{{ $detalle['tipo_traccion'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="t-label">Color exterior</td>
                                        <td class="t-value h6">{{ $detalle['color_exterior']}}</td>
                                    </tr>
                                    <tr>
                                        <td class="t-label">Color interior</td>
                                        <td class="t-value h6">{{ $detalle['color_interior']}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="single-car-mpg heading-font">
                            <div class="text-center">
                                <div class="clearfix dp-in text-left mpg-mobile-selector">
                                    <div class="mpg-unit">
                                        <div class="mpg-value">{{ $detalle['combustible_ciudad'] }}</div>
                                        <div class="mpg-label">Ciudad</div>
                                    </div>
                                    <div class="mpg-icon pt-3">
                                        <i class="fa fa-car fa-2x text-center text-white"></i>
                                    </div>
                                    <div class="mpg-unit">
                                        <div class="mpg-value">{{ $detalle['combustible_carretera'] }}</div>
                                        <div class="mpg-label">Carretera</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix mt-5"></div>
        <ul class="mt-4 nav nav-tabs wpb_tabs_nav ui-tabs-nav vc_clearfix ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#descripcion" role="tab" aria-controls="descripcion" aria-selected="true">Descripción vehículo</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tecnica" role="tab" aria-controls="tecnica" aria-selected="false">Información técnica</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#funciones" role="tab" aria-controls="funciones" aria-selected="false">Funciones</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#localizacion" role="tab" aria-controls="localizacion" aria-selected="false">Localización</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#contacto" role="tab" aria-controls="contacto" aria-selected="false">Contacto</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#cotizacion" role="tab" aria-controls="cotizacion" aria-selected="false">Cotizador</a>
            </li>
        </ul>
        <div class="tab-content panel-theme" id="myTabContent">
            <div class="tab-pane in active" id="descripcion">
                @if (!empty($data['detalle']['descripcion']))
                @include('detalle/tabs/descripcion_vehiculo')
                @else
                <div class="alert alert-warning">
                    No hay informacion
                </div>
                @endif
            </div>
            <div class="tab-pane" id="tecnica">
                @if (!empty($data['info_tecnica']))
                @include('detalle/tabs/informacion_tecnica_vehiculo')
                @else
                <div class="alert alert-warning">
                    No hay informacion
                </div>
                @endif
            </div>
            <div class="tab-pane" id="funciones" role="tabpanel">
                @if (!empty($data['de_funciones']))
                @include('detalle/tabs/funciones_vehiculo')
                @else
                <div class="alert alert-warning">
                    No hay informacion
                </div>
                @endif
            </div>
            <div class="tab-pane" id="localizacion" role="tabpanel">
                @include('detalle/tabs/localizacion_vehiculo')
            </div>
            <div class="tab-pane" id="contacto" role="tabpanel">
                @include('detalle/tabs/contacto_vehiculo')
            </div>
            <div class="tab-pane" id="cotizacion" role="tabpanel">
                @include('detalle/tabs/cotizacion')
            </div>
        </div>
    </div>
    <!--cont-->
</div>

@endsection


<div class="modal fade" id="modalyoutube" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div style="margin-top:200px" class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="videoWrapper">
                    <iframe id="frameyoutube" width="600" height="400" src="" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="$('#frameyoutube').attr('src', false);" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>