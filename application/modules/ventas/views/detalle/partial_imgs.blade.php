 <!--New badge with videos-->
 <div class="special-label h5">OFERTA</div>
 <div class="stm-big-car-gallery owl-carousel owl-theme owl-loaded">
     <div class="owl-stage-outer">
         <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1596px;">
             <div class="owl-item active">
                 <div class="stm-single-image" data-id="big-image-2453">
                     <a href="{{base_url('imagenes_autos/'.$data['detalle']['file_imagen'])}}" class="stm_fancybox external" rel="stm-car-gallery nofollow">
                        <img width="798" height="466" src="{{ base_url('imagenes_autos/'.$data['detalle']['file_imagen']) }}" 
                            class="img-responsive wp-post-image" alt="" loading="lazy" 
                            srcset="{{ base_url('imagenes_autos/'.$data['detalle']['file_imagen']) }}"> 
                    </a>
                 </div>
             </div>
         </div>
     </div>
     <div class="owl-controls" style="display: none;">
         <div class="owl-nav">
             <div class="owl-prev" style="display: none;">prev</div>
             <div class="owl-next" style="display: none;">next</div>
         </div>
         <div class="owl-dots" style="display: none;"></div>
     </div>
 </div>
 <div class="stm-thumbs-car-gallery owl-carousel owl-theme owl-loaded" style="margin-top: 22px;">
     <div class="owl-stage-outer">
         <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 328px;">
             <div class="owl-item active" style="width: 142px; margin-right: 22px;">
                 <div class="stm-single-image" id="big-image-2453">
                     <img width="350" height="205" 
                     src="https://motors.stylemixthemes.com/wp-content/uploads/2020/10/raptor-02-350x205.jpg" 
                     class="img-responsive wp-post-image" alt="" loading="lazy" 
                     srcset="https://motors.stylemixthemes.com/wp-content/uploads/2020/10/raptor-02-350x205.jpg 350w, 
                     https://motors.stylemixthemes.com/wp-content/uploads/2020/10/raptor-02-825x483.jpg 825w, 
                     https://motors.stylemixthemes.com/wp-content/uploads/2020/10/raptor-02-798x466.jpg 798w, 
                     https://motors.stylemixthemes.com/wp-content/uploads/2020/10/raptor-02-700x410.jpg 700w,
                      https://motors.stylemixthemes.com/wp-content/uploads/2020/10/raptor-02-240x140.jpg 240w, 
                      https://motors.stylemixthemes.com/wp-content/uploads/2020/10/raptor-02-280x165.jpg 280w, 
                      https://motors.stylemixthemes.com/wp-content/uploads/2020/10/raptor-02-560x330.jpg 560w" 
                     sizes="(max-width: 350px) 100vw, 350px">
                 </div>
             </div>
             <div class="owl-item active" style="width: 142px; margin-right: 22px;">
                 <div class="stm-single-image" id="big-image-2454">
                     <img src="https://motors.stylemixthemes.com/wp-content/uploads/2020/10/raptor-03-350x205.jpg" alt="Ford F-150 Raptor 6,2L V8 AT full">
                 </div>
             </div>
         </div>
     </div>
     <div class="owl-controls" style="display: none;">
         <div class="owl-nav">
             <div class="owl-prev"></div>
             <div class="owl-next"></div>
         </div>
         <div class="owl-dots" style="display: none;"></div>
     </div>
 </div>