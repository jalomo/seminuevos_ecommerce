<div class="stm_auto_loan_calculator" style="display:block">
    <div class="mb-4">
        <i class="fa fa-calculator text-orange" style="font-size:27px;"></i>
        <span class="title h5">Cotización</span>
    </div>
    <form id="cotizar">

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <h4 class="bold">Precio vehículo: <span class="orange bold">$ {{ utils::formatMoney($detalle['precio']) }}</span></h4>
                    <input type="hidden" readonly="readonly" id="precio_" name="precio_" class="numbersOnly vehicle_price" value="{{ $detalle['precio'] }}">
                </div>

                <div class="form-group md-mg-rt">
                    <h4 class="bold">Porcentaje de interes: <span class="orange">% {{ $detalle['porcentaje_interes'] }}</span></h4>
                    <input type="hidden" id="intereses_" name="intereses_" class="numbersOnly interest_rate" value="{{ $detalle['porcentaje_interes'] }}">
                </div>
                <div class="form-group">
                    <h4 class="bold">Pago minimo inicial: <span class="orange">$ {{ utils::formatMoney(($detalle['porcentaje_enganche_inicial'] * $detalle['precio']) / 100) }}</span></h4>
                    <input type="hidden" id="pagominimo"  name="pagominimo" value="{{ (($detalle['porcentaje_enganche_inicial'] * $detalle['precio']) / 100) }}">
                    <input type="text" id="pago_inicial_"  name="pago_inicial_" class="numbersOnly down_payment" value="{{ (($detalle['porcentaje_enganche_inicial'] * $detalle['precio']) / 100) }}">
                </div>
                <div class="form-group md-mg-lt">
                    <h4 class="bold">* Periodo <span class="orange">(meses)</span></h4>
                    <select class="form-control" name="periodo" id="periodo_" name="periodo_">
                        <option value=""></option>
                        <option value="3">3 meses</option>
                        <option value="6">6 meses</option>
                        <option value="9">9 meses</option>
                        <option value="12">12 meses</option>
                        <option value="18">18 meses</option>
                        <option value="24">24 meses</option>
                        <option value="36">36 meses</option>
                        <option value="48">48 meses</option>
                        <option value="60">60 meses</option>
                    </select>
                </div>
                <button type="button" onclick="app.cotizar()" class="button button-sm btn-primary calculate_loan_payment dp-in float-right"><i class="fa fa-calculator text-white"></i> Calcular</a>
            </div>
            <div class="col-md-12">
                <div id="resultado_cotizacion" class="stm_calculator_results" style="display: none;">
                    <div class="stm-calc-results-inner">
                        <div class="stm-calc-label">Mensualidad</div>
                        <div id="mensualidad_precio" class="monthly_payment h5 _precio"></div>

                        <div class="stm-calc-label">Pago total de intereses</div>
                        <div id="interes_precio" class="total_interest_payment h5 _precio"></div>

                        <div class="stm-calc-label">Subtotal</div>
                        <div id="subtotal" class="total_amount_to_pay h5 _precio"></div>

                        <div class="stm-calc-label">Monto total a pagar</div>
                        <div id="total_precio" class="total_amount_to_pay h5 _precio"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>

</div>