<div class="vc_row wpb_row vc_inner vc_row-fluid mt-4">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner">
            <div class="wpb_wrapper">
                <h4 style="color: #252628;text-align: left;font-family:Montserrat;font-weight:400;font-style:normal" class="vc_custom_heading vc_custom_1448521810500">Descripcion funciones</h4>
                <div class="wpb_text_column wpb_content_element ">
                    <div class="wpb_wrapper mb-4">
                       <p>
                           {{ $data['de_funciones']['funciones']['descripcion_funciones'] }}
                       </p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text vc_sep_color_grey vc_custom_1445932251080  vc_custom_1445932251080"><span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span><span class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
</div>
<div class="vc_row wpb_row vc_inner vc_row-fluid">
    <div class="wpb_column vc_column_container vc_col-sm-3">
        <div class="vc_column-inner">
            <div class="wpb_wrapper">
                <h5 style="text-align: left" class="vc_custom_heading vc_custom_1448521832735">DISEÑO INTERIOR</h5>
                <div class="wpb_text_column wpb_content_element ">
                    <div class="wpb_wrapper">
                        <ul class="list-style-2" style="font-size: 13px;">
                            @foreach ($data['de_funciones']['diseno_interior'] as $item)
                            <li><span style="color: #0e3256;">{{ $item['nombre_diseno_interior'] }}</span></li>                                
                            @endforeach
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wpb_column vc_column_container vc_col-sm-3">
        <div class="vc_column-inner">
            <div class="wpb_wrapper">
                <h5 style="text-align: left" class="vc_custom_heading vc_custom_1448521838574">CARACTERISTICAS DE SEGURIDAD</h5>
                <div class="wpb_text_column wpb_content_element ">
                    <div class="wpb_wrapper">
                        <ul class="list-style-2" style="font-size: 13px;">
                            @foreach ($data['de_funciones']['caracteristica_seguridad'] as $item)
                                <li><span style="color: #0e3256;">{{ $item['nombre_caracterisitca_seguridad'] }}</span></li>                                
                            @endforeach
                           
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wpb_column vc_column_container vc_col-sm-3">
        <div class="vc_column-inner">
            <div class="wpb_wrapper">
                <h5 style="text-align: left" class="vc_custom_heading vc_custom_1448521845021">EXTERIOR FEATURES</h5>
                <div class="wpb_text_column wpb_content_element ">
                    <div class="wpb_wrapper">
                        <ul class="list-style-2" style="font-size: 13px;">
                            @foreach ($data['de_funciones']['caracteristicas_exteriores'] as $item)
                            <li><span style="color: #0e3256;">{{ $item['nombre_disenoexterior'] }}</span></li>                                
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wpb_column vc_column_container vc_col-sm-3">
        <div class="vc_column-inner">
            <div class="wpb_wrapper">
                <h5 style="text-align: left" class="vc_custom_heading vc_custom_1448521851214">CARACTERISTICAS ADICIONALES</h5>
                <div class="wpb_text_column wpb_content_element ">
                    <div class="wpb_wrapper">
                        <ul class="list-style-2" style="font-size: 13px;">
                            @foreach ($data['de_funciones']['caracteristicas_adicionales'] as $item)
                            <li><span style="color: #0e3256;">{{ $item['nombre_cadicional'] }}</span></li>                                
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>