
<div class="header mt-3">
    <h2>Ubicación de la sucursal</h2>
</div>
<div class="body">
    <div id="map" style="height:500px; "></div>
</div>


@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_gbAjK7KGkfidjIi0iLFqD39VWbhtXcs&callback=initMap&libraries=&v=weekly" defer></script>
<script type="text/javascript">
    let map, marker, geocoder;
    let markers = [];

    function initMap() {
        let valorx = '<?php echo $data['detalle']['coordenada_x']; ?>';
        let valory = '<?php echo $data['detalle']['coordenada_y']; ?>';

        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            center: new google.maps.LatLng(parseFloat(valorx), parseFloat(valory)),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        geocoder = new google.maps.Geocoder();

        const myLatLng = {
            lat: parseFloat(valorx),
            lng: parseFloat(valory)
        };
        marker = new google.maps.Marker({
            map: map,
            position: myLatLng,
            draggable: true
        });
        map.setCenter(myLatLng);
        map.setZoom(16);
    }
</script>

<link href="{{ base_url('assets/components/pace/themes/red/pace-theme-minimal.css') }}" rel="stylesheet" />
<script src="{{ base_url('assets/components/pace/pace.min.js') }}"></script>
<script src="{{ base_url('assets/scripts/seminuevos_ecommerce/seminuevos_ecommerce/detalle.js') }}"></script>

@endsection