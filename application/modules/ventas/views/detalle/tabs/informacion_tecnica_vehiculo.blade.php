<div class="stm-tech-infos mt-4">
    <div class="">
        <i class="fa fa-car text-orange" style="font-size:27px;"></i>
        <span class="title h5">Motor</span>
    </div>
    <table>
        <tbody>
            <tr>
                <td>
                    <span class="text-transform ">Número de cilindros</span>
                </td>
                <td class="text-right">
                    <span class="h6">{{ $data['info_tecnica']['numero_cilindros'] }}</span>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="text-transform ">Desplazamiento</span>
                </td>
                <td class="text-right">
                    <span class="h6">{{ $data['info_tecnica']['desplazamiento'] }}</span>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="text-transform ">Caballo de fuerza</span>
                </td>
                <td class="text-right">
                    <span class="h6">{{ $data['info_tecnica']['caballos_fuerza'] }} HP</span>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="text-transform ">@ rpm</span>
                </td>
                <td class="text-right">
                    <span class="h6">{{ $data['info_tecnica']['rpm'] }}</span>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="text-transform ">Torque</span>
                </td>
                <td class="text-right">
                    <span class="h6">{{ $data['info_tecnica']['torque'] }}</span>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="text-transform ">Índice de compresión</span>
                </td>
                <td class="text-right">
                    <span class="h6">{{ $data['info_tecnica']['indice_compresion'] }}</span>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="stm-tech-infos">
    <div class="">
        <i class="fa fa-road text-orange" style="font-size:28px;"></i>
        <span class="title h5">Velocidad</span>
    </div>
    <table>
        <tbody>
            <tr>
                <td>
                    <span class="text-transform ">Velocidad máxima de la pista</span>
                </td>
                <td class="text-right">
                    <span class="h6">{{ $data['info_tecnica']['velocidad_maxima'] }}</span>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="text-transform ">0 - 60 mph</span>
                </td>
                <td class="text-right">
                    <span class="h6">{{ $data['info_tecnica']['velocidad060'] }} s</span>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="stm-tech-infos">
    <div class="">
        <i class="fa fa-cogs text-orange" style="font-size:35px;"></i>
        <span class="title h5">Transmisión</span>
    </div>
    <table>
        <tbody>
            <tr>
                <td>
                    <span class="text-transform ">Tipo</span>
                </td>
                <td class="text-right">
                    <span class="h6">{{ $data['info_tecnica']['trasmision'] }}</span>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="text-transform ">Velocidades</span>
                </td>
                <td class="text-right">
                    <span class="h6">{{ $data['info_tecnica']['numero_velocidades'] }}</span>
                </td>
            </tr>
        </tbody>
    </table>
</div>