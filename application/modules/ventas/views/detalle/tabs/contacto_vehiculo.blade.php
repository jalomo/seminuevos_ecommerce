<div class="vc_row wpb_row vc_inner vc_row-fluid single-car-form">
    <div class="stm-col-pad-left wpb_column vc_column_container">
        <div class="vc_column-inner">
            <div class="wpb_wrapper">
                <div class="icon-box vc_custom_1448522065607 icon_box_65985 stm-layout-box-car_dealer"
                    style="color:#0e3256">
                    <div class="boat-line"></div>
                    <div class="icon vc_custom_1448522065606 boat-third-color" style="font-size:27px;color:#6c98e1; ">
                        <i class="fa fa-paper-plane"></i>
                    </div>
                    <div class="icon-text">
                        <h4 class="title heading-font">Mensaje al vendedor </h4>
                    </div>
                </div>
                <div role="form" class="wpcf7" id="wpcf7-f500-p2452-o1" lang="en-US" dir="ltr">
                    <div class="screen-reader-response">
                        <p role="status" aria-live="polite" aria-atomic="true" class="stm-hidden"></p>
                        <ul></ul>
                    </div>
                    <form id="form_contacto" action="#" method="post" class="wpcf7-form init" novalidate="novalidate"
                        data-status="init">
                        <div class="row">


                            <div class="col-sm-7">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group ">
                                            <label for="" class=" col-form-label">Donde desea tomar su prueba de
                                                manejo:</label>
                                            <select onchange="app.ubicacion(this.value);" class="custom-select"
                                                name="ubicacion_prueba_manejo" id="ubicacion_prueba_manejo">
                                                <option value="" selected>Seleccione una ubicación</option>
                                                <option value="agencia">Agencia</option>
                                                <option value="domicilio">Domicilio</option>
                                            </select>
                                            <small id="ubicacion_prueba_manejo_msg" class="form-text text-danger"></small>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 ubicacion_agencia" style="display:none;">
                                        <div class="form-group ">
                                            <label for="" class=" col-form-label">Sucursal:</label>
                                            <input type="text" readonly class="form-control-plaintext"
                                                value="{{$data['detalle']['sucursal']}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 ubicacion_agencia" style="display:none;">
                                        <div class="form-group ">
                                            <label for="" class=" col-form-label">Agencia:</label>
                                            <input type="text" readonly class="form-control-plaintext"
                                                value="{{$data['detalle']['agencia']}}">
                                            <input type="hidden" class="form-control"
                                                value="{{$data['detalle']['coordenada_x']}}" name="lat_agencia">
                                            <input type="hidden" class="form-control"
                                                value="{{$data['detalle']['coordenada_y']}}" name="lng_agencia">


                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label for="" class=" col-form-label">Nombre:</label>
                                            <input type="text" name="nombre" class="form-control-plaintext" value="">
                                            <small id="nombre_msg" class="form-text text-danger"></small>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label for="" class=" col-form-label">Primer Apellido:</label>
                                            <input type="text" name="apellido1" class="form-control-plaintext" value="">
                                            <small id="apellido1_msg" class="form-text text-danger"></small>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label for="" class=" col-form-label">Segundo Apellido:</label>
                                            <input type="text" name="apellido2" class="form-control-plaintext" value="">
                                            <small id="apellido2_msg" class="form-text text-danger"></small>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group ">
                                            <label for="" class=" col-form-label">Teléfono:</label>
                                            <input type="text" name="telefono" class="form-control-plaintext" value="">
                                            <small id="telefono_msg" class="form-text text-danger"></small>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group ">
                                            <label for="" class=" col-form-label">Correo electrónico:</label>
                                            <input type="text" name="correo_electronico" class="form-control-plaintext"
                                                value="">
                                            <small id="correo_electronico_msg" class="form-text text-danger"></small>
                                        </div>
                                    </div>





                                    <div class="col-sm-12 ubicacion_domicilio" style="display:none;">
                                        <div class="form-group ">
                                            <label for="" class=" col-form-label">Domicilio:</label>
                                            <div class="row">
                                                <div class="col-sm-11">
                                                    <textarea rows="2" style="height:120px;" class="form-control"
                                                        onkeydown="app.search()" name="domicilio"></textarea>
                                                    <small id="domicilio_msg" class="form-text text-danger"></small>
                                                </div>
                                                <div class="col-sm-1 pl-0">
                                                    <button class="btn btn-primary" type="button"
                                                        onclick="app.buscar_direccion()" title="Ubicar en el mapa"><i
                                                            class="fas fa-search"></i></button>
                                                </div>
                                            </div>

                                            <input type="hidden" class="form-control" name="lat">
                                            <input type="hidden" class="form-control" name="lng">
                                        </div>
                                    </div>



                                </div>

                            </div>
                            <div class="col-sm-5" style="display:block;">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <div class="form-group">
                                                <label for="" class=" col-form-label ">Licencia para conducir (Delantera):</label>
                                                <div class="custom-file">
                                                    <input type="file" id="imagen_licencia" accept="image/*"
                                                        name="imagen" class="custom-file-input">
                                                    <label id="imagen_licencia_label" class="custom-file-label"></label>
                                                </div>
                                                <small id="imagen_licencia_msg" class="form-text text-danger"></small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <div class="form-group">
                                                <label for="" class=" col-form-label ">Licencia para conducir (Trasera):</label>
                                                <div class="custom-file">
                                                    <input type="file" id="imagen_licencia2" accept="image/*"
                                                        name="imagen" class="custom-file-input">
                                                    <label id="imagen_licencia_label2" class="custom-file-label"></label>
                                                </div>
                                                <small id="imagen_licencia2_msg" class="form-text text-danger"></small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">

                                        <div class="form-group">
                                            <label for="" class=" col-form-label ">Identificación oficial (Delantera)</label>
                                            <div class="custom-file">
                                                <input type="file" id="imagen_identificacion" accept="image/*"
                                                    name="imagen" class="custom-file-input">
                                                <label id="imagen_identificacion_label"
                                                    class="custom-file-label"></label>
                                            </div>
                                            <small id="imagen_identificacion_msg" class="form-text text-danger"></small>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class=" col-form-label ">Identificación oficial (Trasera)</label>
                                            <div class="custom-file">
                                                <input type="file" id="imagen_identificacion2" accept="image/*"
                                                    name="imagen" class="custom-file-input">
                                                <label id="imagen_identificacion_label2"
                                                    class="custom-file-label"></label>
                                            </div>
                                            <small id="imagen_identificacion2_msg" class="form-text text-danger"></small>
                                        </div>
                                        <small class="form-text text-muted mt-0">
                                            <b>Documentos que son aceptados como identificación oficial:</b>
                                            <ul>
                                                <li class="mb-0">Credencial para votar vigente, expedida por el
                                                    Instituto Nacional Electoral (antes Instituto Federal Electoral).
                                                </li>
                                                <li class="mb-0 mt-0">Pasaporte vigente.</li>
                                                <li class="mb-0 mt-0">Cédula profesional vigente con fotografía. Quedan
                                                    exceptuadas las cédulas profesionales electrónicas.</li>
                                                <li class="mb-0 mt-0">Credencial del Instituto Nacional de las Personas
                                                    Adultas Mayores vigente.</li>
                                            </ul>
                                        </small>

                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-12" style="display:block;">
                                <div id="map_prueba_manejo" style="height:300px; ">Cargando mapa...</div>
                            </div>
                            <div class="col-sm-12">
                                <button type="button" onclick="app.guardar_contacto();"
                                    class="btn btn-primary mt-4 float-right"><i
                                        class="fa fa-save"></i>&nbsp;Guardar</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>