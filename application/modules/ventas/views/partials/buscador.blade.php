<script>
@if ($condicion == 2)
var url_actual = "<?php echo site_url('ventas/autos_seminuevos'); ?>";
@else
var url_actual = "<?php echo site_url('ventas/autos_nuevos'); ?>";
@endif

var anio_actual = <?php echo date('Y') ?>;    
</script>

<form action="" method="get" data-trigger="filter" id="form_busqueda" >
    <div class="filter filter-sidebar ajax-filter">
        <div class="sidebar-entry-header">
            <span class="h4"> <i class="fa fa-search"></i>
                Buscador</span>        </div>
            <div class="row row-pad-top-24">
                <div class="col-md-12 col-sm-6 stm-filter_condition">
                    <h4>Autos {{$condicion == 2 ? ' seminuevos' : ' nuevos '}}</h4>
                    <hr/>
                </div>
                
                <div class="col-md-12 col-sm-6 stm-filter_body">
                    <div class="form-group">
                        <select name="sucursal_id"  model="CaSucursales_model" placeholder="Sucursal" class="form-control" tabindex="-1" aria-hidden="true">
                        </select>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6 stm-filter_body">
                    <div class="form-group">
                        <select name="tipo" model="CaTipoCarroceria_model" placeholder="Tipo" class="form-control" tabindex="-1" aria-hidden="true">
                        </select>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6 stm-filter_make">
                    <div class="form-group">
                        <select name="marca" model="CaMarcas_model" placeholder="Marca" class="form-control" tabindex="-1" aria-hidden="true">
                        </select>

                    </div>
                </div>
                <div class="col-md-12 col-sm-6 stm-filter_serie">
                    <div class="form-group">
                        <select name="modelo" model="CaModelos_model" placeholder="Modelo" class="form-control" tabindex="-1" aria-hidden="true">
                        </select>
                    </div>
                </div>
                @if ($condicion == 2)
                <div class="col-md-12 col-sm-6 stm-filter_mileage">
                    <div class="form-group">
                        <span><small>Kilometraje</small></span>
                        <div class="row">
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="" name="kilometraje" value="<?php echo $kilometraje; ?>" />
                            </div>
                            <div class="col-3">
                                <button type="button" class="btn btn-light btn-sm mt-4 text-dark" onclick="app.busqueda();"> <i class="fas fa-chevron-right"></i> </button>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <div class="col-md-12 col-sm-6 stm-filter_ca-year">
                    <div class="form-group">
                        <span><small>Año</small></span>
                        <div class="row">
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="" name="anio" value="<?php echo $anio; ?>" />
                            </div>
                            <div class="col-sm-3">
                                <button type="button" class="btn btn-light btn-sm mt-4 text-dark" onclick="app.busqueda();"> <i class="fas fa-chevron-right"></i> </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6 stm-filter_transmission">
                    <div class="form-group">
                        <select name="transmision" model="CaTipoTrasmision_model" placeholder="Transmisión" class="form-control " tabindex="-1" aria-hidden="true">
                        </select>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6 stm-filter_transmission">
                    <div class="form-group">
                        <span><small>Precio</small></span>
                        <div class="row">
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="" name="precio" value="<?php echo $precio; ?>" />
                            </div>
                            <div class="col-sm-3">
                                <button type="button" class="btn btn-light btn-sm mt-4 text-dark" onclick="app.busqueda();"> <i class="fas fa-chevron-right"></i> </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6 stm-filter_exterior-color">
                    <div class="form-group">
                        <select name="colorexterior" model="CaColores_model" placeholder="Color Exterior" class="form-control" tabindex="-1" aria-hidden="true">
                        </select>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6 stm-filter_interior-color">
                    <div class="form-group">
                        <select name="colorinterior"  model="CaColores_model" placeholder="Color Interior" class="form-control" tabindex="-1" aria-hidden="true">
                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" name="condicion_id" value="<?php echo $condicion; ?>" />

            <!-- <div class="sidebar-action-units">
                <button type="button" onclick="app.busqueda();" class="button external btn-primary col-md-12" rel="nofollow"><span><i class="fa fa-search"></i>&nbsp;Buscar</span></button>
            </div> -->
    </div>
</form>