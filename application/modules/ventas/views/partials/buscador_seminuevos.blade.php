<script>
    var anio_actual = <?php echo date('Y') ?>;
</script>

<form action="" method="get" data-trigger="filter">
    <div class="filter filter-sidebar ajax-filter">
        <div class="sidebar-entry-header">
            <span class="h4"> <i class="fa fa-search"></i>
                Buscador</span>
        </div>
        <form action="<?php echo site_url('ventas/autos_seminuevos'); ?>" method="get">
            <div class="row row-pad-top-24">
                <div class="col-md-12 col-sm-6 stm-filter_condition">
                    <h4>Autos seminuevos</h4>
                    <hr />
                </div>

                <div class="col-md-12 col-sm-6 stm-filter_body">
                    <div class="form-group">
                        <select name="tipo" model="CaTipoCarroceria_model" placeholder="Tipo" class="form-control" tabindex="-1" aria-hidden="true">
                        </select>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6 stm-filter_make">
                    <div class="form-group">
                        <select name="marca" model="CaMarcas_model" placeholder="Marca" class="form-control" tabindex="-1" aria-hidden="true">
                        </select>

                    </div>
                </div>
                <div class="col-md-12 col-sm-6 stm-filter_serie">
                    <div class="form-group">
                        <select name="modelo" model="CaModelos_model" placeholder="Modelo" class="form-control" tabindex="-1" aria-hidden="true">
                        </select>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6 stm-filter_mileage">
                    <div class="form-group">
                        <span><small>Kilometraje</small></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" class="form-control" placeholder="" name="kilometraje" value="<?php echo $kilometraje; ?>" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6 stm-filter_ca-year">
                    <div class="form-group">
                        <span><small>Año</small></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" class="form-control" placeholder="" name="anio" value="<?php echo $anio; ?>" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6 stm-filter_transmission">
                    <div class="form-group">
                        <select name="transmision" model="CaTipoTrasmision_model" placeholder="Transmisión" class="form-control " tabindex="-1" aria-hidden="true">
                        </select>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6 stm-filter_transmission">
                    <div class="form-group">
                        <span><small>Precio</small></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" class="form-control" placeholder="" name="precio" value="<?php echo $precio; ?>" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6 stm-filter_exterior-color">
                    <div class="form-group">
                        <select name="colorexterior" model="CaColores_model" placeholder="Color Exterior" class="form-control" tabindex="-1" aria-hidden="true">
                        </select>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6 stm-filter_interior-color">
                    <div class="form-group">
                        <select name="colorinterior" model="CaColores_model" placeholder="Color Interior" class="form-control" tabindex="-1" aria-hidden="true">
                        </select>
                    </div>
                </div>
            </div>
            <div class="sidebar-action-units">
                <button type="submit" class="button external btn-primary col-md-12" rel="nofollow"><span><i class="fa fa-search"></i>&nbsp;Buscar</span></button>
            </div>
        </form>
    </div>
</form>