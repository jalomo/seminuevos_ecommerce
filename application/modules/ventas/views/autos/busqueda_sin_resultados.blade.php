<div class="card text-center">
    <div class="card-body">
        <p>
            <i class="fas fa-search" style="font-size: 5rem;"></i>
            <br />
            <h3>No hay publicaciones que coincidan con tu búsqueda.</h3>
        </p>
    </div>
</div>