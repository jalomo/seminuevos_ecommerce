<div class="stm-ajax-row">
    <div class="stm-car-listing-sort-units clearfix">
        <span>Listado de búsqueda autos</span>
    </div>
    <div id="listings-result">
        <div class="stm-isotope-sorting stm-isotope-sorting-list">
            <?php if (is_array($resultados) && count($resultados) > 0) { ?>
                <?php foreach ($resultados as $resultado) { ?>
                    <div class="listing-list-loop stm-listing-directory-list-loop stm-isotope-listing-item ">
                        <div class="image">
                            <div id="carouselInterval_<?php echo $resultado['id']; ?>" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <?php if (is_array($resultado['imagenes']) && count($resultado['imagenes']) > 1) { ?>
                                        <?php foreach ($resultado['imagenes'] as $key => $imagen) { ?>
                                            <li data-target="#carouselInterval_<?php echo $resultado['id']; ?>" data-slide-to="<?php echo $key; ?>" class="<?php echo (($key == 0) ? 'active' : ''); ?>"></li>
                                        <?php } ?>
                                    <?php } ?>
                                </ol>
                                <div class="carousel-inner">
                                    <?php if (is_array($resultado['imagenes']) && count($resultado['imagenes']) > 0) { ?>
                                        <?php foreach ($resultado['imagenes'] as $key => $imagen) { ?>
                                            <div class="carousel-item <?php echo (($key == 0) ? 'active' : ''); ?>" data-interval="5000">
                                                <img src="<?php echo base_url('imagenes_autos/' . $imagen['file_imagen']); ?>" class="d-block w-100 img-thumbnail img-fluid"  onerror="this.src='<?php echo base_url('assets/img/no-imagen.jpg'); ?>';">
                                                <div class="carousel-caption d-none d-md-block">
                                                    <h5></h5>
                                                    <p></p>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                <?php if (is_array($resultado['imagenes']) && count($resultado['imagenes']) > 1) { ?>
                                    <a class="carousel-control-prev" href="#carouselInterval_<?php echo $resultado['id']; ?>" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselInterval_<?php echo $resultado['id']; ?>" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="content">
                            <div class="meta-top">
                                <div class="price">
                                    <?php                                     
                                    $webapp = utils::post('webapp') ? '?webapp=true' : ''; ?>
                                    <a href="<?php echo site_url('ventas/autos_nuevos/detalle/' . base64_encode($resultado['id']). $webapp); ?>" class="rmv_txt_drctn archive_request_price" data-toggle="modal" data-target="#get-car-price" data-title="" data-id="">
                                        <span class="heading-font price-form-label">$<?php echo utils::formatMoney($resultado['precio']); ?></span>
                                    </a>
                                </div>
                                <div class="title heading-font">
                                    <a href="<?php echo site_url('ventas/autos_nuevos/detalle/' . base64_encode($resultado['id']). $webapp); ?>" class="rmv_txt_drctn mt-3">
                                        <small class="bold">{{ $resultado['marca'] }} {{ $resultado['modelo'] }}
                                            {{ $resultado['tipo_carroceria'] }} {{ $resultado['anio'] }} </small></a><br />
                                    <small class="value h5" style="color:#767474">Folio# {{ $resultado['folio'] }} | {{ $resultado['sucursal'] }} {{ $resultado['agencia'] }}</small>
                                </div>
                            </div>
                            <div class="meta-middle">
                                <div class="meta-middle-unit font-exists fuel">
                                    <div class="meta-middle-unit-top">
                                        <div class="icon"><i class="fas fa-gas-pump"></i></div>
                                        <div class="name">Combustible</div>
                                    </div>
                                    <div class="value h5">
                                        {{ $resultado['combustible'] }}
                                    </div>
                                </div>
                                <div class="meta-middle-unit font-exists ca-year">
                                    <div class="meta-middle-unit-top">
                                        <div class="icon"><i class="far fa-calendar-alt"></i></div>
                                        <div class="name">Año</div>
                                    </div>
                                    <div class="value h5"> {{ $resultado['anio'] }} </div>
                                </div>
                                <div class="meta-middle-unit font-exists transmission">
                                    <div class="meta-middle-unit-top">
                                        <div class="icon"><i class="fas fa-cogs"></i></div>
                                        <div class="name">Transmisión</div>
                                    </div>

                                    <div class="value h5">
                                        {{ $resultado['tipo_trasmision'] }}
                                    </div>
                                </div>
                                <div class="meta-middle-unit font-exists drive">
                                    <div class="meta-middle-unit-top">
                                        <div class="icon"><i class="fas fa-tachometer-alt"></i></div>
                                        <div class="name">Kilómetros</div>
                                    </div>
                                    @if ($condicion == 2)
                                    <div class="value h5"> {{ $resultado['kilometraje'] }} </div>
                                    @else
                                    <div class="value h5"> 0 </div>
                                    @endif
                                </div>
                                <div class="meta-middle-unit font-exists transmission">
                                    <div class="meta-middle-unit-top">
                                        <div class="icon"><i class="fas fa-cogs"></i></div>
                                        <div class="name">Transmisión</div>
                                    </div>

                                    <div class="value h5">
                                        {{ $resultado['tipo_trasmision'] }}
                                    </div>
                                </div>
                                <div class="float-right mt-3">
                                    <?php if (is_array($resultado['certificados']) && count($resultado['certificados']) > 0) { ?>
                                        <?php foreach ($resultado['certificados'] as $key => $certificado) { ?>
                                                <img title="Certificado" src="<?php echo base_url('certificados_agencia/' . $certificado['file_imagen']); ?>" class="img-thumbnail_ img-fluid" style="max-height: 60px !important;" onerror="this.src='<?php echo base_url('assets/img/no-imagen.jpg'); ?>';">
                                        <?php } ?>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <!-- <div class="float-right">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item disabled"><a class="page-link" href="#">Anterior</a></li>
                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">Siguiente</a></li>
                </ul>
            </nav>
        </div> -->
    </div>
</div>