@layout('templates/layout_banner')

@section('contenido')
<script>
    var condicion = "{{ $data['condicion'] }}";
    var parametros_busqueda = {{ $data['busqueda'] }};
</script>
@include('partials/partial_banner')

<div class="container">
    <div class="vc_row wpb_row vc_row-fluid vc_custom_1448609395747">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="archive-listing-page">
                        <div class="container_">
                            <div class="row">
                                <div class="col-md-3 col-sm-12 classic-filter-row sidebar-sm-mg-bt ">
                                    @include('partials/buscador')
                                </div>
                                <div class="col-md-9 col-sm-12 " id="listado_busqueda">
                                    <center style="font-size: 6rem;" class="mt-5 mb-5">
                                        <i class="fas fa-spinner fa-spin"></i>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
@endsection

@section('scripts')

<link rel="stylesheet" href="{{ base_url('assets/components/rangeslider/ion.rangeSlider.min.css') }}" />
<script src="{{ base_url('assets/components/rangeslider/ion.rangeSlider.min.js') }}"></script>

<link href="{{ base_url('assets/components/pace/themes/red/pace-theme-minimal.css') }}" rel="stylesheet" />
<script src="{{ base_url('assets/components/pace/pace.min.js') }}"></script>

<script type="text/javascript" src="{{ base_url('assets/scripts/seminuevos_ecommerce/seminuevos_ecommerce/index.js') }}"></script>

@endsection