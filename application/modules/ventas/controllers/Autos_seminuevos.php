<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Autos_seminuevos extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index() {
      $this->load->model('CaBanners_model');

      $data = [
        'titulo' => 'Autos seminuevos',
        'mensaje' => 'Listado general de autos seminuevos'
      ];
      $layout = $this->input->get('webapp') ? 'templates/layoutframe' : 'templates/layout';

      $data = array(
        'layout' => $layout,
        'kilometraje' => utils::post('kilometraje'),
        'anio' => utils::post('anio'),
        'precio' => utils::post('precio'),
        'condicion' => 2,
        'titulo' => 'Autos seminuevos',
        'banners' => $this->CaBanners_model->getAll(),
        'busqueda' => (($this->input->get() != false)? json_encode($this->input->get()) : json_encode(array('condicion_id' => 2)) )
      );
      
      $this->blade->render('/autos/index', $data);

    }

    public function detalle($id = '') {

      $id = base64_decode($id);
      
      $this->load->model(['CaAutos_model','DeInformacionTecnica_model', 'DeFunciones_model','DeImagenes_model']);
      $info_tecnica = $this->DeInformacionTecnica_model->get(array('automovil_id' => $id));
      $de_funciones = $this->DeFunciones_model->getdetallefunciones(array('de_funciones.automovil_id' => $id));
      $detalle = $this->CaAutos_model->getFiltrado(array('ca_automovil.id' => $id));
      $imagenes = $this->DeImagenes_model->getAll(array('automovil_id' => $id));  

      $link_video = explode( '=', $detalle[0]['link_videos'] );
      $data = [
        'id' => $id,
        'detalle'=> !empty($detalle) ? $detalle[0] : [],
        'de_funciones' => isset($de_funciones)  ? $de_funciones : [],
        'info_tecnica'=> !empty($info_tecnica)  ? $info_tecnica : [],
        'titulo' => 'Detalle vehículo',
        'frame_url' => isset($link_video[1]) ? 'https://www.youtube.com/embed/'. $link_video[1] : '',
        'banners' => $imagenes,
      ];
      
      $this->blade->render('ventas/detalle/index', $data);

    }


  }
