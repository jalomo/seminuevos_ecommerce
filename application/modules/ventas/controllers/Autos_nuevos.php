<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Autos_nuevos extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->load->model('CaBanners_model');

    $data = [
      'titulo' => 'Autos nuevos',
      'mensaje' => 'Listado general de autos nuevos'
    ];
    $layout = $this->input->get('webapp') ? 'templates/layoutframe' : 'templates/layout';
    $data = array(
      'layout' => $layout,
      'kilometraje' => utils::post('kilometraje'),
      'anio' => utils::post('anio'),
      'precio' => utils::post('precio'),
      'condicion' => 1,
      'titulo' => 'Autos nuevos',
      'banners' => $this->CaBanners_model->getAll(),
      'busqueda' => (($this->input->get() != false) ? json_encode($this->input->get()) : json_encode(array('condicion_id' => 1)))
    );

    $this->blade->render('/autos/index', $data);
  }

  public function busqueda()
  {
    $find_params = array();
    if (utils::post('sucursal_id') != null) {
      $find_params['ca_automovil.sucursal_id'] = utils::post('sucursal_id');
    }
    if (utils::post('tipo') != null) {
      $find_params['ca_automovil.tipo_carroceria_id'] = utils::post('tipo');
    }

    if (utils::post('marca') != null) {
      $find_params['ca_automovil.marca_id'] = utils::post('marca');
    }

    if (utils::post('modelo') != null) {
      $find_params['ca_automovil.modelo_id'] = utils::post('modelo');
    }

    if (utils::post('transmision') != null) {
      $find_params['ca_automovil.tipo_transmision_id'] = utils::post('transmision');
    }

    if (utils::post('colorexterior') != null) {
      $find_params['ca_automovil.color_exterior_id'] = utils::post('colorexterior');
    }

    if (utils::post('colorinterior') != null) {
      $find_params['ca_automovil.color_interior_id'] = utils::post('colorinterior');
    }
    $this->db->where($find_params);

    $kilometraje = (utils::post('kilometraje') != null) ? explode('|', utils::post('kilometraje')) : null;
    if ($kilometraje  != null && is_array($kilometraje) && count($kilometraje) == 2) {
      $this->db->where('ca_automovil.kilometraje BETWEEN "' . current($kilometraje) . '" and "' . end($kilometraje) . '"');
    }

    $anio = (utils::post('anio') != null) ? explode('|', utils::post('anio')) : null;
    if ($anio  != null && is_array($anio) && count($anio) == 2) {
      $this->db->where('ca_automovil.anio BETWEEN "' . current($anio) . '" and "' . end($anio) . '"');
    }

    $precio = (utils::post('precio') != null) ? explode('|', utils::post('precio')) : null;
    if ($precio  != null && is_array($precio) && count($precio) == 2) {
      $this->db->where('ca_automovil.precio BETWEEN "' . current($precio) . '" and "' . end($precio) . '"');
    }

    $condicion_id = $this->input->post('condicion_id');
    $this->db->where('condicion_id', $condicion_id);

    $this->load->model('DeCertificados_model');
    $this->load->model('CaAutos_model');
    $resultados = $this->CaAutos_model->getFiltrado();
    // utils::pre($resultados);
    if (is_array($resultados)) {
      $this->load->model('DeImagenes_model');
      foreach ($resultados as $key => $resultado) {
        $resultados[$key]['folio'] = str_pad($resultado['id'], 6, "0", STR_PAD_LEFT);
        $resultados[$key]['imagenes'] = $this->DeImagenes_model->getAll(array(
          'automovil_id' => $resultado['id']
        ));
        $resultados[$key]['certificados'] = $this->DeCertificados_model->getAll(array(
          'agencia_id' => $resultado['agencia_id']
        ));
      }
    }
    $data = array(
      'kilometraje' => utils::post('kilometraje'),
      'anio' => utils::post('anio'),
      'precio' => utils::post('precio'),
      'condicion' => utils::post('condicion_id'),
      'resultados' => $resultados,
      'titulo' => 'Autos nuevos'
    );

    if (is_array($resultados) && count($resultados) > 0) {
      $html = utf8_decode($this->blade->render('/autos/busqueda', $data, true));
    } else {
      $html = utf8_decode($this->blade->render('/autos/busqueda_sin_resultados', $data, true));
    }

    $response = array(
      'status' => 'OK',
      'contenido' => base64_encode($html)
    );

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
      ->_display();
    exit;
  }

  public function detalle($id = '')
  {

    $id = base64_decode($id);

    $this->load->model(['CaAutos_model', 'DeInformacionTecnica_model', 'DeFunciones_model', 'DeImagenes_model']);
    $info_tecnica = $this->DeInformacionTecnica_model->get(array('automovil_id' => $id));
    $de_funciones = $this->DeFunciones_model->getdetallefunciones(array('de_funciones.automovil_id' => $id));
    $detalle = $this->CaAutos_model->getFiltrado(array('ca_automovil.id' => $id));
    // utils::pre($detalle);
    $imagenes = $this->DeImagenes_model->getAll(array('automovil_id' => $id));
    $layout = $this->input->get('webapp') ? 'templates/layoutframe' : 'templates/layout';

    $link_video = explode('=', $detalle[0]['link_videos']);
    $data = [
      'id' => $id,
      'layout' => $layout,
      'detalle' => !empty($detalle) ? $detalle[0] : [],
      'de_funciones' => isset($de_funciones)  ? $de_funciones : [],
      'info_tecnica' => !empty($info_tecnica)  ? $info_tecnica : [],
      'titulo' => 'Detalle vehículo',
      'frame_url' => isset($link_video[1]) ? 'https://www.youtube.com/embed/' . $link_video[1] : '',
      'banners' => $imagenes,
      'mensaje' => 'Detalle del vehículo tal con tales caracterisitcas'
    ];

    $this->blade->render('ventas/detalle/index', $data);
  }

  public function guardar_contacto()
  {
    //utils::pre($_POST);
    $response = array();

    $this->load->library('form_validation');
    $this->form_validation->set_rules('ubicacion_prueba_manejo', 'Donde desea tomar su prueba de manejo', 'trim|required');
    $this->form_validation->set_rules('nombre', 'nombre', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('apellido1', 'Primer Apellido', 'trim|required|max_length[255]');
    $this->form_validation->set_rules(
      'telefono',
      'Teléfono',
      'trim|required|numeric|exact_length[10]',
      array('validar_telefono' => 'El número de Teléfono no es válido')
    );
    $this->form_validation->set_rules('correo_electronico', 'Correo electrónico', 'trim|required|valid_email|max_length[255]');

    if ($this->input->post('ubicacion_prueba_manejo') == 'domicilio') {
      $this->form_validation->set_rules('domicilio', 'Domicilio', 'trim|required|max_length[2000]');
    }

    if (!array_key_exists('imagen_identificacion', $_FILES)) {
      $this->form_validation->set_rules('imagen_identificacion', 'Identificación oficial (Delantera)', 'trim|required');
    }
    if (!array_key_exists('imagen_identificacion2', $_FILES)) {
      $this->form_validation->set_rules('imagen_identificacion2', 'Identificación oficial (Trasera)', 'trim|required');
    }

    if (!array_key_exists('imagen_licencia', $_FILES)) {
      $this->form_validation->set_rules('imagen_licencia', 'Licencia para conducir (Delantera)', 'required');
    }
    if (!array_key_exists('imagen_licencia2', $_FILES)) {
      $this->form_validation->set_rules('imagen_licencia2', 'Licencia para conducir (Trasera)', 'required');
    }

    if ($this->form_validation->run($this) == FALSE) {
      $response['status'] = 'error';
      $response['message'] = $this->form_validation->error_array();
    } else {
      $path = FCPATH . "documentos_prueba_manejo" . DIRECTORY_SEPARATOR;

      $archivo_fisico_LICENCIA = utils::getUniqueName($_FILES['imagen_licencia']['name']);
      $fichero = $_FILES['imagen_licencia']['tmp_name'];
      $nuevo_fichero_LICENCIA = $path . $archivo_fisico_LICENCIA;

      $archivo_fisico_LICENCIA2 = utils::getUniqueName($_FILES['imagen_licencia2']['name']);
      $fichero2 = $_FILES['imagen_licencia2']['tmp_name'];
      $nuevo_fichero_LICENCIA2 = $path . $archivo_fisico_LICENCIA2;

      if (!copy($fichero2, $nuevo_fichero_LICENCIA2) || !copy($fichero, $nuevo_fichero_LICENCIA)) {
        $response['message']['imagen_licencia'] = 'Ocurrió un error inesperado al momento de procesar el archivo';
      } else {

        $archivo_fisico_IDENTIFICACION = utils::getUniqueName($_FILES['imagen_identificacion']['name']);
        $fichero = $_FILES['imagen_identificacion']['tmp_name'];
        $nuevo_fichero_IDENTIFICACION = $path . $archivo_fisico_IDENTIFICACION;

        $archivo_fisico_IDENTIFICACION2 = utils::getUniqueName($_FILES['imagen_identificacion2']['name']);
        $fichero2 = $_FILES['imagen_identificacion2']['tmp_name'];
        $nuevo_fichero_IDENTIFICACION2 = $path . $archivo_fisico_IDENTIFICACION2;

        if (!copy($fichero, $nuevo_fichero_IDENTIFICACION) || !copy($fichero2, $nuevo_fichero_IDENTIFICACION2)) {
          $response['message']['imagen_identificacion'] = 'Ocurrió un error inesperado al momento de procesar el archivo';
        } else {
          $this->load->model('CaContactos_model');
          $id = $this->CaContactos_model->insert(array(
            'automovil_id' => $this->input->post('automovil_id'),
            'tipo_ubicacion' => $this->input->post('ubicacion_prueba_manejo'),
            'nombre' => $this->input->post('nombre'),
            'apellido1' => $this->input->post('apellido1'),
            'apellido2' => $this->input->post('apellido2'),
            'telefono' => $this->input->post('telefono'),
            'correo_electronico' => $this->input->post('correo_electronico'),
            'domicilio' => $this->input->post('domicilio'),
            'latitud' => $this->input->post('latitud'),
            'longitud' => $this->input->post('longitud'),
            'imagen_identificacion' => $archivo_fisico_IDENTIFICACION,
            'imagen_licencia' => $archivo_fisico_LICENCIA,
            'imagen_identificacion2' => $archivo_fisico_IDENTIFICACION2,
            'imagen_licencia2' => $archivo_fisico_LICENCIA2
          ));
          $response['status'] = 'ok';
          $response['data'] = $id;
        }
      }
    }
  }
  
  public function validar_telefono($str)
  {
    if (strlen($str) > 0) {
      $pattern = '/^([0-9]{4})(-)([0-9]{10})$/';
      $pattern_2 = '/^([0-9]{3})(-)([0-9]{4})(-)([0-9]{4})$/';
      return ((preg_match($pattern, $str)) || (preg_match($pattern_2, $str))) ? true : false;
    }
  }

  public function cotizar()
  {

    $response = array();

    $this->load->library('form_validation');
    $this->form_validation->set_rules('precio_', 'Precio', 'trim|required|numeric');
    $this->form_validation->set_rules('intereses_', 'Intereses', 'trim|required|numeric');
    $this->form_validation->set_rules('periodo', 'Periodo', 'trim|required');
    $this->form_validation->set_rules('pago_inicial_', 'Pago inicial', 'trim|required|numeric');

    if ($this->form_validation->run($this) == FALSE) {
      $response['status'] = 'error';
      $response['validations'] = $this->form_validation->error_array();
    } else {

      $precio = $this->input->post('precio_');
      $intereses = $this->input->post('intereses_');
      $periodo = $this->input->post('periodo');
      $pagominimo = $this->input->post('pagominimo');
      $pago_inicial = $this->input->post('pago_inicial_');

      if ($pago_inicial < $pagominimo) {
        $response['status'] = 'error';
        $response['message'] = 'El pago minimo inicial no puede ser menor a $' . utils::formatMoney($pagominimo);
      } else {

        $subtotal = $precio - $pago_inicial;
        $mensualidad = round($subtotal / $periodo);
        $mensualidad_intereses = (($mensualidad * $intereses) / 100) + $mensualidad;
        $data['mensualidad_interes'] = utils::formatMoney($mensualidad_intereses);
        $total_sin_pago_inicial = ($mensualidad_intereses * $periodo);

        $data['subtotal'] = utils::formatMoney($total_sin_pago_inicial);
        $total_de_totales = $total_sin_pago_inicial + $pago_inicial;
        $data['total'] = utils::formatMoney($total_de_totales);
        $data['total_intereses'] = utils::formatMoney($total_de_totales - $precio);


        $response['status'] = 'ok';
        $response['respuesta'] = $data;
      }
    }

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
      ->_display();
    exit;
  }
}
