<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Inicio extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('CaAutos_model');
        $this->load->library('form_validation');
    }

    public function index() {

      $this->load->model('CaBanners_model');
      $banners = $this->CaBanners_model->getAll();

      $data = [
        'titulo' => 'Principal',
        'banners' => $banners,
        'mensaje' => 'Las mejores promociones en autos nuevos y seminuevos',
        'autos' => $this->CaAutos_model->getFiltrado(array('condicion_id' => 1)),
        'seminuevos' => $this->CaAutos_model->getFiltrado(array('condicion_id' => 2))
      ];
      $this->blade->render('inicio/principal', $data);

    }

    public function login() {

      if($this->input->server('REQUEST_METHOD') == 'POST'){
        $this->form_validation->set_rules('password', 'Contraseña', 'required');
        $this->form_validation->set_rules('usuario', 'Usuario', 'required|callback_username_check',
          array('username_check' => 'El nombre de usuario o la contraseña son incorrectos.')
        );
        
        if ($this->form_validation->run($this) != FALSE){
          redirect('administrador');
        }
      }

      $data = [
        'titulo' =>  '<img style="height:70px" src="'.base_url('assets/img/xehosautos.png').'" class="img-fluid" />',
      ];

      $this->blade->render('inicio/login', $data);

    }

    public function username_check() {

      if(strlen($this->input->post('usuario')) >0){

        $DB_login = $this->load->database('login', TRUE);
        $query = $DB_login->from('admin')->where(array(
          'adminUsername' => $this->input->post('usuario'),
          'adminPassword' => md5(sha1($this->input->post('password')))
        ))->get();
        $result = $query->num_rows();
        $result_data = $query->row_array();
        
        if ($result >  0) {
          $this->session->set_userdata($result_data);
          $this->session->set_userdata('session_tipo', 2);
          $this->session->set_userdata('logged_in', true);
          $this->session->set_userdata('agencia', false);
          $this->session->set_userdata('sucursal', false);

          $this->load->model('CaPermisos_model');
          $permisos = $this->CaPermisos_model->getAll();
          $this->session->set_userdata('permisos', $permisos);

          return TRUE;
        } else {

          $DB_login = $this->load->database('default', TRUE);
          $query = $DB_login->from('ca_usuarios')->where(array(
            'usuario' => $this->input->post('usuario'),
            'contrasena' => md5($this->input->post('password'))
          ))->get();
          $result = $query->num_rows();
          $result_data = $query->row_array();

          if ($result >  0) {
            $this->session->set_userdata($result_data);
            $this->session->set_userdata('session_tipo', 1);
            $this->session->set_userdata('logged_in', true);

            $this->load->model('CaPermisos_model');
            $permisos = $this->CaPermisos_model->re_agencias_premiso(array(
              'id_agencia' => $result_data['agencia_id']
            ));
            $this->session->set_userdata('permisos', $permisos);

            $query = $DB_login->from('ca_agencia')->where(array(
              'id' => $result_data['agencia_id']
            ))->get();
            $result_data = $query->row_array();
            $this->session->set_userdata('agencia', $result_data);
            
            $query = $DB_login->from('ca_sucursal')->where(array(
              'id' => $result_data['sucursal_id']
            ))->get();
            $result_data = $query->row_array();
            $this->session->set_userdata('sucursal', $result_data);

            return TRUE;
          }

          return FALSE;
        }
    }
  }

    


  }
