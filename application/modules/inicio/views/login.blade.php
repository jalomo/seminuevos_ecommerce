@layout('templates/layout')

@section('contenido')
<div class="container">
    <div class="row">
        <div class="col-md-5 col-centered mt-5 mb-5 panel-theme">
            <p>Iniciar sesión para poder acceder al sistema</p>
            <form name="login" action="<?php echo site_url('inicio/login'); ?>" method="post">
                <div class="form-group">
                    <label><b>Usuario</b></label>
                    <input type="text" name="usuario" class="form-control" value="jalomo" />
                    <small id="msg_usuario" class="form-text text-danger"><?php echo form_error('usuario'); ?></small>
                </div>
                <div class="form-group">
                    <label><b>Contraseña</b></label>
                    <input type="password" name="password" class="form-control" value="libros" />
                    <small id="msg_password" class="form-text text-danger"><?php echo form_error('password'); ?></small>
                </div>
                <div class="float-right">
                    <button type="submit" id="loginbtn" class="btn btn-primary"><i class="fa fa-play"></i> Entrar</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
</script>
@endsection