@layout('templates/layout_banner')

@section('css')
<style>
    .car-listing-tabs-unit .car-listing-top-part:before {
        background-color: #0e3256;
    }
</style>
@endsection

@section('contenido')

@include('banners/partial_banner')

<div class="container">
    <div class="vc_row wpb_row vc_row-fluid vc_custom_1448544673381">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="car-listing-tabs-unit listing-cars-id-30554">
                        <div class="car-listing-top-part">
                            <div class="found-cars-cloned found-cars-71002"></div>
                            <div class="title">
                                <h1 style="font-size: 36px;"><span style="color: #ffffff;">Listado de </span> <span class="stm-base-color">Autos</span></h1>
                            </div>
                            <div class="stm-listing-tabs">
                                <ul class="nav nav-tabs row">
                                    <li class="nav-item col-lg-5 col-6">
                                        <a class="nav-link active" href="#car-listing-category-new-cars" role="tab" data-toggle="tab" aria-selected="true">Autos nuevos </a>
                                    </li>
                                    <li class="nav-item col-lg-5 col-6">
                                        <a class="nav-link" href="#car-listing-category-used-cars" role="tab" data-toggle="tab" aria-selected="false">Autos seminuevos </a>
                                    </li>
                                    <!-- <li class="nav-item">
                                        <a class="nav-link" href="#car-listing-tab-search" role="tab" data-toggle="tab" aria-selected="false"> Buscar inventario </a>
                                    </li> -->
                                </ul>
                            </div>
                        </div>

                        <div class="car-listing-main-part">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="car-listing-category-new-cars">
                                    <div class="found-cars-clone">
                                        <div style="color:#fff" class="found-cars heading-font"><i class="stm-icon-car"></i>available <span class="blue-lt">22&nbsp;cars</span>
                                        </div>
                                    </div>
                                    <div class="row row-4 car-listing-row">
                                        <?php foreach($autos as $auto) { ?>
                                            <div class="col-md-3 col-sm-4 col-xs-12 col-xxs-12 stm-template-front-loop">
                                                <a href="<?php echo site_url('ventas/autos_nuevos/detalle/'.base64_encode($auto['id'])); ?>" class="rmv_txt_drctn xx external" rel="nofollow">
                                                    <div class="image">
                                                        <img width="255" height="135" src="{{ base_url('imagenes_autos/'.$auto['file_imagen']) }}" onerror="this.src='<?php echo base_url('assets/img/no-imagen.jpg');?>'" class="attachment-stm-img-255-135 size-stm-img-255-135" alt="" loading="lazy"/>
                                                        <!-- <div class="stm-badge-directory heading-font stm-badge-dealer"> Special </div> -->
                                                    </div>
                                                    <div class="listing-car-item-meta">
                                                        <div class="car-meta-top heading-font clearfix">
                                                            <div class="price">
                                                                <div class="normal-price">$ {{ utils::formatMoney($auto['precio']) }}</div>
                                                            </div>
                                                            <div class="car-title">{{ $auto['marca'] }} {{ $auto['modelo'] }} {{ $auto['tipo_carroceria'] }} {{ $auto['anio'] }} </div>
                                                        </div>
                                                        <div class="car-meta-bottom">
                                                            <ul>
                                                                <li><i class="fa fa-wrench"></i><span>{{ $auto['tipo_traccion'] }}</span></li>
                                                                <li><i class="fa fa-road"></i><span>{{ $auto['combustible'] }}</span></li>
                                                                <li><i class="fa fa-car"></i><span>{{ $auto['tipo_trasmision'] }}</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane " id="car-listing-category-used-cars">
                                    <div class="found-cars-clone">
                                        <div style="color:#fff" class="found-cars heading-font"><i class="stm-icon-car"></i>available <span class="blue-lt">19&nbsp;cars</span>
                                        </div>
                                    </div>
                                    <div class="row row-4 car-listing-row">
                                        <?php foreach($seminuevos as $auto) { ?>
                                            <div class="col-md-3 col-sm-4 col-xs-12 col-xxs-12 stm-template-front-loop">
                                                <a href="<?php echo site_url('ventas/autos_seminuevos/detalle/'.base64_encode($auto['id'])); ?>" class="rmv_txt_drctn xx external" rel="nofollow">
                                                    <div class="image">
                                                        <img width="255" height="135" src="{{ base_url('imagenes_autos/'.$auto['file_imagen']) }}" onerror="this.src='<?php echo base_url('assets/img/no-imagen.jpg');?>'" class="attachment-stm-img-255-135 size-stm-img-255-135" alt="" loading="lazy"/>
                                                        <!-- <div class="stm-badge-directory heading-font stm-badge-dealer"> Special </div> -->
                                                    </div>
                                                    <div class="listing-car-item-meta">
                                                        <div class="car-meta-top heading-font clearfix">
                                                            <div class="price">
                                                                <div class="normal-price">$ {{ utils::formatMoney($auto['precio']) }}</div>
                                                            </div>
                                                            <div class="car-title">{{ $auto['marca'] }} {{ $auto['modelo'] }} {{ $auto['tipo_carroceria'] }} {{ $auto['anio'] }} </div>
                                                        </div>
                                                        <div class="car-meta-bottom">
                                                            <ul>
                                                                <li><i class="fa fa-wrench"></i><span>{{ $auto['tipo_traccion'] }}</span></li>
                                                                <li><i class="fa fa-road"></i><span>{{ $auto['combustible'] }}</span></li>
                                                                <li><i class="fa fa-car"></i><span>{{ $auto['tipo_trasmision'] }}</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        <?php } ?>

                                    </div>
                                </div>
                                <!--Search tab-->
                                <!-- <div role="tabpanel" class="tab-pane " id="car-listing-tab-search">

                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
      $("#carouselbanners > .carousel-inner > .carousel-item").first().addClass('active')
    </script>
@endsection