<div id="carouselbanners" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      @if (isset($data['banners']) && is_array($data['banners']))
        @foreach ($data['banners'] as $item)
            <div class="carousel-item">
              <img  class="d-block w-100" src="{{ base_url($item['ruta'].$item['nombre_archivo']) }}" alt="Third slide">
            </div>
        @endforeach
        @else
        <div class="carousel-item active">
          <img  class="d-block w-100" src="{{ base_url('/assets/banners/banner1.png') }}" alt="Third slide">
        </div>
      @endif
    </div>
    <a class="carousel-control-prev" href="#carouselbanners" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Atras</span>
    </a>
    <a class="carousel-control-next" href="#carouselbanners" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Siguiente</span>
    </a>
  </div>