var app;
var APP_VIEW = Backbone.View.extend({
    el: 'body',
    table: 'table-catalogos',
    events: {
        "click button#btn-modal-agregar": "saveform",
        "click button#btn-modal-editar": "updateform",
    },
    
    crearTabla: () => {
        $('table#' + app.table).dataTable({
            language: utils.datatablesConfig,
            order: [
                [0, 'asc']
            ],
            ajax: PATH+'/administrador/usuarios/getAll',
            columns: [{
                    title: "#",
                    width: '40px',
                    data: 'id'
                },
                {
                    title: "Nombre",
                    data: 'nombre',
                },
                {
                    title: "Perfil",
                    data: 'perfil',
                },
                {
                    title: "Sucursal",
                    data: 'sucursal',
                },
                {
                    title: "Agencia",
                    data: 'agencia',
                },
                {
                    title: "Acciones",
                    defaultContent: '',
                    orderable: false,
                    width: '80px',
                    render: function(_, _, row) {
                        let btn_editar = '';
                        let btn_eliminar = '';
                        btn_editar = '<button  catalogo_id="'+row.id+'" nombre="'+row.nombre+'" usuario="'+row.usuario+'" correo_electronico="'+row.correo_electronico+'" telefono="'+row.telefono+'" sucursal_id="'+row.sucursal_id+'" agencia_id="'+row.agencia_id+'" perfil_id="'+row.perfil_id+'"  title="Editar" onclick="app.openModal(this)" data-catalogo_id="' + row.id + '" data-nombre="' + row.nombre + '" class="btn btn-primary btn-sm"><i class="fa fa-pencil-alt"></i></button>';
                        btn_eliminar = '<button title="Eliminar" onclick="app.delete(this)" data-catalogo_id="' + row.id + '" data-nombre="' + row.nombre + '" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                        return btn_editar + ' ' + btn_eliminar;
                    }
                }
            ]
        });
    },

    delete: (_this) => {
        title = 'Estas seguro de eliminar el registro?';
        tipo = 'info';
        sendData = {
            'id': $(_this).data('catalogo_id'),
            'model': 'CaUsuarios_model'
        }
        var callback = function(result) {
            if (result.value) {
                $.ajax({
                    dataType: "json",
                    type: 'POST',
                    url: PATH + '/administrador/usuarios/delete',
                    data: sendData,
                    success: function(result, status, xhr) {
                        if (result.estatus == 'error') {
                            toastr.error(result.mensaje);
                        } else {
                            if (result.data) {
                                toastr.success(result.mensaje);
                                $('table#' + app.table).DataTable().ajax.reload();
                            }
                        }
                    }
                });
            }
        }
        utils.displayWarningDialog(title, tipo, callback);
    },
    openModal: (_this) => {
        var catalogo_id = $(_this).attr('catalogo_id')
        if(catalogo_id == 0 || catalogo_id == undefined){
            $('form[name="form_content"]').trigger("reset");
            $('h5#title_modal').html('Agregar usuario')
            $('div#div_contrasena').show()
            $('input[name=catalogo_id]').val(0);
        }else{
            $('h5#title_modal').html('Editar usuario')
            $('div#div_contrasena').hide()
            $('input[name=catalogo_id]').val(catalogo_id);

            var nombre = $(_this).attr('nombre');
            $('input[name=nombre]').val(nombre);

            var usuario = $(_this).attr('usuario');
            $('input[name=usuario]').val(usuario);

            var correo_electronico = $(_this).attr('correo_electronico');
            $('input[name=correo_electronico]').val(correo_electronico);

            var telefono = $(_this).attr('telefono');
            $('input[name=telefono]').val(telefono);

            var sucursal_id = $(_this).attr('sucursal_id');
            $('select[name=sucursal_id] option[value='+sucursal_id+']').prop('selected',true);

            var agencia_id = $(_this).attr('agencia_id');
            if (agencia_id >= 1) {
                $("select[name*='agencia_id']").attr('data',agencia_id);
                ajax.getCatalogoSimple("getAgenciaBySucursal?sucursal_id=" + sucursal_id, $("select[name*='agencia_id']"),true);
            }

            var perfil_id = $(_this).attr('perfil_id');
            $('select[name=perfil_id] option[value='+perfil_id+']').prop('selected',true);

        }

        
        $('#modal-catalogo').modal('show');
        
    },
    getAgencias: () => {
        let sucursal_id = $("select[name*='sucursal_id']").val();
        if (sucursal_id >= 1) {
            ajax.getCatalogoSimple("getAgenciaBySucursal?sucursal_id=" + sucursal_id, $("select[name*='agencia_id']"));
        }
    },
    saveform: () => {

        $('small.form-text.text-danger').html('');
        var url = ($('input[name=catalogo_id]').val() == 0)? PATH + '/administrador/usuarios/nuevo_guardar' : PATH + '/administrador/usuarios/editar_guardar';

        var sendData = $('form[name="form_content"]').serializeArray();
        Pace.track(function() {
            $.ajax({
                dataType: "json",
                type: 'POST',
                url: url,
                data: sendData,
                success: function(data, status, xhr) {
                    if (data.status == 'error') {
                        if (data.message != undefined) {
                            $.each(data.message, function(index, value) {
                                try {
                                    if ($('small#msg_' + index ).length) {
                                        $('small#msg_' + index ).html(value)
                                    }
                                } catch (error) {

                                }
                            });
                        }
                    } else {
                        $('#modal-catalogo').modal('hide');
                        $('form[name="form_content"]').trigger("reset");
                        message.success('El usuario se ha guardado correctamente',function() {
                            $('table#' + app.table).DataTable().ajax.reload();
                        });
                    }
                }
            });
        });
    },
    updateform: () => {
        var sendData = $('form[name="formCatalogo"]').serializeArray();
        sendData.push({
            name: 'model',
            value: app.model,
        });
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + app.api + '/update',
            data: sendData,
            success: function(result, status, xhr) {
                if (result.estatus == 'error') {
                    errors.mostrar(result.info);
                } else {
                    if (result.data) {
                        $('#modal-catalogo').modal('hide');
                        message.success(result.mensaje, function() {
                            app.getBusqueda();
                        });
                    }
                }
            }
        });
    },

    render: function() {
        // $('select[name=sucursal_id]').select2();
        // $('select[name=agencia_id]').select2();

        this.crearTabla();
        // this.getBusqueda();
    }
});


$(function() {
    app = new APP_VIEW();
    app.render();
})