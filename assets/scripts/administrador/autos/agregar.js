var app;
var APP_VIEW = Backbone.View.extend({
    el: 'body',
    automovil_id: '',

    events: {
        "click button#guardar": "saveform",
        "click button#guardarInformacionTecnica": "saveformTecnica",
        "click button#guardarFunciones": "saveformFunciones",
        "change #sucursal_id": "getAgencias",
    },

    crearTabla: () => {
        $('table#table-imagenes').dataTable(app.configuracionTabla());
    },

    configuracionTabla: () => {
        return {
            language: utils.datatablesConfig,
            lengthChange: false,
            searching: false,
            destroy: true,
            responsive: true,
            bInfo: false,
            paging: true,
            order: [
                [0, 'asc']
            ],
            columns: [{
                    title: "#",
                    data: 'id'
                },
                {
                    title: "Imagen",
                    data: 'nombre_imagen',
                    render: function(_, _, row) {
                        return '<img class="img-fluid" style="width:100px" src="' + BASE_URL + '/imagenes_autos/' + row.file_imagen + '">'
                    }
                },
                {
                    title: "Nombre",
                    data: 'nombre_imagen',
                },
                {
                    title: "Acciones",
                    defaultContent: '',
                    orderable: false,
                    render: function(_, _, row) {
                        let btn_ver = '';
                        let btn_eliminar = '';
                        btn_eliminar = '<button title="Ir" onclick="app.eliminarFile(this)" data-catalogo_id="' + row.id + '" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                        return btn_eliminar;
                    }
                }
            ]
        }
    },
    eliminarFile: (_this) => {
        $.ajax({
            dataType: "json",
            type: 'GET',
            url: PATH + '/administrador/autos/deleteFile?file_id=' + $(_this).data('catalogo_id'),
            success: function(response, status, xhr) {
                console.log(response.data);
                if (response.data) {
                    message.success(response.mensaje, function() {
                        app.getBusquedaImagenes();
                    });
                } else {
                    message.error(response.mensaje);
                }

            }
        });
    },

    getBusquedaImagenes: () => {
        $.ajax({
            dataType: "json",
            type: 'GET',
            url: PATH + '/administrador/autos/getImagenesByAuto?auto_id=' + $("[name='automovil_id']").val(),
            success: function(response, status, xhr) {
                var listado = $('table#table-imagenes').DataTable();
                listado.clear().draw();
                if (response.data && response.data.length > 0) {
                    response.data.forEach(listado.row.add);
                    listado.draw();
                }
            }
        });
    },

    saveform: () => {
        var sendData = $('form[name="formAuto"]').serializeArray();
        // sendData.push({
        //     name: 'contenido',
        //     value: CKEDITOR.instances.contenido.getData(),
        // });
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/administrador/autos/save_caracteristicas',
            data: sendData,
            success: function(result, status, xhr) {
                if (result.estatus == 'error') {
                    errors.mostrar(result.info);
                } else {
                    if (result.data) {
                        message.success(result.mensaje, function() {
                            app.automovil_id = result.data.id;
                            window.location.href = PATH + '/administrador/autos/continuar?auto_id=' + result.automovil_id + '&tab=2'
                        });
                    }
                }
            }
        });
    },

    saveformTecnica: () => {
        var sendData = $('form[name="formTecnica"]').serializeArray();
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/administrador/autos/save_informacion_tecnica',
            data: sendData,
            success: function(result, status, xhr) {
                if (result.estatus == 'error') {
                    errors.mostrar(result.info);
                } else {
                    if (result.data) {
                        message.success(result.mensaje, function() {
                            window.location.href = PATH + '/administrador/autos/continuar?auto_id=' + result.automovil_id + '&tab=3'
                        });
                    }
                }
            }
        });
    },

    saveformFunciones: () => {
        var sendData = $('form[name="formFunciones"]').serializeArray();
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/administrador/autos/save_funciones',
            data: sendData,
            success: function(result, status, xhr) {
                if (result.estatus == 'error') {
                    errors.mostrar(result.info);
                } else {
                    if (result.data) {
                        message.success(result.mensaje, function() {
                            window.location.href = PATH + '/administrador/autos/continuar?auto_id=' + result.automovil_id + '&tab=4'
                        });
                    }
                }
            }
        });
    },

    getAgencias: () => {
        let sucursal_id = $("select[name*='sucursal_id']").val();
        if (sucursal_id >= 1) {
            ajax.getCatalogo("getAgenciaBySucursal?sucursal_id=" + sucursal_id, $("select[name*='agencia_id']"), true);
        }
    },

    render: function() {

        ajax.getCatalogo("getAll?model=CaMarcas_model", $("select[name*='marca_id']"), true);
        ajax.getCatalogo("getAll?model=CaCondicion_model", $("select[name*='condicion_id']"), true);
        ajax.getCatalogo("getAll?model=CaSucursales_model", $("select[name*='sucursal_id']"), true);
        ajax.getCatalogo("getAll?model=CaTipoCarroceria_model", $("select[name*='tipo_carroceria_id']"), true);
        ajax.getCatalogo("getAll?model=CaModelos_model", $("select[name*='modelo_id']"), true);
        ajax.getCatalogo("getAll?model=CaTipoTrasmision_model", $("select[name*='tipo_transmision_id']"), true);
        ajax.getCatalogo("getAll?model=CaTipoCombustible_model", $("select[name*='tipo_combustible_id']"), true);
        ajax.getCatalogo("getAll?model=CaColores_model", $("select[name*='color_exterior_id']"), true);
        ajax.getCatalogo("getAll?model=CaColores_model", $("select[name*='color_interior_id']"), true);
        ajax.getCatalogo("getAll?model=CaTipoTraccion_model", $("select[name*='tipo_traccion_id']"), true);
        app.crearTabla();
        app.getBusquedaImagenes();
        app.getAgencias();
    }
});


$(function() {
    app = new APP_VIEW();
    app.render();
})