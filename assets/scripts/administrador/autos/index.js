var app;
var APP_VIEW = Backbone.View.extend({
    el: 'body',
    table: 'table-autos',

    events: {
        "click button#mostrarModal": "mostrar",
        "click button#guardarCatalogo": "guardar",
        "click button#editarCatalogo": "editar",
    },

    crearTabla: () => {
        $('table#table-autos').dataTable(app.configuracionTabla());
    },

    configuracionTabla: () => {
        return {
            language: utils.datatablesConfig,
            order: [
                [0, 'asc']
            ],
            columns: [{
                    title: "#",
                    data: 'id'
                },
                {
                    title: "Marca",
                    data: 'marca',
                },
                {
                    title: "Modelo",
                    data: 'modelo',
                },
                {
                    title: "Tipo carroceria",
                    data: 'tipo_carroceria',
                },
                {
                    title: "Precio",
                    data: 'precio',
                },
                {
                    title: "Año",
                    data: 'anio',
                },
                {
                    title: "Acciones",
                    defaultContent: '',
                    orderable: false,
                    render: function(_, _, row) {
                        let btn_editar = '';
                        let btn_eliminar = '';
                        btn_editar = '<button title="Ir" onclick="app.goTo(this)" data-catalogo_id="' + row.id + '" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button>';
                        return btn_editar + ' ' + btn_eliminar;
                    }
                }
            ]
        }
    },

    getBusqueda: () => {
        $.ajax({
            dataType: "json",
            type: 'GET',
            url: PATH + '/administrador/autos/getAll?condicion_id=' + $("#condicion_id").val(),
            success: function(response, status, xhr) {
                var listado = $('table#' + app.table).DataTable();
                listado.clear().draw();
                if (response.data && response.data.length > 0) {
                    response.data.forEach(listado.row.add);
                    listado.draw();
                }
            }
        });
    },

    goTo: function(_this) {
        let id = $(_this).data('catalogo_id');
        window.location.href = PATH + '/administrador/autos/continuar?auto_id=' + id + '&tab=1';

    },

    render: function() {
        this.crearTabla();
        this.getBusqueda();
    }
});


$(function() {
    app = new APP_VIEW();
    app.render();
})