var app;
var APP_VIEW = Backbone.View.extend({
    el: 'body',

    openModal: function(id){

        Pace.track(function() {
            $.ajax({
                data: {id:id},
                type: "POST",
                dataType: "json",
                url: PATH + '/administrador/contacto/get',
            })
            .done(function( data, textStatus, jqXHR ) {
                $('div#modal_contacto').html( atob(data.contenido) );
                $('div#modal_contacto').modal();

                setTimeout(() => {
                    
                    
                        var map = new google.maps.Map($("div#map_prueba_manejo")[0], {
                            center: {
                                lat: parseFloat($('input#coordenada_y').val()),
                                lng: parseFloat($('input#coordenada_x').val())
                            },
                            zoom: 16
                        });
                        var maker_ = new google.maps.Marker({
                            position: {
                                lat: parseFloat($('input#coordenada_y').val()),
                                lng:parseFloat($('input#coordenada_x').val())
                            },
                            map
                        });

                }, 1500);
            })
        });
    },

    render: function() {
        
        $('table#table-contactos').dataTable({
            ajax: {
                "url": PATH + '/administrador/contacto/getAll',
                "type": "POST"
            },
            language: utils.datatablesConfig,
            order: [
                [0, 'desc']
            ],
            columns: [{
                    title: "#",
                    width: '40px',
                    data: 'id'
                },
                {
                    title: "Nombre",
                    data: 'nombre',
                },
                {
                    title: "Fecha",
                    data: 'fecha_creacion',
                    render: function ( data, type, row, meta ) {
                        const date = moment(data);
                        var item = '';
                        if(date.isValid()){
                            item = date.format("DD/MM/YYYY hh:mm A");
                        }
                        return item;
                    }
                },

                {
                    title: "Acciones",
                    defaultContent: '',
                    orderable: false,
                    width: '80px',
                    render: function(_, __, row) {
                        var btn_ver = '<button title="Editar" onclick="app.openModal('+row.id+')" data-catalogo_id="' + row.id + '" data-nombre="' + row.nombre + '" class="btn btn-primary btn-sm"><i class="far fa-list-alt"></i></button>';
                        return btn_ver;
                    }
                }
            ]
        });
        
    },
    abrir_imagen: function($this){
        var imagen = $($this).attr('imagen');

        var request = new XMLHttpRequest();  
        request.open('GET', atob(imagen), true);  
        request.onreadystatechange = function() {
            if (this.readyState == 4) {
                if (this.status == 404) {
                    toastr.warning('No se encuentra el archivo o no se puede abrir')
                }else{
                    Swal.fire({
                        imageUrl: atob(imagen),
                        confirmButtonText: 'Cerrar',
                        width: '900px'
                    });
                }
            }
        }
        request.send();  
        
        
    }
});

function initMap(){}

$(function() {
    app = new APP_VIEW();
    app.render();
})