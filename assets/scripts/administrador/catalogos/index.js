var app;
var APP_VIEW = Backbone.View.extend({
    el: 'body',
    table: 'table-catalogos',
    api: '/administrador/catalogos/',
    name: nombre,
    model: modelo,

    events: {
        "click button#btn-modal-agregar": "saveform",
        "click button#btn-modal-editar": "updateform",
    },
    crearTabla: () => {
        $('table#' + app.table).dataTable(app.configuracionTabla());
    },

    configuracionTabla: () => {
        return {
            language: utils.datatablesConfig,
            order: [
                [0, 'asc']
            ],
            columns: [{
                    title: "#",
                    width: '40px',
                    data: 'id'
                },
                {
                    title: "Nombre",
                    data: 'nombre',
                },
                {
                    title: "Acciones",
                    defaultContent: '',
                    orderable: false,
                    width: '80px',
                    render: function(_, _, row) {
                        let btn_editar = '';
                        let btn_eliminar = '';
                        btn_editar = '<button title="Editar" onclick="app.openModal(this)" data-catalogo_id="' + row.id + '" data-nombre="' + row.nombre + '" class="btn btn-primary btn-sm"><i class="fa fa-pencil-alt"></i></button>';
                        btn_eliminar = '<button title="Eliminar" onclick="app.delete(this)" data-catalogo_id="' + row.id + '" data-nombre="' + row.nombre + '" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                        return btn_editar + ' ' + btn_eliminar;
                    }
                }
            ]
        }
    },
    delete: (_this) => {
        title = 'Estas seguro de eliminar el registro?';
        tipo = 'info';
        sendData = {
            'id': $(_this).data('catalogo_id'),
            'model': app.model
        }
        var callback = function(result) {
            if (result.value) {
                $.ajax({
                    dataType: "json",
                    type: 'POST',
                    url: PATH + app.api + '/delete',
                    data: sendData,
                    success: function(result, status, xhr) {
                        if (result.estatus == 'error') {
                            toastr.error(result.mensaje);
                        } else {
                            if (result.data) {
                                toastr.success(result.mensaje);
                                app.getBusqueda();
                            }
                        }
                    }
                });
            }
        }
        utils.displayWarningDialog(title, tipo, callback);
    },
    openModal: (_this) => {
        let catalogo_id = $(_this).data('catalogo_id');
        $('[name="nombre"]').val($(_this).data('nombre'));
        $('[name="catalogo_id"]').val(catalogo_id);
        if (utils.isDefined(catalogo_id)) {
            $("#title_modal").html('Editar ' + app.name);
            $("#btn-modal-agregar").hide();
            $("#btn-modal-editar").show();
        } else {
            $("#title_modal").html('Agregar ' + app.name);
            $("#btn-modal-agregar").show();
            $("#btn-modal-editar").hide();
        }

        $('#modal-catalogo').modal();
    },
    getBusqueda: () => {
        $.ajax({
            dataType: "json",
            type: 'GET',
            url: PATH + app.api + '/getAll?model=' + app.model,
            success: function(response, status, xhr) {
                var listado = $('table#' + app.table).DataTable();
                listado.clear().draw();
                if (response.data && response.data.length > 0) {
                    response.data.forEach(listado.row.add);
                    listado.draw();
                }
            }
        });
    },
    saveform: () => {
        var sendData = $('form[name="formCatalogo"]').serializeArray();
        sendData.push({
            name: 'model',
            value: app.model,
        });
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + app.api + '/store',
            data: sendData,
            success: function(result, status, xhr) {
                if (result.estatus == 'error') {
                    errors.mostrar(result.info);
                } else {
                    if (result.data) {
                        $('#modal-catalogo').modal('hide');
                        message.success(result.mensaje, function() {
                            app.getBusqueda();
                        });
                    }
                }
            }
        });
    },
    updateform: () => {
        var sendData = $('form[name="formCatalogo"]').serializeArray();
        sendData.push({
            name: 'model',
            value: app.model,
        });
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + app.api + '/update',
            data: sendData,
            success: function(result, status, xhr) {
                if (result.estatus == 'error') {
                    errors.mostrar(result.info);
                } else {
                    if (result.data) {
                        $('#modal-catalogo').modal('hide');
                        message.success(result.mensaje, function() {
                            app.getBusqueda();
                        });
                    }
                }
            }
        });
    },

    render: function() {
        this.crearTabla();
        this.getBusqueda();
    }
});


$(function() {
    app = new APP_VIEW();
    app.render();
})