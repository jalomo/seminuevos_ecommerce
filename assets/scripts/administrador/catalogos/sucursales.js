var app;
var APP_VIEW = Backbone.View.extend({
    el: 'body',
    table: 'table-catalogos',
    api: '/administrador/sucursales/',
    name: nombre,
    model: modelo,
    valorx: 19.264810,
    valory: -103.703050,

    events: {
        "click button#btn-modal-agregar": "saveform",
        "click button#btn-modal-editar": "updateform",
    },
    crearTabla: () => {
        $('table#' + app.table).dataTable(app.configuracionTabla());
    },

    configuracionTabla: () => {
        return {
            language: utils.datatablesConfig,
            order: [
                [0, 'asc']
            ],
            columns: [{
                    title: "#",
                    width: '40px',
                    data: 'id'
                },
                {
                    title: "Nombre",
                    data: 'nombre',
                },
                {
                    title: "Marca",
                    data: 'marca',
                },
                {
                    title: "Acciones",
                    defaultContent: '',
                    orderable: false,
                    width: '80px',
                    render: function(_, _, row) {
                        let btn_editar = '';
                        let btn_eliminar = '';
                        btn_editar = '<button title="Editar" onclick="app.openModal(this)" data-catalogo_id="' + row.id + '" data-nombre="' + row.nombre + '" data-coordenada_x="' + row.coordenada_x + '" data-marca="' + row.marca_id + '"  class="btn btn-primary btn-sm"><i class="fa fa-pencil-alt"></i></button>';
                        btn_eliminar = '<button title="Eliminar" onclick="app.delete(this)" data-catalogo_id="' + row.id + '" data-nombre="' + row.nombre + '" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                        return btn_editar + ' ' + btn_eliminar;
                    }
                }
            ]
        }
    },
    delete: (_this) => {
        title = 'Estas seguro de eliminar el registro?';
        tipo = 'info';
        sendData = {
            'id': $(_this).data('catalogo_id'),
            'model': app.model
        }
        var callback = function(result) {
            if (result.value) {
                $.ajax({
                    dataType: "json",
                    type: 'POST',
                    url: PATH + app.api + '/delete',
                    data: sendData,
                    success: function(result, status, xhr) {
                        if (result.estatus == 'error') {
                            toastr.error(result.mensaje);
                        } else {
                            if (result.data) {
                                toastr.success(result.mensaje);
                                app.getBusqueda();
                            }
                        }
                    }
                });
            }
        }
        utils.displayWarningDialog(title, tipo, callback);
    },
    openModal: (_this) => {
        let catalogo_id = $(_this).data('catalogo_id');

        $('[name="nombre"]').val($(_this).data('nombre'));
        $('[name="catalogo_id"]').val(catalogo_id);
        if (utils.isDefined(catalogo_id)) {
            let lat = utils.isDefined($(_this).data('coordenada_x')) ? $(_this).data('coordenada_x') : app.valorx;
            let long = utils.isDefined($(_this).data('coordenada_y')) ? $(_this).data('coordenada_y') : app.valory;
            app.getCoordenadas(lat, long);
            $("#title_modal").html('Editar ' + app.name);
            $('[name="coordenada_x"]').val(lat);
            $('[name="coordenada_y"]').val(long);
            $('[name="marca"]').val($(_this).data('marca'));

            $("#btn-modal-agregar").hide();
            $("#btn-modal-editar").show();
        } else {
            app.getCoordenadas(app.valorx, app.valory);
            $("#title_modal").html('Agregar ' + app.name);
            $('[name="coordenada_x"]').val(app.valorx);
            $('[name="coordenada_y"]').val(app.valory);
            $("#btn-modal-agregar").show();
            $("#btn-modal-editar").hide();
        }
        ajax.getCatalogo("getAll?model=CaMarcas_model", $("select[name*='marca']"));

        $('div#modal-sucursal').modal();
    },
    getCoordenadas: (lat, long) => {
        const myLatLng = {
            lat: parseFloat(lat),
            lng: parseFloat(long)
        };
        var newLatLng = new google.maps.LatLng(parseFloat(lat), parseFloat(long));
        marker.setPosition(newLatLng);
        map.setCenter(myLatLng);
        map.setZoom(16);
    },
    getBusqueda: () => {
        $.ajax({
            dataType: "json",
            type: 'GET',
            url: PATH + app.api + '/getAll?model=' + app.model,
            success: function(response, status, xhr) {
                var listado = $('table#' + app.table).DataTable();
                listado.clear().draw();
                if (response.data && response.data.length > 0) {
                    response.data.forEach(listado.row.add);
                    listado.draw();
                }
            }
        });
    },
    saveform: () => {
        var sendData = $('form[name="formCatalogo"]').serializeArray();
        sendData.push({
            name: 'model',
            value: app.model,
        });
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + app.api + '/store',
            data: sendData,
            success: function(result, status, xhr) {
                if (result.estatus == 'error') {
                    errors.mostrar(result.info);
                } else {
                    if (result.data) {
                        $('#modal-sucursal').modal('hide');
                        message.success(result.mensaje, function() {
                            app.getBusqueda();
                        });
                    }
                }
            }
        });
    },
    updateform: () => {
        var sendData = $('form[name="formCatalogo"]').serializeArray();
        sendData.push({
            name: 'model',
            value: app.model,
        });
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + app.api + '/update',
            data: sendData,
            success: function(result, status, xhr) {
                if (result.estatus == 'error') {
                    errors.mostrar(result.info);
                } else {
                    if (result.data) {
                        $('#modal-sucursal').modal('hide');
                        message.success(result.mensaje, function() {
                            app.getBusqueda();
                        });
                    }
                }
            }
        });
    },

    render: function() {
        this.valorx = 19.264810;
        this.valory = -103.703050;
        this.crearTabla();
        this.getBusqueda();
    }
});


$(function() {
    app = new APP_VIEW();
    app.render();
})

let map, marker, geocoder;
let markers = [];

function initMap() {
    let valorx = 19.264810;
    let valory = -103.703050;
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: new google.maps.LatLng(valorx, valory),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    geocoder = new google.maps.Geocoder();

    const myLatLng = {
        lat: valorx,
        lng: valory
    };
    marker = new google.maps.Marker({
        map: map,
        position: myLatLng,
        draggable: true
    });
    map.setCenter(myLatLng);
    map.setZoom(16);

    google.maps.event.addListener(marker, 'dragend', function(evt) {
        $("#coordenada_x").val(evt.latLng.lat().toFixed(6));
        $("#coordenada_y").val(evt.latLng.lng().toFixed(6));
        map.panTo(evt.latLng);
    });
}