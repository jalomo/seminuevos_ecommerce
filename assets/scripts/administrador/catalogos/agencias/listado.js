var app;
var APP_VIEW = Backbone.View.extend({
    el: 'body',
    table: 'table-catalogos',
    api: '/administrador/agencias/',
    name: nombre,

    crearTabla: () => {
        $('table#' + app.table).dataTable(app.configuracionTabla());
    },

    configuracionTabla: () => {
        return {
            language: utils.datatablesConfig,
            order: [
                [0, 'asc']
            ],
            columns: [{
                    title: "#",
                    width: '40px',
                    data: 'id'
                },
                {
                    title: "Agencia",
                    data: 'nombre',
                },
                {
                    title: "Sucursal",
                    data: 'sucursal',
                },
                {
                    title: "Acciones",
                    defaultContent: '',
                    orderable: false,
                    width: '120px',
                    render: function(_, _, row) {
                        let btn_editar = '';
                        let btn_eliminar = '';
                        btn_certificados = '<button title="Certificados" onclick="app.gotoCertificados(this)" data-catalogo_id="' + row.id + '" class="btn btn-dark btn-sm"><i class="fa fa-file-image"></i></button>';
                        btn_editar = '<button title="Editar" onclick="app.gotoEditar(this)" data-catalogo_id="' + row.id + '" class="btn btn-primary btn-sm"><i class="fa fa-pencil-alt"></i></button>';
                        btn_eliminar = '<button title="Eliminar" onclick="app.delete(this)" data-catalogo_id="' + row.id + '" data-nombre="' + row.nombre + '" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                        return btn_certificados + ' ' + btn_editar + ' ' + btn_eliminar;
                    }
                }
            ]
        }
    },
    delete: (_this) => {
        title = 'Estas seguro de eliminar el registro?';
        tipo = 'info';
        sendData = {
            'id': $(_this).data('catalogo_id'),
        }
        var callback = function(result) {
            if (result.value) {
                $.ajax({
                    dataType: "json",
                    type: 'POST',
                    url: PATH + app.api + '/delete',
                    data: sendData,
                    success: function(result, status, xhr) {
                        if (result.estatus == 'error') {
                            toastr.error(result.mensaje);
                        } else {
                            if (result.data) {
                                toastr.success(result.mensaje);
                                app.getBusqueda();
                            }
                        }
                    }
                });
            }
        }
        utils.displayWarningDialog(title, tipo, callback);
    },

    getBusqueda: () => {
        $.ajax({
            dataType: "json",
            type: 'GET',
            url: PATH + app.api + '/getAll',
            success: function(response, status, xhr) {
                var listado = $('table#' + app.table).DataTable();
                listado.clear().draw();
                if (response.data && response.data.length > 0) {
                    response.data.forEach(listado.row.add);
                    listado.draw();
                }
            }
        });
    },

    gotoEditar(_this) {
        window.location.href = PATH + '/administrador/agencias/editar?agencia_id=' + $(_this).data('catalogo_id')
    },

    gotoCertificados(_this) {
        window.location.href = PATH + '/administrador/agencias/certificados?agencia_id=' + $(_this).data('catalogo_id')
    },

    render: function() {
        this.crearTabla();
        this.getBusqueda();
    }
});


$(function() {
    app = new APP_VIEW();
    app.render();
})