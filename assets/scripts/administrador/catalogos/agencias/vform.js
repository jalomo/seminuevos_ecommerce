var app;
var APP_VIEW = Backbone.View.extend({
    el: 'body',
    table: 'table-catalogos',
    api: '/administrador/agencias/',
    name: nombre,
    valorx: 19.264810,
    valory: -103.703050,

    events: {
        "click button#btn-guardar": "saveform",
        "click button#btn-editar": "saveform",
    },

    obtenerDatos: () => {
        let catalogo_id = $('[name="catalogo_id"]').val();

        if (utils.isDefined(catalogo_id) && catalogo_id) {
            let lat = $('[name="coordenada_x"]').val();
            let long = $('[name="coordenada_y"]').val();
            app.getCoordenadas(lat, long);
            $('[name="coordenada_x"]').val(lat);
            $('[name="coordenada_y"]').val(long);
        } else {
            app.getCoordenadas(app.valorx, app.valory);
            $('[name="coordenada_x"]').val(app.valorx);
            $('[name="coordenada_y"]').val(app.valory);
        }
        ajax.getCatalogo("getAll?model=CaSucursales_model", $("select[name*='sucursal_id']"), true);
    },

    getCoordenadas: (lat, long) => {
        const myLatLng = {
            lat: parseFloat(lat),
            lng: parseFloat(long)
        };
        var newLatLng = new google.maps.LatLng(parseFloat(lat), parseFloat(long));
        marker.setPosition(newLatLng);
        map.setCenter(myLatLng);
        map.setZoom(12);
    },

    saveform: () => {
        var sendData = $('form[name="formAgencias"]').serializeArray();
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + app.api + '/saveform',
            data: sendData,
            success: function(result, status, xhr) {
                if (result.estatus == 'error') {
                    errors.mostrar(result.info);
                } else {
                    if (result.data) {
                        message.success(result.mensaje, function() {
                            window.location.href = PATH + 'administrador/agencias/index'
                        });
                    }
                }
            }
        });
    },
    render: function() {
        this.valorx = 19.264810;
        this.valory = -103.703050;
        this.obtenerDatos();
    }
});


$(function() {
    app = new APP_VIEW();
    app.render();
})

let map, marker, geocoder;
let markers = [];

function initMap() {
    let valorx = 19.264810;
    let valory = -103.703050;
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: new google.maps.LatLng(valorx, valory),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    geocoder = new google.maps.Geocoder();

    const myLatLng = {
        lat: valorx,
        lng: valory
    };
    marker = new google.maps.Marker({
        map: map,
        position: myLatLng,
        draggable: true
    });
    map.setCenter(myLatLng);
    map.setZoom(16);

    google.maps.event.addListener(marker, 'dragend', function(evt) {
        $("#coordenada_x").val(evt.latLng.lat().toFixed(6));
        $("#coordenada_y").val(evt.latLng.lng().toFixed(6));
        map.panTo(evt.latLng);
    });
}