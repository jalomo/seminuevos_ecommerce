var app;
var APP_VIEW = Backbone.View.extend({
    el: 'body',
    events: {
        "click button#guardar": "saveform",
    },

    crearTabla: () => {
        $('table#table-certificados').dataTable(app.configuracionTabla());
    },

    configuracionTabla: () => {
        return {
            language: utils.datatablesConfig,
            lengthChange: false,
            searching: false,
            destroy: true,
            responsive: true,
            bInfo: false,
            paging: true,
            order: [
                [0, 'asc']
            ],
            columns: [{
                    title: "#",
                    data: 'id'
                },
                {
                    title: "Imagen",
                    data: 'nombre_imagen',
                    render: function(_, _, row) {
                        return '<img src="' + BASE_URL + '/certificados_agencia/' + row.file_imagen + '">'
                    }
                },
                {
                    title: "Nombre",
                    data: 'nombre_imagen',
                },
                {
                    title: "Acciones",
                    defaultContent: '',
                    orderable: false,
                    render: function(_, _, row) {
                        let btn_editar = '';
                        let btn_eliminar = '';
                        btn_eliminar = '<button title="Ir" onclick="app.eliminarFile(this)" data-catalogo_id="' + row.id + '" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                        return btn_eliminar;
                    }
                }
            ]
        }
    },
    eliminarFile: (_this) => {
        $.ajax({
            dataType: "json",
            type: 'GET',
            url: PATH + '/administrador/agencias/deleteFile?file_id=' + $(_this).data('catalogo_id'),
            success: function(response, status, xhr) {
                console.log(response.data);
                if (response.data) {
                    message.success(response.mensaje, function() {
                        app.getBusquedaCertificados();
                    });
                } else {
                    message.error(response.mensaje);
                }

            }
        });
    },

    getBusquedaCertificados: () => {
        $.ajax({
            dataType: "json",
            type: 'GET',
            url: PATH + '/administrador/agencias/getCertificadosByAgencia?agencia_id=' + $("[name='agencia_id']").val(),
            success: function(response, status, xhr) {
                var listado = $('table#table-certificados').DataTable();
                listado.clear().draw();
                if (response.data && response.data.length > 0) {
                    response.data.forEach(listado.row.add);
                    listado.draw();
                }
            }
        });
    },



    render: function() {
        app.crearTabla();
        app.getBusquedaCertificados();

    }
});


$(function() {
    app = new APP_VIEW();
    app.render();
})