var app;
var APP_VIEW = Backbone.View.extend({
    el: 'body',
    compra_auto_id: '',
    count_ext: 0,
    count_int: 0,

    events: {
        "click button#guardar": "saveform",
    },

    crearTablaImagenes: () => {
        $('table#table-imagenes').dataTable(app.configuracionTabla());
    },

    configuracionTabla: () => {
        return {
            language: utils.datatablesConfig,
            lengthChange: false,
            searching: false,
            destroy: true,
            responsive: true,
            bInfo: false,
            paging: true,
            order: [
                [0, 'asc']
            ],
            columns: [{
                    title: "#",
                    data: 'id'
                },
                {
                    title: "Imagen",
                    data: 'nombre_imagen',
                    render: function(_, _, row) {
                        return '<img class="img-fluid" style="width:100px" src="' + BASE_URL + '/compra_autos/' + row.file_imagen + '">'
                    }
                },
                {
                    title: "Tipo",
                    render: function(_, _, row) {
                        return row.tipo_imagen == 1 ? 'Externo' : 'Interior';
                    }
                },
                {
                    title: "Nombre",
                    data: 'nombre_imagen',
                },
                {
                    title: "Acciones",
                    defaultContent: '',
                    orderable: false,
                    render: function(_, _, row) {
                        let btn_ver = '';
                        let btn_eliminar = '';
                        btn_eliminar = '<button title="Ir" onclick="app.eliminarFile(this)" data-catalogo_id="' + row.id + '" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                        return btn_eliminar;
                    }
                }
            ]
        }
    },
    eliminarFile: (_this) => {
        $.ajax({
            dataType: "json",
            type: 'GET',
            url: PATH + '/compras/compramos_auto/deleteFile?file_id=' + $(_this).data('catalogo_id'),
            success: function(response, status, xhr) {
                if (response.data) {
                    message.success(response.mensaje, function() {
                        app.getImagenesByAuto();
                    });
                } else {
                    message.error(response.mensaje);
                }

            }
        });
    },

    publicar: function(){
        if(app.count_ext >= 4 && app.count_int >= 4){

            $.ajax({
                dataType: "json",
                type: 'GET',
                url: PATH + '/compras/compramos_auto/publicar?compra_auto_id=' + $("[name='compra_auto_id']").val(),
                success: function(result, status, xhr) {
                    message.success(result.mensaje, function() {
                        app.compra_auto_id = result.data.id;
                        window.location.reload();
                        // window.location.href = PATH + '/compras/compramos_auto/continuar?compra_auto_id=' + result.compra_auto_id + '&tab=2'
                    });
                }
            });

        }else{
            toastr.error('Favor de adjuntar como mínimo 4 imagenes externas y 4 imagenes internas de su vehículo')
        }
    },

    getImagenesByAuto: () => {
        var $_this = this;
        $.ajax({
            dataType: "json",
            type: 'GET',
            url: PATH + '/compras/compramos_auto/getImagenesByAuto?compra_auto_id=' + $("[name='compra_auto_id']").val(),
            success: function(response, status, xhr) {
                var listado = $('table#table-imagenes').DataTable();
                listado.clear().draw();
                if (response.data && response.data.length > 0) {
                    response.data.forEach(listado.row.add);
                    listado.draw();

                    app.count_ext = response.count_ext;
                    app.count_int = response.count_int;

                    // if(response.data.count_ext >= 4 && response.data.count_int >= 4){
                        $('button#publicar_anuncio').show();
                    // }
                }else{
                    $('button#publicar_anuncio').show();
                }
            }
        });
    },

    saveform: () => {
        var sendData = $('form[name="formAuto"]').serializeArray();
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/compras/compramos_auto/save',
            data: sendData,
            success: function(result, status, xhr) {
                if (result.estatus == 'error') {
                    errors.mostrar(result.info);
                } else {
                    if (result.data) {
                        message.success(result.mensaje, function() {
                            app.compra_auto_id = result.data.id;
                            window.location.href = PATH + '/compras/compramos_auto/continuar?compra_auto_id=' + result.compra_auto_id + '&tab=2'
                        });
                    }
                }
            }
        });
    },

    render: function() {

        ajax.getCatalogo("getAll?model=CaMarcas_model", $("select[name*='marca_id']"), true);
        ajax.getCatalogo("getAll?model=CaCondicion_model", $("select[name*='condicion_id']"), true);
        ajax.getCatalogo("getAll?model=CaTipoCarroceria_model", $("select[name*='tipo_carroceria_id']"), true);
        ajax.getCatalogo("getAll?model=CaModelos_model", $("select[name*='modelo_id']"), true);
        ajax.getCatalogo("getAll?model=CaTipoTrasmision_model", $("select[name*='tipo_transmision_id']"), true);
        ajax.getCatalogo("getAll?model=CaTipoCombustible_model", $("select[name*='tipo_combustible_id']"), true);
        ajax.getCatalogo("getAll?model=CaColores_model", $("select[name*='color_exterior_id']"), true);
        ajax.getCatalogo("getAll?model=CaColores_model", $("select[name*='color_interior_id']"), true);
        ajax.getCatalogo("getAll?model=CaTipoTraccion_model", $("select[name*='tipo_traccion_id']"), true);
        app.crearTablaImagenes();
        app.getImagenesByAuto();
    }
});


$(function() {
    app = new APP_VIEW();
    app.render();
})