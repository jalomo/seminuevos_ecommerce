var app;
var APP_VIEW = Backbone.View.extend({
    el: 'body',
    table: 'table-autos',
    auto_actual: null,
    events: {
        "click button#mostrarModal": "mostrar",
        "click button#guardarCatalogo": "guardar",
        "click button#editarCatalogo": "editar",
    },

    crearTabla: () => {
        $('table#table-autos').dataTable(app.configuracionTabla());
    },

    abrir_modal: (id) => {
        app.auto_actual = id;
        $.ajax({
            dataType: "json",
            type: 'GET',
            url: PATH + '/administrador/autos_ofertados/get_ofertas',
            data: { id: id },
            success: function(response, status, xhr) {
                $('div#staticBackdrop .modal-body').html(atob(response.html));
                $('div#staticBackdrop').modal('show');

            }
        });
    },

    rechazar: (id) => {

        Swal.fire({
            title: '¿Esta seguro de rechazar la oferta?',
            showCancelButton: true,
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    dataType: "json",
                    type: 'POST',
                    url: PATH + '/administrador/autos_ofertados/aceptar_rechazar',
                    data: { id: id, auto: app.auto_actual, estatus: 3 },
                    success: function(response, status, xhr) {
        
                        $('div#staticBackdrop').modal('hide');
                        app.abrir_modal(app.auto_actual);
                    }
                });
            }
        })
    },

    aceptar: (id) => {

        Swal.fire({
            title: '¿Esta seguro de aceptar la oferta?',
            showCancelButton: true,
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    dataType: "json",
                    type: 'POST',
                    url: PATH + '/administrador/autos_ofertados/aceptar_rechazar',
                    data: { id: id, auto: app.auto_actual, estatus: 2 },
                    success: function(response, status, xhr) {
        
                        $('div#staticBackdrop').modal('hide');
                        Swal.fire({
                            title: 'Sus datos se han guardado correctamente',
                            text: 'En breve un asesor se comunicará con usted y le enviará información.',
                            icon: 'success',
                            confirmButtonText: 'Aceptar'
                        }).then((result) => {
                            location.reload()
                        })
                    }
                });
            }
        })

        
    },

    configuracionTabla: () => {
        return {
            language: utils.datatablesConfig,
            order: [
                [0, 'asc']
            ],
            columns: [{
                    title: "#",
                    data: 'id'
                },
                {
                    title: "Marca",
                    data: 'marca',
                },
                {
                    title: "Modelo",
                    data: 'modelo',
                },
                {
                    title: "Tipo carroceria",
                    data: 'tipo_carroceria',
                },
                {
                    title: "Precio",
                    data: 'precio',
                },
                {
                    title: "Año",
                    data: 'anio',
                },
                {
                    title: "Acciones",
                    defaultContent: '',
                    orderable: false,
                    render: function(_, _, row) {
                        let btn_editar = '';
                        let btn_eliminar = '';
                        btn_editar = '<button title="Ir" onclick="app.goTo(this)" data-catalogo_id="' + row.id + '" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button>';
                        return btn_editar + ' ' + btn_eliminar;
                    }
                }
            ]
        }
    },

    getBusqueda: () => {
        $.ajax({
            dataType: "json",
            type: 'GET',
            url: PATH + '/compras/compramos_auto/getAll',
            success: function(response, status, xhr) {
                var listado = $('table#' + app.table).DataTable();
                listado.clear().draw();
                if (response.data && response.data.length > 0) {
                    response.data.forEach(listado.row.add);
                    listado.draw();
                }
            }
        });
    },

    goTo: function(_this) {
        let id = $(_this).data('catalogo_id');
        window.location.href = PATH + '/compras/compramos_auto/continuar?compra_auto_id=' + id + '&tab=1';

    },

    render: function() {
        this.crearTabla();
        this.getBusqueda();
    }
});


$(function() {
    app = new APP_VIEW();
    app.render();
})