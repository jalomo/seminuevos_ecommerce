var app;
var APP_VIEW = Backbone.View.extend({
    el: 'body',

    goTo: function(_this) {
        let id = $(_this).data('catalogo_id');
        window.location.href = PATH + '/administrador/autos_nuevos/continuar?auto_id=' + id;

    },

    busqueda: function() {
        this.realizar_busqueda($('form#form_busqueda').serializeArray());
    },

    realizar_busqueda: function(parametros_busqueda) {

        try {
            window.history.pushState('', '', url_actual + '?' + $.param(parametros_busqueda));
        } catch (error) {

        }

        Pace.track(function() {
            $.ajax({
                    data: parametros_busqueda,
                    type: "POST",
                    dataType: "json",
                    url: PATH + "/ventas/autos_nuevos/busqueda",
                })
                .done(function(data, textStatus, jqXHR) {
                    $('div#listado_busqueda').html(atob(data.contenido));
                })
        });
    },

    render: function() {
        // CARGAR SELECTS
        $('div.classic-filter-row select.form-control').each(function(index) {
            var item = $(this);
            var modelo = item.attr('model');
            var name = item.attr('name');
            var placeholder = item.attr('placeholder');

            if (modelo != undefined) {

                var item_value = null;
                var items_value = utils.getAllUrlParam();
                $.each(items_value, function(index, value) {
                    if (index == name) {
                        item_value = value;
                    }
                });
                item.attr('data', item_value);

                ajax.getCatalogo("getAll?model=" + modelo, item, true, function() {
                    $(item).select2({
                        placeholder: placeholder,
                        allowClear: true,
                        language: {
                            noResults: function() {
                                return "No hay resultado";
                            }
                        }
                    });

                    $(item).attr('onchange', 'app.busqueda();');

                });
            }
        });

        if ($('input[name=kilometraje]').length) {
            $('input[name=kilometraje]').ionRangeSlider({
                type: "double",
                grid: true,
                min: 0,
                max: 250000,
                drag_interval: true,
                step: 10000,
                input_values_separator: "|"
            });
        }

        if ($('input[name=anio]').length) {
            $('input[name=anio]').ionRangeSlider({
                type: "double",
                grid: true,
                min: 1990,
                max: anio_actual,
                drag_interval: true,
                input_values_separator: "|"
            });
        }

        if ($('input[name=precio]').length) {
            $('input[name=precio]').ionRangeSlider({
                type: "double",
                grid: true,
                min: 10000,
                max: 1000000,
                drag_interval: true,
                step: 10000,
                prefix: "$",
                input_values_separator: "|"
            });
        }




    }
});

$(function() {
    app = new APP_VIEW();
    if (typeof(parametros_busqueda) != 'undefined') {
        app.realizar_busqueda(parametros_busqueda);
    } else {
        app.realizar_busqueda({});
    }
    app.render();
})