var app;
var APP_VIEW = Backbone.View.extend({
    el: 'body',

    goTo: function(_this) {
        let id = $(_this).data('catalogo_id');
        window.location.href = PATH + '/administrador/autos_nuevos/continuar?auto_id=' + id;

    },

    openModal: (_this) => {
        $("#modalyoutube").modal("show");
        $("#frameyoutube").attr('src', $(_this).data('frame'))
    },
    map: null,
    maker: null,
    geocoder: null,
    initMap: function(lat, lng, zoom) {
        app.map = new google.maps.Map($("div#map_prueba_manejo")[0], {
            center: {
                lat: lat,
                lng: lng
            },
            zoom: zoom
        });
        app.geocoder = new google.maps.Geocoder();
        var map = app.map;
        app.maker = new google.maps.Marker({
            position: {
                lat: lat,
                lng: lng
            },
            map,
            draggable: true
                // title: "Hello World!",
        });

        app.geocoder.geocode({
            'latLng': app.maker.getPosition()
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    $('textarea[name=domicilio]').val(results[0].formatted_address);
                    $('input[name=lat]').val(results[0].geometry.location.lat());
                    $('input[name=lng]').val(results[0].geometry.location.lng());
                }
            }
        });

        google.maps.event.addListener(app.maker, 'dragend', function() {
            geocoder.geocode({
                'latLng': app.maker.getPosition()
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('textarea[name=domicilio]').val(results[0].formatted_address);
                        $('input[name=lat]').val(results[0].geometry.location.lat());
                        $('input[name=lng]').val(results[0].geometry.location.lng());
                    }
                }
            });
        });
    },

    buscar_direccion() {
        var domicilio = $('textarea[name=domicilio]').val();
        app.geocodeAddress(app.geocoder, app.map, domicilio);
    },

    search: function() {
        if (event.keyCode == 13) {
            app.buscar_direccion();
        }
    },

    geocodeAddress: function(geocoder, resultsMap, address) {
        geocoder.geocode({
            'address': address
        }, function(results, status) {
            if (status === 'OK') {
                resultsMap.setCenter(results[0].geometry.location);
                resultsMap.setZoom(16);
                app.maker.setPosition(results[0].geometry.location);
                $('input[name=lng]').val(resultsMap.center.lng());
                $('input[name=lat]').val(resultsMap.center.lat());
            } else {
                toastr.info('El domicilio no fue localizado, favor de verificar');
            }
        });
    },

    //Error Callback
    show_error: function(error) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
                toastr.info("Permiso denegado por el usuario");
                break;
            case error.POSITION_UNAVAILABLE:
                toastr.info("Ubicación no disponible");
                break;
            case error.TIMEOUT:
                toastr.info("Se agotó el tiempo de espera de la solicitud para obtener la ubicación del usuario");
                // case error.UNKNOWN_ERROR:
                // toastr.info("Error desconocido.");
                // break;
        }
    },

    adjuntar_archivo: function() {
        var formData = new FormData(document.getElementById("form_contacto"));
        $.ajax({
            url: PATH + '/administrador/agencias/save_certificados',
            type: "post",
            dataType: "json",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(result, status, xhr) {
                $('.custom-file-label').removeClass("selected").html('');
                if (result.estatus == 'error') {
                    if (result.mensaje) {
                        message.error(result.mensaje, true);
                    } else {
                        errors.mostrar(result.info);
                    }
                } else {
                    if (result.data) {
                        $("form#formCertificados")[0].reset();
                        message.success(result.mensaje, function() {
                            app.getBusquedaCertificados();
                        });
                    }
                }
            }
        })
    },

    ubicacion: function(tipo) {
        if (tipo != '') {
            if (tipo == 'agencia') {
                $('div.ubicacion_agencia').show();
                $('div.ubicacion_domicilio').hide();

                var lat = parseFloat($('input[name=lat_agencia]').val());
                var lng = parseFloat($('input[name=lng_agencia]').val());
                app.initMap(lat, lng, 16);

            } else {
                $('div.ubicacion_agencia').hide();
                $('div.ubicacion_domicilio').show();

                if ("geolocation" in navigator) { //check geolocation available 
                    //try to get user current location using getCurrentPosition() method
                    navigator.geolocation.getCurrentPosition(function(position) {
                        setTimeout(() => {
                            $('input[name=lng]').val(position.coords.longitude);
                            $('input[name=lat]').val(position.coords.latitude);
                            app.initMap(position.coords.latitude, position.coords.longitude, 16);
                        }, 50);
                    }, app.show_error, {
                        timeout: 1000,
                        enableHighAccuracy: true
                    }); //position request
                } else {
                    toastr.info("El navegador no admite la geolocalización");
                    app.initMap(20.6738686, -103.3705185, 16);
                }
            }

        } else {
            app.initMap(20.6738686, -103.3705185, 6);
        }
    },

    guardar_contacto: function() {

        $('small.form-text.text-danger').html('');
        var formData = new FormData();
        formData.append('ubicacion_prueba_manejo', $('select#ubicacion_prueba_manejo option:selected').val());
        formData.append('domicilio', $('textarea[name=domicilio]').val());
        formData.append('telefono', $('input[name=telefono]').val());
        formData.append('correo_electronico', $('input[name=correo_electronico]').val());
        formData.append('nombre', $('input[name=nombre]').val());
        formData.append('apellido1', $('input[name=apellido1]').val());
        formData.append('apellido2', $('input[name=apellido2]').val());
        formData.append('latitud', $('input[name=lng]').val());
        formData.append('longitud', $('input[name=lat]').val());
        formData.append('automovil_id', automovil_id);

        if ($('input[type=file]#imagen_licencia')[0].files[0] != undefined) {
            formData.append('imagen_licencia', $('input[type=file]#imagen_licencia')[0].files[0]);
        }
        if ($('input[type=file]#imagen_licencia2')[0].files[0] != undefined) {
            formData.append('imagen_licencia2', $('input[type=file]#imagen_licencia2')[0].files[0]);
        }
        
        if ($('input[type=file]#imagen_identificacion')[0].files[0] != undefined) {
            formData.append('imagen_identificacion', $('input[type=file]#imagen_identificacion')[0].files[0]);
        }
        if ($('input[type=file]#imagen_identificacion2')[0].files[0] != undefined) {
            formData.append('imagen_identificacion2', $('input[type=file]#imagen_identificacion2')[0].files[0]);
        }

        Pace.track(function() {
            $.ajax({
                    data: formData,
                    type: "POST",
                    dataType: "json",
                    url: PATH + "/ventas/autos_nuevos/guardar_contacto/",
                    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                    processData: false,
                })
                .done(function(data, textStatus, jqXHR) {

                    if (data.status == 'error') {
                        if (data.message != undefined) {

                            $.each(data.message, function(index, value) {
                                try {
                                    if ($('small#' + index + '_msg').length) {
                                        $('small#' + index + '_msg').html(value)
                                    }
                                } catch (error) {

                                }
                            });
                        }

                    } else {
                        Swal.fire({
                            title: 'Sus datos se han guardado correctamente',
                            text: 'En breve un asesor se comunicará con usted y le enviará información relacionada a su solicitud',
                            icon: 'success',
                            confirmButtonText: 'Aceptar'
                        }).then((result) => {
                            $("form#form_contacto")[0].reset();
                            $('div.ubicacion_agencia').hide();
                            app.initMap(20.6738686, -103.3705185, 6);
                            $('input[type=file]#imagen_identificacion').val('');
                            $('input[type=file]#imagen_licencia').val('');
                            $('#imagen_identificacion_label').html('');
                            $('#imagen_licencia_label').html('');
                            $('input[type=file]#imagen_identificacion2').val('');
                            $('input[type=file]#imagen_licencia2').val('');
                            $('#imagen_identificacion_label2').html('');
                            $('#imagen_licencia_label2').html('');
                        })


                    }
                });
        });


    },

    cotizar: function() {
        $("._precio").empty();
        var data_save = $('form#cotizar').serializeArray();
        Pace.track(function() {
            $.ajax({
                    data: data_save,
                    type: "POST",
                    dataType: "json",
                    url: PATH + "/ventas/autos_nuevos/cotizar/",
                })
                .done(function(data, textStatus, jqXHR) {
                    $("#resultado_cotizacion").hide();

                    if (data.status == 'error') {
                        if (data.message) {
                            message.error(data.message, true);
                        } else {
                            errors.mostrar(data.validations);
                        }
                    } else {
                        $("#resultado_cotizacion").show();
                        $("#mensualidad_precio").html('$' + data.respuesta.mensualidad_interes);
                        $("#interes_precio").html('$' + data.respuesta.total_intereses);
                        $("#subtotal").html('$' + data.respuesta.subtotal);
                        $("#total_precio").html('$' + data.respuesta.total);
                    }
                })
        });
    },

    render: function() {

        // $('button#ayuda_identificacion').popover({});

        $('#imagen_identificacion').on('change', function() {
            var fileName = document.getElementById("imagen_identificacion").files[0].name;
            $('#imagen_identificacion_label').addClass("selected").html(fileName);
        })
        $('#imagen_licencia').on('change', function() {
            var fileName = document.getElementById("imagen_licencia").files[0].name;
            $('#imagen_licencia_label').addClass("selected").html(fileName);
        })


        $('#imagen_identificacion2').on('change', function() {
            var fileName = document.getElementById("imagen_identificacion2").files[0].name;
            $('#imagen_identificacion_label2').addClass("selected").html(fileName);
        })
        $('#imagen_licencia2').on('change', function() {
            var fileName = document.getElementById("imagen_licencia2").files[0].name;
            $('#imagen_licencia_label2').addClass("selected").html(fileName);
        })

    }
});

$(function() {
    app = new APP_VIEW();
    setTimeout(() => {
        app.initMap(20.6738686, -103.3705185, 6);
    }, 1000);
    app.render();
})