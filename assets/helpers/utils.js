var _ajax = function() {
        this.responseDefault = {
            header: {
                url: null,
                status: 200,
                message: null,
                contentType: null,
                successfull: !1
            },
            data: null
        }, this.headerSend = function(e) {
            var t = {
                method: null,
                ///headers: utils.header()
            };
            return utils.objCombine(t, e)
        }, this.statusSuccess = [200, 201, 202, 204], this.fetch = function(e, t, n, a, r) {
            utils.Loader.open();
            var s = this,
                i = this.responseDefault;
            fetch(PATH + e, t).then(function(t) {
                var n = t.headers.get("content-type");
                i.header.contentType = n, i.header.status = t.status;
                var a = utf8.decode(t.headers.get("X-Message"));
                try {
                    utils.string.isJson(a) ? i.header.message = a.length ? a : "" : i.header.message = a.length ? $.trim(a.split('"').join("")) : ""
                } catch (e) {
                    i.header.message = a.length ? $.trim(a.split('"').join("")) : ""
                }
                return i.header.url = e, n && n.includes("application/json") ? t.json() : {}
            }).then(function(e) {
                i.data = e
            }).catch(function(e) {
                i.header.status = "500", i.header.message = e.toString()
            }).finally(function() {
                utils.Loader.closed(), s.responsefn(i, n, a, r)
            })
        }, this.responsefn = function(e, t, n, a) {
            0 != n && utils.Loader.closed(), e.header.successfull = "-1" != this.statusSuccess.indexOf(e.header.status), "-1" == this.statusSuccess.indexOf(e.header.status) ? (0 != a && this.showValidations(e.header), e.data = null) : 0 != a, utils.executeFn(t, e.data, e.header)
        }, this.buildFetch = function(e, t, n, a, r) {
            n = void 0 !== n ? n : null, a = void 0 === a || a, 0 != (r = void 0 === r || r) && utils.Loader.open(), this.fetch(e, t, n, r, a)
        }, this.get = function(e, t, n, a, r) {
            t = void 0 !== t && utils.objSize(t) > 0 ? "?" + $.param(t) : "";
            var s = this.headerSend({
                method: "GET"
            });
            this.buildFetch(e + t, s, n, a, r)
        }, this.getExcelFile = function(e, t, n) {
            var a = !1;
            $.each(t, function(e, t) {
                $.trim(t).length > 0 && (a = !0)
            }), 1 == a ? $.ajax({
                type: "GET",
                dataType: "json",
                url: PATH + e,
                data: t,
                headers: utils.header(),
                success: n
            }) : message.error("Favor de llenar por lo menos un campo para realizar la búsqueda", !1)
        }, this.post = function(e, t, n, a, r) {
            t = void 0 !== t ? t : null;
            var s = this.headerSend({
                method: "POST",
                body: JSON.stringify(t)
            });
            this.fetch(e, s, n, a, r)
        }, this.postFile = function(e, t, n, a, r) {
            t = void 0 !== t ? t : null;
            var s = this.headerSend({
                headers: {},
                method: "POST",
                body: t
            });
            this.fetch(e, s, n, a, r)
        }, this.delete = function(e, t, n, a, r) {
            t = void 0 !== t && utils.objSize(t) > 0 ? "?" + $.param(t) : "";
            var s = this.headerSend({
                method: "DELETE"
            });
            this.fetch(e + t, s, n, a, r)
        }, this.put = function(e, t, n, a, r) {
            t = void 0 !== t ? t : null;
            var s = this.headerSend({
                method: "PUT",
                body: JSON.stringify(t)
            });
            this.fetch(e, s, n, a, r)
        }, this.message = function(e) {
            var t = this,
                n = {};
            try {
                n = JSON.parse(e)
            } catch (t) {
                n.push(e)
            }
            var a = [];
            $.each(n, function(e, n) {
                $("[name=" + e + "]").length ? t.showError(e, n) : a.push(n)
            }), a.length >= 1 && e.error(resp.header.message, !0)
        }, this.showError = function(e, t) {
            0 == $("small#" + e + "_Help").length && $("[name=" + e + "]").after('<small id="' + e + '_Help" class="text-danger ajaxError"></small>'), 0 == $("small#" + e + "_Help").is(":visible") && $("small#" + e + "_Help").show(), $("small#" + e + "_Help").html(t), $("[name=" + e + "]").parent().addClass("warning-input")
        }, this.loadComponent = function(e, t) {
            var n = PATH + "/components/" + e;
            if ("js" == t)(a = document.createElement("script")).setAttribute("type", "text/javascript"), a.setAttribute("src", n);
            else if ("css" == t) {
                var a;
                (a = document.createElement("link")).setAttribute("rel", "stylesheet"), a.setAttribute("type", "text/css"), a.setAttribute("href", n)
            }
            void 0 !== a && document.getElementsByTagName("head")[0].appendChild(a)
        }, this.loadJS = function(e, t) {
            t = void 0 !== t && t;
            var n = document.getElementsByTagName("script")[0],
                a = document.createElement("script");
            a.type = "text/javascript", a.src = e, a.async = !0, a.onreadystatechange = function(e) {}, a.onload = a.onreadystatechange, a.onerror = function() {}, a.onabort = function(e) {}, null != n.parentNode && n.parentNode.insertBefore(a, n)
        }, this.loadCSS = function(e, t) {
            t = void 0 !== t && t;
            var n = document.createElement("link");
            n.rel = "preload", n.href = e, n.as = "style", n.async = !0, n.onload = function(e) {
                utils.executeFn(t), this.rel = "stylesheet", this.onload = null
            }, n.onload = n.onreadystatechange, n.onerror = function() {}, n.onabort = function(e) {}, document.head.insertBefore(n, document.head.childNodes[document.head.childNodes.length - 1].nextSibling)
        }, this.getCatalogo = function(e, t, n = !1, a = !1) {
            utils.Loader.open(), t && t.find("option").length > 0 && t.find("option").remove(), t.select2(utils.defaultSelect2()), Backbone.ajax({
                url: PATH + '/administrador/catalogos/' + e,
                type: "get",
                dataType: "json",
                beforeSend: function(e) {
                    var t = utils.header();
                    e.setRequestHeader("Authorization", t.Authorization)
                },
                success: function(e) {
                    $select = t;
                    var r = "";
                    if (1 == n) var s = t.attr("data");
                    $.each(e.data, function(e, t) {
                        1 == n && s == t.id ? r += "<option selected value=" + t.id + ">" + t.nombre + "</option>" : r += "<option value=" + t.id + ">" + t.nombre + "</option>"
                    }), $select.append(r), s || $select.val(""), $select.trigger("change.select2"), utils.executeFn(a, t)
                },
                error: function(e) {},
                complete: utils.Loader.closed
            })
        }, this.getCatalogoSimple = function(e, t, n = !1, a = !1) {
            utils.Loader.open(), t && t.find("option").length > 0 && t.find("option").remove(), Backbone.ajax({
                url: PATH + '/administrador/catalogos/' + e,
                type: "get",
                dataType: "json",
                beforeSend: function(e) {
                    var t = utils.header();
                    e.setRequestHeader("Authorization", t.Authorization)
                },
                success: function(e) {
                    $select = t;
                    var r = "";
                    if (1 == n) var s = t.attr("data");
                    $.each(e.data, function(e, t) {
                        1 == n && s == t.id ? r += "<option selected value=" + t.id + ">" + t.nombre + "</option>" : r += "<option value=" + t.id + ">" + t.nombre + "</option>"
                    }), $select.append(r), s || $select.val(""),  utils.executeFn(a, t)
                },
                error: function(e) {},
                complete: utils.Loader.closed
            })
        }, this.showValidations = function(e) {
            if (utils.string.isJson(e.message)) return $(".parsley-error").each(function(e) {
                $(this).children(".validation_error").remove(), $(this).removeClass("parsley-error"), $(".text-danger").html("")
            }), this.buildValidations(JSON.parse(e.message))
        }, this.buildValidations = function(e) {
            "object" == $.type(e) ? $.each(e, function(e, t) {
                $("small#msg_" + e).length ? ($("small#msg_" + e).html(t), $("[name=" + e + "]").addClass("parsley-error")) : toastr.error(t)
            }) : toastr.error(message.replace(/\"/g, ""))
        }, this.destruir = function() {
            $(".parsley-error").each(function(e) {
                $(this).children(".validation_error").remove(), $(this).removeClass("parsley-error"), $(".text-danger").html("")
            })
        }
    },
    _modal = function() {
        this.create = function(e, t) {
            var n = this;
            e = void 0 !== e ? e : "", (t = void 0 !== t ? t : {}).backdrop = !0, t.keyboard = !1, t.focus = !0;
            var a = utils.getId(),
                r = document.createElement("div");
            return r.classList.add("modal"), r.classList.add("fade"), r.id = a, document.body.appendChild(r), t.backdrop = "static", t.keyboard = !1, $("div#" + a).html(e).modal(t).on("hidden.bs.modal", function() {
                n.callbackClose(r.id)
            }), a
        }, this.callbackClose = function(e) {
            $("div#" + e).data("modal", null), $("div#" + e).remove()
        }
    },
    _utils = function() {
        this.getAllUrlParam = function(url) {

            // get query string from url (optional) or window
            var queryString = url ? url.split('?')[1] : window.location.search.slice(1);
            
            // we'll store the parameters here
            var obj = {};
            
            // if query string exists
            if (queryString) {
            
              // stuff after # is not part of query string, so get rid of it
              queryString = queryString.split('#')[0];
            
              // split our query string into its component parts
              var arr = queryString.split('&');
            
              for (var i = 0; i < arr.length; i++) {
                // separate the keys and the values
                var a = arr[i].split('=');
            
                // set parameter name and value (use 'true' if empty)
                var paramName = a[0];
                var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];
            
                // (optional) keep case consistent
                paramName = paramName.toLowerCase();
                if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();
            
                // if the paramName ends with square brackets, e.g. colors[] or colors[2]
                if (paramName.match(/\[(\d+)?\]$/)) {
            
                  // create key if it doesn't exist
                  var key = paramName.replace(/\[(\d+)?\]/, '');
                  if (!obj[key]) obj[key] = [];
            
                  // if it's an indexed array e.g. colors[2]
                  if (paramName.match(/\[\d+\]$/)) {
                    // get the index value and add the entry at the appropriate position
                    var index = /\[(\d+)\]/.exec(paramName)[1];
                    obj[key][index] = paramValue;
                  } else {
                    // otherwise add the value to the end of the array
                    obj[key].push(paramValue);
                  }
                } else {
                  // we're dealing with a string
                  if (!obj[paramName]) {
                    // if it doesn't exist, create property
                    obj[paramName] = paramValue;
                  } else if (obj[paramName] && typeof obj[paramName] === 'string'){
                    // if property does exist and it's a string, convert it to an array
                    obj[paramName] = [obj[paramName]];
                    obj[paramName].push(paramValue);
                  } else {
                    // otherwise add the property
                    obj[paramName].push(paramValue);
                  }
                }
              }
            }
            
            return obj;
        },
        this.datatablesConfig = {
                sProcessing: "Procesando...",
                sLengthMenu: "Mostrar _MENU_ registros",
                sZeroRecords: "No se encontraron resultados",
                sEmptyTable: "Ningún dato disponible en esta tabla",
                sInfo: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                sInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
                sInfoFiltered: "(filtrado de un total de _MAX_ registros)",
                sInfoPostFix: "",
                sSearch: "Buscar:",
                sUrl: "",
                sInfoThousands: ",",
                sLoadingRecords: "Cargando...",
                oPaginate: {
                    sNext: '<i class="fa fa-angle-right"></i>',
                    sPrevious: '<i class="fa fa-angle-left"></i>',
                    sFirst: '<i class="fa fa-step-backward"></i>',
                    sLast: '<i class="fa fa-step-forward"></i>'
                },
                oAria: {
                    sSortAscending: ": Activar para ordenar la columna de manera ascendente",
                    sSortDescending: ": Activar para ordenar la columna de manera descendente"
                }
            }, this.objSize = function(e) {
                var t, n = 0;
                for (t in e) e.hasOwnProperty(t) && n++;
                return n
            }, this.logout = function() {
                localStorage.clear(), window.location.href = PATH + "/logout"
            }, this.ObjToArray = function(e) {
                return Object.keys(e).map(function(t) {
                    return [Number(t), e[t]]
                })
            }, this.defaultSelect2 = function(e) {
                return e = void 0 !== e ? e : {}, utils.objCombine({
                    language: {
                        noResults: function() {
                            return "No hay resultado"
                        },
                        searching: function() {
                            return "Buscando.."
                        }
                    },
                    placeholder: "Seleccione una opción",
                    allowClear: !0,
                    width: "100%"
                }, e)
            }, this.defaultDatePicker = function(e) {
                return e = void 0 !== e ? e : {}, utils.objCombine({
                    locale: "es",
                    format: "DD/MM/YYYY",
                    icons: {
                        time: "far fa-clock",
                        date: "far fa-calendar",
                        up: "fas fa-arrow-up",
                        down: "fas fa-arrow-down",
                        previous: "fas fa-chevron-left",
                        next: "fas fa-chevron-right",
                        today: "far fa-calendar-check",
                        clear: "fas fa-trash",
                        close: "fas fa-times"
                    }
                }, e)
            }, this.isFunction = function(e) {
                return "function" == typeof e
            }, this.loadFilesDropzone = (async(e, t, n) => {
                let a = e;
                a.emit("addedfile", t), a.emit("thumbnail", t, n + t.name), a.emit("success", t), a.emit("complete", t)
            }), this.showMessageStringToJson = function(e) {
                var t = JSON.parse(e.message);
                $.each(t, function(e, t) {
                    return message.error(t[0])
                })
            }, this.showMessageStringToJsonArray = function(e, t = !0) {
                var n = JSON.parse(e.message);
                return $.each(n, function(e, t) {
                    var n = $('[name="' + e + '"]').prop("tagName");
                    "SELECT" == n ? ($('[name="' + e + '"]').parent().eq(0).addClass("warning-input"), $('[name="' + e + '"]').next().find(".select2-selection").css("border-color", "#d9534f")) : $('[name="' + e + '"]').parent().eq(0).addClass("warning-input");
                    var a = "";
                    t.length > 0 ? ($.each(t, function(e, t) {
                        a += "<small class='form-text text-danger'>" + t + "</small>"
                    }), "SELECT" == n ? $('[name="' + e + '"]').parent().after("<div class='validation_error'>" + a + "</div>") : $('[name="' + e + '"]').after("<div class='validation_error'>" + a + "</div>")) : "SELECT" == n ? $('[name="' + e + '"]').after("<div class='validation_error'><small class='form-text text-danger'>" + t + "</small></div>") : $('[name="' + e + '"]').parent().after("<div class='validation_error'><small class='form-text text-danger'>" + t + "</small></div>")
                }), message.error(app.config.mensaje.ERROR_ARRAY, t)
            }, this.executeFn = function(e, t, n, a) {
                return t = void 0 !== t ? t : "", n = void 0 !== n ? n : "", a = void 0 !== a ? a : "", !!this.isFunction(e) && (e(t, n, a), !0)
            }, this.objCombine = function(e, t) {
                for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
                return e
            }, this.getId = function() {
                var e = Math.random();
                return e.toString(36), e.toString(36).substr(2, 9)
            }, this.select2 = {
                config: function(e) {
                    return {
                        allowClear: !0,
                        tags: !0,
                        language: "es",
                        placeholder: e = void 0 !== e && e.length > 0 ? e : ""
                    }
                },
                optionsCatalogo: function(e, t) {
                    if ("object" == typeof e && e.length > 0) {
                        var n = [];
                        $.each(e, function(e, t) {
                            n.push(new Option(t.descripcion, t.id, !1, !1))
                        }), t.append(n)
                    }
                }
            }, this.string = {
                empty: function(e) {
                    return e && null != e && "" != e && 0 != e.length ? e : ""
                },
                firstUpperCase: function(e) {
                    return e.charAt(0).toUpperCase() + e.slice(1)
                },
                replace: function(e, t, n) {
                    e = $.trim(e);
                    for (var a = 0; a < t.length; a++) e = e.replace(t[a], n[a]);
                    return e
                },
                toOBJ: function(e) {
                    var t = {};
                    try {
                        t = JSON.parse(e)
                    } catch (n) {
                        t = $.parseJSON(e)
                    }
                    return t
                },
                isJson: function(e) {
                    e = "string" != typeof e ? JSON.stringify(e) : e;
                    try {
                        e = JSON.parse(e)
                    } catch (e) {
                        return !1
                    }
                    return "object" == typeof e && null !== e
                }
            }, this.isDefined = function(e) {
                return void 0 !== e && null != e
            }, this.header = function() {
                // var e;
                // try {
                //     e = {
                //         Accept: "application/json",
                //         Authorization: "Bearer " + window.atob(xtoken),
                //         "Content-Type": "application/json"
                //     }
                // } catch (t) {
                //     e = {
                //         Accept: "application/json",
                //         "Content-Type": "application/json"
                //     }
                // }
                return {};
            }, this.makeId = function() {
                for (var e = "", t = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", n = 0; n < 5; n++) e += t.charAt(Math.floor(Math.random() * t.length));
                return e
            }, this.loadScript = function(e, t) {
                jQuery.ajax({
                    url: e,
                    dataType: "script",
                    success: t,
                    async: !0
                })
            }, this.validaCurp = function(e) {
                return "" != e && e.match(/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0\d|1[0-2])(?:[0-2]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/)
            }, this.validaRfc = function(e) {
                return "" != e && e.match(/^[A-Za-z]{4}[ |\-]{0,1}[0-9]{6}[ |\-]{0,1}[0-9A-Za-z]{3}$/)
            }, this.obtenerEdad = function(e) {
                var t = new Date,
                    n = new Date(e),
                    a = t.getFullYear() - n.getFullYear(),
                    r = t.getMonth() - n.getMonth();
                return (r < 0 || 0 === r && t.getDate() < n.getDate()) && a--, a
            }, this.serializeForm = function(e) {
                return Backbone.Syphon.serialize(e)
            }, this.Loader = {
                open: function(e) {
                    e = void 0 !== e ? e : "", $(".page-loader-wrapper h1").html(e), $(".page-loader-wrapper").show()
                },
                closed: function() {
                    $(".page-loader-wrapper").hide()
                }
            },
            this.detectIE = function() {
                var e = window.navigator.userAgent,
                    t = e.indexOf("MSIE ");
                if (t > 0) return parseInt(e.substring(t + 5, e.indexOf(".", t)), 10);
                if (e.indexOf("Trident/") > 0) {
                    var n = e.indexOf("rv:");
                    return parseInt(e.substring(n + 3, e.indexOf(".", n)), 10)
                }
                var a = e.indexOf("Edge/");
                return a > 0 && parseInt(e.substring(a + 5, e.indexOf(".", a)), 10)
            }, this.displayWarningDialog = function(e, t, n) {
                Swal.fire({
                    title: utils.isDefined(e) ? e : "¿Estás seguro?",
                    type: utils.isDefined(t) ? t : "warning",
                    showCloseButton: !0,
                    showCancelButton: !0,
                    confirmButtonText: "Aceptar",
                    cancelButtonText: "Cerrar",
                    confirmButtonClass: "btn btn-primary",
                    cancelButtonClass: "btn btn-outline-secondary"
                }).then(n)
            }, this.button = function(e, t) {
                if (utils.isDefined(e) && utils.isDefined(t)) {
                    var n = $(e),
                        a = $(n).html();
                    if (t) {
                        var r = '<i class="fa fa-spinner fa-spin fa-fw"></i>' + t;
                        $(n).html() !== r && (n.html(r), n.attr("disabled", !0)), setTimeout(function() {
                            n.html(a), n.attr("disabled", !1)
                        }, 2e3)
                    }
                }
            }
    },
    utf8 = new function() {
        this.encode = function(e) {
            var t = [],
                n = 0,
                a = 0,
                r = 0;
            for (e += ""; n < e.length;) {
                r = 0, (a = 255 & e.charCodeAt(n)) <= 191 ? (a &= 127, r = 1) : a <= 223 ? (a &= 31, r = 2) : a <= 239 ? (a &= 15, r = 3) : (a &= 7, r = 4);
                for (var s = 1; s < r; ++s) a = a << 6 | 63 & e.charCodeAt(s + n);
                4 === r ? (a -= 65536, t.push(String.fromCharCode(55296 | a >> 10 & 1023)), t.push(String.fromCharCode(56320 | 1023 & a))) : t.push(String.fromCharCode(a)), n += r
            }
            return t.join("")
        }, this.decode = function(e) {
            var t = [],
                n = 0,
                a = 0,
                r = 0,
                s = 0,
                i = 0;
            for (e += ""; n < e.length;)(r = e.charCodeAt(n)) <= 191 ? (t[a++] = String.fromCharCode(r), n++) : r <= 223 ? (s = e.charCodeAt(n + 1), t[a++] = String.fromCharCode((31 & r) << 6 | 63 & s), n += 2) : r <= 239 ? (s = e.charCodeAt(n + 1), i = e.charCodeAt(n + 2), t[a++] = String.fromCharCode((15 & r) << 12 | (63 & s) << 6 | 63 & i), n += 3) : (r = (7 & r) << 18 | (63 & (s = e.charCodeAt(n + 1))) << 12 | (63 & (i = e.charCodeAt(n + 2))) << 6 | 63 & e.charCodeAt(n + 3), r -= 65536, t[a++] = String.fromCharCode(55296 | r >> 10 & 1023), t[a++] = String.fromCharCode(56320 | 1023 & r), n += 4);
            return t.join("")
        }
    },
    _messages = function() {
        this.getToast = function(customConfig) {
                var config = {
                    toast: true,
                    position: 'top',
                    showConfirmButton: false,
                    timer: 7000
                };
                return Swal.mixin(utils.objCombine(config, customConfig));
            },
            this.builder = function(_msg, _modal, _tipo, customConfig) {
                _modal = (typeof _modal !== 'undefined') ? _modal : true;
                _msg = (typeof _msg !== 'undefined') ? _msg : '';
                if ($.trim(_msg).length == 0) {
                    return false;
                }
                if (_modal) {
                    Swal.fire({
                        title: _msg,
                        type: _tipo,
                        allowOutsideClick: false
                    }).then(function(result) {
                        utils.executeFn(_modal, result);
                    });
                } else {
                    var toast = this.getToast(customConfig);
                    switch (_tipo) {
                        case 'success':
                            toastr.success(_msg);
                            break;
                        case 'error':
                            toastr.error(_msg);
                            break;
                        case 'warning':
                            toastr.warning(_msg);
                            break;
                        case 'info':
                            toastr.info(_msg);
                            break;
                    }
                    toast.fire({
                        // type: _tipo,
                        // title: _msg
                    });
                }
            },
            this.error = function(_msg, _modal) {
                this.builder(_msg, _modal, 'error', {
                    background: '#F8D7DA'
                });
            },
            this.info = function(_msg, _modal) {
                this.builder(_msg, _modal, 'info', {
                    background: '#CCE5FF'
                });
            },
            this.success = function(_msg, _modal) {
                this.builder(_msg, _modal, 'success', {
                    background: '#D4EDDA'
                });
            }

    }
_permisos = function() {
    this.validar = function() {
        let params = {
            'tipoModulo': moduloActual,
            'seccion': seccionActual,
            'perfil_id': perfil_id
        }
        let funciones = ['Visualizar', 'Agregar', 'Editar', 'Eliminar', 'Descargar'];
        let permisos = [];
        ajax.get('/api/modulos/permisosFunciones', params, function(response, header) {
            if (header.status == 200) {
                let datos = response.shift();
                $.each(datos.funciones, function(key, value) {
                    permisos.push(value.nombre);
                });
                console.log("Este módulo tiene permisos para: " + permisos);
                $.each(funciones, function(key, funcion) {
                    if ($.inArray(funcion, permisos) !== -1) {
                        $('.f_' + funcion.toLowerCase()).show();
                    } else {
                        $('.f_' + funcion.toLowerCase()).remove();
                    }
                });
            } else {
                message.info('Sin permisos para este módulo', false);
            }
        });
    }
}
_showErrors = function() {
    this.destruir = function() {
        $('.has-error').each(function(index) {
            $(this).removeClass('has-error');
            $(".text-danger").html("");
        });
    }
    this.mostrar = function(errores) {
        $('.has-error').each(function(index) {
            $(this).removeClass('has-error');
            $(".text-danger").html("");
        });
        $.each(errores, function(index, value) {
            if ($('small#msg_' + index).length) {
                $('small#msg_' + index).html(value);
                $('[name="' + index + '"]').parent().eq(0).addClass('has-error');
            } else {
                toastr["error"](value);
            }
        });
    }
}

modal = new _modal;
utils = new _utils;
message = new _messages;
ajax = new _ajax;
permisos = new _permisos;
errors = new _showErrors;