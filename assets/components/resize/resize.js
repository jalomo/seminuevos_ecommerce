var Liip = Liip || {};
Liip.resizer = (function($) {
    var mainCanvas;

    var init = function() {
        $("#resize").click(startResize);
        $('.custom-file-input').on('change', startResize)

    };

    var createImage = function(src) {
        var deferred = $.Deferred();
        var img = new Image();

        img.onload = function() {
            deferred.resolve(img);
        };
        img.src = src;
        return deferred.promise();
    };

    function startResize() {
        setTimeout(() => {
            $.when(
                createImage($("#outputImage").attr('src'))
            ).then(resize, function() { console.log('error') });
        }, 3000);

    };

    var resize = function(image) {
        console.log(image.width);
        mainCanvas = document.createElement("canvas");
        mainCanvas.width = image.width;
        mainCanvas.height = image.height;
        var ctx = mainCanvas.getContext("2d");
        ctx.drawImage(image, 0, 0, mainCanvas.width, mainCanvas.height);
        size = parseInt(1023, 10);
        console.log(size);
        while (mainCanvas.width > size) {
            mainCanvas = halfSize(mainCanvas);
        }
        $('#outputImage').attr('src', mainCanvas.toDataURL("image/jpeg"));
    };

    var halfSize = function(i) {
        var canvas = document.createElement("canvas");
        canvas.width = i.width / 1.2;
        canvas.height = i.height / 1.2;

        var ctx = canvas.getContext("2d");
        ctx.drawImage(i, 0, 0, canvas.width, canvas.height);
        return canvas;
    };

    return {
        init: init
    };

})(jQuery);

jQuery(function($) {
    Liip.resizer.init();
});